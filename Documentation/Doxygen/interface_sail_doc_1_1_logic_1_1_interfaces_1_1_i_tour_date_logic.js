var interface_sail_doc_1_1_logic_1_1_interfaces_1_1_i_tour_date_logic =
[
    [ "DeleteTour", "interface_sail_doc_1_1_logic_1_1_interfaces_1_1_i_tour_date_logic.html#a31e5393ce730815a510f5747a8733810", null ],
    [ "GetAllTour", "interface_sail_doc_1_1_logic_1_1_interfaces_1_1_i_tour_date_logic.html#ab1404fbe0ff55b3cdffc98bb243a53e6", null ],
    [ "GetTourDateById", "interface_sail_doc_1_1_logic_1_1_interfaces_1_1_i_tour_date_logic.html#aeb254aea6762048342e3ad163ab83dea", null ],
    [ "InsertTour", "interface_sail_doc_1_1_logic_1_1_interfaces_1_1_i_tour_date_logic.html#a47899c12a6f4f197e5790e461565cdff", null ],
    [ "UpdateTour", "interface_sail_doc_1_1_logic_1_1_interfaces_1_1_i_tour_date_logic.html#a21bf7ac100de09bde79caa5f4b6f9697", null ]
];