var interface_sail_doc_1_1_logic_1_1_interfaces_1_1_i_tour_logics =
[
    [ "DeleteTour", "interface_sail_doc_1_1_logic_1_1_interfaces_1_1_i_tour_logics.html#a3c6e9bcf6449c5592d575413faea7743", null ],
    [ "GetAllTour", "interface_sail_doc_1_1_logic_1_1_interfaces_1_1_i_tour_logics.html#acd59f853ef86b077b8c47bc6be81bf9a", null ],
    [ "GetTourById", "interface_sail_doc_1_1_logic_1_1_interfaces_1_1_i_tour_logics.html#a469e1b91c7609f1f66f959534879efd9", null ],
    [ "InsertTour", "interface_sail_doc_1_1_logic_1_1_interfaces_1_1_i_tour_logics.html#a61cf6e825ace6cee637d7d3e879b92fb", null ],
    [ "UpdateTour", "interface_sail_doc_1_1_logic_1_1_interfaces_1_1_i_tour_logics.html#a5f3b413b97484d88aaeb3c92d93d9393", null ]
];