var interface_sail_doc_1_1_repository_1_1_interfaces_1_1_i_repository_base =
[
    [ "Delete", "interface_sail_doc_1_1_repository_1_1_interfaces_1_1_i_repository_base.html#ae1da7ba1ba6eab4cc3b1f34e8ea7f957", null ],
    [ "GetAll", "interface_sail_doc_1_1_repository_1_1_interfaces_1_1_i_repository_base.html#a71fcd1b6a0e3a1e98919cb647aa83a6d", null ],
    [ "GetById", "interface_sail_doc_1_1_repository_1_1_interfaces_1_1_i_repository_base.html#a1f4802d09b8c863f8e314308ab124222", null ],
    [ "Insert", "interface_sail_doc_1_1_repository_1_1_interfaces_1_1_i_repository_base.html#af54ef754d034194eb5b9be31fa9acbc8", null ],
    [ "Update", "interface_sail_doc_1_1_repository_1_1_interfaces_1_1_i_repository_base.html#a37b4772e85d6e0864f0a5f2ef18a4450", null ]
];