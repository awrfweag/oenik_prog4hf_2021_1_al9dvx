var interface_sail_doc_1_1_logic_1_1_interfaces_1_1_i_harbour_logic =
[
    [ "DeleteHarbour", "interface_sail_doc_1_1_logic_1_1_interfaces_1_1_i_harbour_logic.html#a70f7416dbe73689c0473359e6bca7ef8", null ],
    [ "GetAllHarbour", "interface_sail_doc_1_1_logic_1_1_interfaces_1_1_i_harbour_logic.html#aa9afbe57267bf1e11e68c1f8cb1d5dbd", null ],
    [ "GetHarbourById", "interface_sail_doc_1_1_logic_1_1_interfaces_1_1_i_harbour_logic.html#aa756d2fe6db581c49d8c6674516ecdb7", null ],
    [ "InsertHarbour", "interface_sail_doc_1_1_logic_1_1_interfaces_1_1_i_harbour_logic.html#a1f00d6840a2ede40de5681c886ded84f", null ],
    [ "UpdateHarbour", "interface_sail_doc_1_1_logic_1_1_interfaces_1_1_i_harbour_logic.html#a0c9cd66dfb03b9b8b6ea0a66ce93896d", null ]
];