var annotated_dup =
[
    [ "SailDoc", null, [
      [ "Data", "namespace_sail_doc_1_1_data.html", "namespace_sail_doc_1_1_data" ],
      [ "Logic", "namespace_sail_doc_1_1_logic.html", "namespace_sail_doc_1_1_logic" ],
      [ "Repository", "namespace_sail_doc_1_1_repository.html", "namespace_sail_doc_1_1_repository" ],
      [ "Test", null, [
        [ "BoatManagementTests", "class_sail_doc_1_1_test_1_1_boat_management_tests.html", "class_sail_doc_1_1_test_1_1_boat_management_tests" ],
        [ "HarbourManagementTests", "class_sail_doc_1_1_test_1_1_harbour_management_tests.html", "class_sail_doc_1_1_test_1_1_harbour_management_tests" ],
        [ "SailorManagementTests", "class_sail_doc_1_1_test_1_1_sailor_management_tests.html", "class_sail_doc_1_1_test_1_1_sailor_management_tests" ],
        [ "TripHandlingTests", "class_sail_doc_1_1_test_1_1_trip_handling_tests.html", "class_sail_doc_1_1_test_1_1_trip_handling_tests" ]
      ] ],
      [ "DependencyFactory", "class_sail_doc_1_1_dependency_factory.html", "class_sail_doc_1_1_dependency_factory" ],
      [ "Program", "class_sail_doc_1_1_program.html", null ]
    ] ]
];