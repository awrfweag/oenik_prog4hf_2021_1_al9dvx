var namespace_sail_doc_1_1_logic_1_1_interfaces =
[
    [ "IBoatLogic", "interface_sail_doc_1_1_logic_1_1_interfaces_1_1_i_boat_logic.html", "interface_sail_doc_1_1_logic_1_1_interfaces_1_1_i_boat_logic" ],
    [ "IHarbourLogic", "interface_sail_doc_1_1_logic_1_1_interfaces_1_1_i_harbour_logic.html", "interface_sail_doc_1_1_logic_1_1_interfaces_1_1_i_harbour_logic" ],
    [ "IHarbourManagement", "interface_sail_doc_1_1_logic_1_1_interfaces_1_1_i_harbour_management.html", "interface_sail_doc_1_1_logic_1_1_interfaces_1_1_i_harbour_management" ],
    [ "IRouteLogic", "interface_sail_doc_1_1_logic_1_1_interfaces_1_1_i_route_logic.html", "interface_sail_doc_1_1_logic_1_1_interfaces_1_1_i_route_logic" ],
    [ "ISailorLogic", "interface_sail_doc_1_1_logic_1_1_interfaces_1_1_i_sailor_logic.html", "interface_sail_doc_1_1_logic_1_1_interfaces_1_1_i_sailor_logic" ],
    [ "ISailorManagement", "interface_sail_doc_1_1_logic_1_1_interfaces_1_1_i_sailor_management.html", "interface_sail_doc_1_1_logic_1_1_interfaces_1_1_i_sailor_management" ],
    [ "ITourDateLogic", "interface_sail_doc_1_1_logic_1_1_interfaces_1_1_i_tour_date_logic.html", "interface_sail_doc_1_1_logic_1_1_interfaces_1_1_i_tour_date_logic" ],
    [ "ITourLogics", "interface_sail_doc_1_1_logic_1_1_interfaces_1_1_i_tour_logics.html", "interface_sail_doc_1_1_logic_1_1_interfaces_1_1_i_tour_logics" ],
    [ "ITripHandling", "interface_sail_doc_1_1_logic_1_1_interfaces_1_1_i_trip_handling.html", "interface_sail_doc_1_1_logic_1_1_interfaces_1_1_i_trip_handling" ]
];