var class_sail_doc_1_1_repository_1_1_repositories_1_1_repository_base =
[
    [ "RepositoryBase", "class_sail_doc_1_1_repository_1_1_repositories_1_1_repository_base.html#aa9d01087e6ed9226472341eeca309837", null ],
    [ "Delete", "class_sail_doc_1_1_repository_1_1_repositories_1_1_repository_base.html#a5c34935a8e5ca17ee19cfbbadd8c797f", null ],
    [ "GetAll", "class_sail_doc_1_1_repository_1_1_repositories_1_1_repository_base.html#aa9fab81ab7be24fd770cbd09de8aea6d", null ],
    [ "GetById", "class_sail_doc_1_1_repository_1_1_repositories_1_1_repository_base.html#ad2b80f05bfdd36b88bd303699671e947", null ],
    [ "Insert", "class_sail_doc_1_1_repository_1_1_repositories_1_1_repository_base.html#a825e45166e403fe874323df4edc161ce", null ],
    [ "Update", "class_sail_doc_1_1_repository_1_1_repositories_1_1_repository_base.html#af688942636c37353b163ee597277b1b7", null ],
    [ "dbContext", "class_sail_doc_1_1_repository_1_1_repositories_1_1_repository_base.html#aec0f17f4ad4a7ef2f76454ccfcd26bb4", null ]
];