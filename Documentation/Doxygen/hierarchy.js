var hierarchy =
[
    [ "SailDoc.Test.BoatManagementTests", "class_sail_doc_1_1_test_1_1_boat_management_tests.html", null ],
    [ "DbContext", null, [
      [ "SailDoc.Data.Models.SailDocContext", "class_sail_doc_1_1_data_1_1_models_1_1_sail_doc_context.html", null ]
    ] ],
    [ "SailDoc.DependencyFactory", "class_sail_doc_1_1_dependency_factory.html", null ],
    [ "SailDoc.Data.Models.Hajo", "class_sail_doc_1_1_data_1_1_models_1_1_hajo.html", null ],
    [ "SailDoc::Test::HarbourManagementTests", "class_sail_doc_1_1_test_1_1_harbour_management_tests.html", null ],
    [ "SailDoc.Logic.Models.HarbourStat", "class_sail_doc_1_1_logic_1_1_models_1_1_harbour_stat.html", null ],
    [ "SailDoc.Logic.Interfaces.IBoatLogic", "interface_sail_doc_1_1_logic_1_1_interfaces_1_1_i_boat_logic.html", [
      [ "SailDoc.Logic.Services.BoatLogic", "class_sail_doc_1_1_logic_1_1_services_1_1_boat_logic.html", null ]
    ] ],
    [ "SailDoc.Logic.Interfaces.IHarbourLogic", "interface_sail_doc_1_1_logic_1_1_interfaces_1_1_i_harbour_logic.html", [
      [ "SailDoc.Logic.Services.HarbourLogics", "class_sail_doc_1_1_logic_1_1_services_1_1_harbour_logics.html", null ]
    ] ],
    [ "SailDoc.Logic.Interfaces.IHarbourManagement", "interface_sail_doc_1_1_logic_1_1_interfaces_1_1_i_harbour_management.html", [
      [ "SailDoc.Logic.Services.HarbourManagement", "class_sail_doc_1_1_logic_1_1_services_1_1_harbour_management.html", null ]
    ] ],
    [ "SailDoc.Repository.Interfaces.IRepositoryBase< TEntity, TKey >", "interface_sail_doc_1_1_repository_1_1_interfaces_1_1_i_repository_base.html", [
      [ "SailDoc.Repository.Repositories.RepositoryBase< TEntity, TKey >", "class_sail_doc_1_1_repository_1_1_repositories_1_1_repository_base.html", null ]
    ] ],
    [ "SailDoc.Repository.Interfaces.IRepositoryBase< Hajo, int >", "interface_sail_doc_1_1_repository_1_1_interfaces_1_1_i_repository_base.html", [
      [ "SailDoc.Repository.Interfaces.IBoatRepository", "interface_sail_doc_1_1_repository_1_1_interfaces_1_1_i_boat_repository.html", [
        [ "SailDoc.Repository.Repositories.BoatRepository", "class_sail_doc_1_1_repository_1_1_repositories_1_1_boat_repository.html", null ]
      ] ]
    ] ],
    [ "SailDoc.Repository.Interfaces.IRepositoryBase< Kikoto, int >", "interface_sail_doc_1_1_repository_1_1_interfaces_1_1_i_repository_base.html", [
      [ "SailDoc.Repository.Interfaces.IHarbourRepository", "interface_sail_doc_1_1_repository_1_1_interfaces_1_1_i_harbour_repository.html", [
        [ "SailDoc.Repository.Repositories.HarbourRepository", "class_sail_doc_1_1_repository_1_1_repositories_1_1_harbour_repository.html", null ]
      ] ]
    ] ],
    [ "SailDoc.Repository.Interfaces.IRepositoryBase< Matroz, int >", "interface_sail_doc_1_1_repository_1_1_interfaces_1_1_i_repository_base.html", [
      [ "SailDoc.Repository.Interfaces.ISailorRepository", "interface_sail_doc_1_1_repository_1_1_interfaces_1_1_i_sailor_repository.html", [
        [ "SailDoc.Repository.Repositories.SailorRepository", "class_sail_doc_1_1_repository_1_1_repositories_1_1_sailor_repository.html", null ]
      ] ]
    ] ],
    [ "SailDoc.Repository.Interfaces.IRepositoryBase< Tura, int >", "interface_sail_doc_1_1_repository_1_1_interfaces_1_1_i_repository_base.html", [
      [ "SailDoc.Repository.Interfaces.ITourRepository", "interface_sail_doc_1_1_repository_1_1_interfaces_1_1_i_tour_repository.html", [
        [ "SailDoc.Repository.Repositories.TourRepository", "class_sail_doc_1_1_repository_1_1_repositories_1_1_tour_repository.html", null ]
      ] ]
    ] ],
    [ "SailDoc.Repository.Interfaces.IRepositoryBase< TuraDatum, int >", "interface_sail_doc_1_1_repository_1_1_interfaces_1_1_i_repository_base.html", [
      [ "SailDoc.Repository.Interfaces.ITourDateRepository", "interface_sail_doc_1_1_repository_1_1_interfaces_1_1_i_tour_date_repository.html", [
        [ "SailDoc.Repository.Repositories.TourDateRepository", "class_sail_doc_1_1_repository_1_1_repositories_1_1_tour_date_repository.html", null ]
      ] ]
    ] ],
    [ "SailDoc.Repository.Interfaces.IRepositoryBase< Utvonal, int >", "interface_sail_doc_1_1_repository_1_1_interfaces_1_1_i_repository_base.html", [
      [ "SailDoc.Repository.Interfaces.IRouteRepository", "interface_sail_doc_1_1_repository_1_1_interfaces_1_1_i_route_repository.html", [
        [ "SailDoc.Repository.Repositories.RouteRepository", "class_sail_doc_1_1_repository_1_1_repositories_1_1_route_repository.html", null ]
      ] ]
    ] ],
    [ "SailDoc.Logic.Interfaces.IRouteLogic", "interface_sail_doc_1_1_logic_1_1_interfaces_1_1_i_route_logic.html", [
      [ "SailDoc.Logic.Services.RouteLogics", "class_sail_doc_1_1_logic_1_1_services_1_1_route_logics.html", null ]
    ] ],
    [ "SailDoc.Logic.Interfaces.ISailorLogic", "interface_sail_doc_1_1_logic_1_1_interfaces_1_1_i_sailor_logic.html", [
      [ "SailDoc.Logic.Services.SailorLogic", "class_sail_doc_1_1_logic_1_1_services_1_1_sailor_logic.html", null ]
    ] ],
    [ "SailDoc.Logic.Interfaces.ISailorManagement", "interface_sail_doc_1_1_logic_1_1_interfaces_1_1_i_sailor_management.html", [
      [ "SailDoc.Logic.Services.SailorManagement", "class_sail_doc_1_1_logic_1_1_services_1_1_sailor_management.html", null ]
    ] ],
    [ "SailDoc.Logic.Interfaces.ITourDateLogic", "interface_sail_doc_1_1_logic_1_1_interfaces_1_1_i_tour_date_logic.html", null ],
    [ "SailDoc.Logic.Interfaces.ITourLogics", "interface_sail_doc_1_1_logic_1_1_interfaces_1_1_i_tour_logics.html", [
      [ "SailDoc.Logic.Services.TourLogic", "class_sail_doc_1_1_logic_1_1_services_1_1_tour_logic.html", null ]
    ] ],
    [ "SailDoc.Logic.Models.ITrip", "interface_sail_doc_1_1_logic_1_1_models_1_1_i_trip.html", [
      [ "SailDoc.Logic.Models.Trip", "class_sail_doc_1_1_logic_1_1_models_1_1_trip.html", null ],
      [ "SailDoc.Logic.Models.TripDetails", "class_sail_doc_1_1_logic_1_1_models_1_1_trip_details.html", null ]
    ] ],
    [ "SailDoc.Logic.Interfaces.ITripHandling", "interface_sail_doc_1_1_logic_1_1_interfaces_1_1_i_trip_handling.html", [
      [ "SailDoc.Logic.Services.TripHandling", "class_sail_doc_1_1_logic_1_1_services_1_1_trip_handling.html", null ]
    ] ],
    [ "SailDoc.Data.Models.Kikoto", "class_sail_doc_1_1_data_1_1_models_1_1_kikoto.html", null ],
    [ "SailDoc.Data.Models.Matroz", "class_sail_doc_1_1_data_1_1_models_1_1_matroz.html", null ],
    [ "SailDoc.Program", "class_sail_doc_1_1_program.html", null ],
    [ "SailDoc.Repository.Repositories.RepositoryBase< Hajo, int >", "class_sail_doc_1_1_repository_1_1_repositories_1_1_repository_base.html", [
      [ "SailDoc.Repository.Repositories.BoatRepository", "class_sail_doc_1_1_repository_1_1_repositories_1_1_boat_repository.html", null ]
    ] ],
    [ "SailDoc.Repository.Repositories.RepositoryBase< Kikoto, int >", "class_sail_doc_1_1_repository_1_1_repositories_1_1_repository_base.html", [
      [ "SailDoc.Repository.Repositories.HarbourRepository", "class_sail_doc_1_1_repository_1_1_repositories_1_1_harbour_repository.html", null ]
    ] ],
    [ "SailDoc.Repository.Repositories.RepositoryBase< Matroz, int >", "class_sail_doc_1_1_repository_1_1_repositories_1_1_repository_base.html", [
      [ "SailDoc.Repository.Repositories.SailorRepository", "class_sail_doc_1_1_repository_1_1_repositories_1_1_sailor_repository.html", null ]
    ] ],
    [ "SailDoc.Repository.Repositories.RepositoryBase< Tura, int >", "class_sail_doc_1_1_repository_1_1_repositories_1_1_repository_base.html", [
      [ "SailDoc.Repository.Repositories.TourRepository", "class_sail_doc_1_1_repository_1_1_repositories_1_1_tour_repository.html", null ]
    ] ],
    [ "SailDoc.Repository.Repositories.RepositoryBase< TuraDatum, int >", "class_sail_doc_1_1_repository_1_1_repositories_1_1_repository_base.html", [
      [ "SailDoc.Repository.Repositories.TourDateRepository", "class_sail_doc_1_1_repository_1_1_repositories_1_1_tour_date_repository.html", null ]
    ] ],
    [ "SailDoc.Repository.Repositories.RepositoryBase< Utvonal, int >", "class_sail_doc_1_1_repository_1_1_repositories_1_1_repository_base.html", [
      [ "SailDoc.Repository.Repositories.RouteRepository", "class_sail_doc_1_1_repository_1_1_repositories_1_1_route_repository.html", null ]
    ] ],
    [ "SailDoc.Test.SailorManagementTests", "class_sail_doc_1_1_test_1_1_sailor_management_tests.html", null ],
    [ "SailDoc.Logic.Models.SailorStat", "class_sail_doc_1_1_logic_1_1_models_1_1_sailor_stat.html", null ],
    [ "SailDoc.Test.TripHandlingTests", "class_sail_doc_1_1_test_1_1_trip_handling_tests.html", null ],
    [ "SailDoc.Data.Models.Tura", "class_sail_doc_1_1_data_1_1_models_1_1_tura.html", null ],
    [ "SailDoc.Data.Models.TuraDatum", "class_sail_doc_1_1_data_1_1_models_1_1_tura_datum.html", null ],
    [ "SailDoc.Data.Models.Utvonal", "class_sail_doc_1_1_data_1_1_models_1_1_utvonal.html", null ]
];