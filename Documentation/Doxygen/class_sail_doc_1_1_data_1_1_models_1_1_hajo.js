var class_sail_doc_1_1_data_1_1_models_1_1_hajo =
[
    [ "Equals", "class_sail_doc_1_1_data_1_1_models_1_1_hajo.html#a95fc8a41bcf6c780e8926ee579ec23ca", null ],
    [ "GetHashCode", "class_sail_doc_1_1_data_1_1_models_1_1_hajo.html#ad88e0cdf033f9a651d86b1cd54a57e36", null ],
    [ "ToString", "class_sail_doc_1_1_data_1_1_models_1_1_hajo.html#ac5a7d11c5abaa7f1ecde17a4b3a109cf", null ],
    [ "Ferohely", "class_sail_doc_1_1_data_1_1_models_1_1_hajo.html#af54f506735a46cdb83b824c46ef8040e", null ],
    [ "Hajohossz", "class_sail_doc_1_1_data_1_1_models_1_1_hajo.html#a960b763d31e465c7b7627f5e037eac83", null ],
    [ "HajoId", "class_sail_doc_1_1_data_1_1_models_1_1_hajo.html#a96c167c1ae256f3852632dba47a7ebb5", null ],
    [ "Lajstromszam", "class_sail_doc_1_1_data_1_1_models_1_1_hajo.html#a82fe3ba6f84654062f2f20b2634d26d5", null ],
    [ "Merules", "class_sail_doc_1_1_data_1_1_models_1_1_hajo.html#a4c6575441df6a59c40403664b0c54966", null ],
    [ "Nev", "class_sail_doc_1_1_data_1_1_models_1_1_hajo.html#a30aced9a8b842dadf3880cd8a305be2b", null ]
];