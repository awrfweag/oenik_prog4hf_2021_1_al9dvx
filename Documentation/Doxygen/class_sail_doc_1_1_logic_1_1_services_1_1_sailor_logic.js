var class_sail_doc_1_1_logic_1_1_services_1_1_sailor_logic =
[
    [ "SailorLogic", "class_sail_doc_1_1_logic_1_1_services_1_1_sailor_logic.html#a8f79617ba782eb5d072107caae98db59", null ],
    [ "DeleteSailor", "class_sail_doc_1_1_logic_1_1_services_1_1_sailor_logic.html#a5e2ea7d7e0a2943b3703f2142e356c26", null ],
    [ "GetAllSailor", "class_sail_doc_1_1_logic_1_1_services_1_1_sailor_logic.html#aff998f3ef7ffacf46ef171db871d3df6", null ],
    [ "GetSailorById", "class_sail_doc_1_1_logic_1_1_services_1_1_sailor_logic.html#a2d99e45f45bf2c18c0cb79dfceaa7f2e", null ],
    [ "InsertSailor", "class_sail_doc_1_1_logic_1_1_services_1_1_sailor_logic.html#a59ec2554dbacdc6fbccbfc2f64bea6df", null ],
    [ "UpdateSailor", "class_sail_doc_1_1_logic_1_1_services_1_1_sailor_logic.html#a03e60f8be719efb54592895b4645cf70", null ]
];