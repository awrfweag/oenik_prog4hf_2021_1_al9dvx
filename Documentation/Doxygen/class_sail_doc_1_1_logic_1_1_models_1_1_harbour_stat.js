var class_sail_doc_1_1_logic_1_1_models_1_1_harbour_stat =
[
    [ "Equals", "class_sail_doc_1_1_logic_1_1_models_1_1_harbour_stat.html#aaa054c10128c53c182958c34e9bb69df", null ],
    [ "GetHashCode", "class_sail_doc_1_1_logic_1_1_models_1_1_harbour_stat.html#a900c2effe9a4d9b3ad4feb11c637c8fb", null ],
    [ "ToString", "class_sail_doc_1_1_logic_1_1_models_1_1_harbour_stat.html#a73e7fa91d8fda0fa58fa1b834f814528", null ],
    [ "BoatNames", "class_sail_doc_1_1_logic_1_1_models_1_1_harbour_stat.html#adf2237a2b457a8ac954d51e92d96450a", null ],
    [ "HarbourName", "class_sail_doc_1_1_logic_1_1_models_1_1_harbour_stat.html#a889eab252e02b72945277dffadee2978", null ],
    [ "SailorNames", "class_sail_doc_1_1_logic_1_1_models_1_1_harbour_stat.html#a58787f5bd0aefe4fb878ade69142f816", null ]
];