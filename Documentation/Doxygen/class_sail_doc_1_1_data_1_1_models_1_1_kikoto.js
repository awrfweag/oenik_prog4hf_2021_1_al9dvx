var class_sail_doc_1_1_data_1_1_models_1_1_kikoto =
[
    [ "Kikoto", "class_sail_doc_1_1_data_1_1_models_1_1_kikoto.html#a6d0dd813c656e0f1d8171faab068f473", null ],
    [ "Equals", "class_sail_doc_1_1_data_1_1_models_1_1_kikoto.html#a3c0c7a22d6eeda0aba5f62cb0cb81e66", null ],
    [ "GetHashCode", "class_sail_doc_1_1_data_1_1_models_1_1_kikoto.html#a3f01b2ef8a8c1f165cf7e894f3e8bdca", null ],
    [ "ToString", "class_sail_doc_1_1_data_1_1_models_1_1_kikoto.html#a3ab6bf9976b77cc784e57d9a53c930a3", null ],
    [ "KikotoDij", "class_sail_doc_1_1_data_1_1_models_1_1_kikoto.html#a88803a83bfe6de24aeae5525b1125234", null ],
    [ "KikotoId", "class_sail_doc_1_1_data_1_1_models_1_1_kikoto.html#ac746f556c7810f126169fea7e37a04c1", null ],
    [ "Mosdo", "class_sail_doc_1_1_data_1_1_models_1_1_kikoto.html#aff7621b11952dee3989fd313d6dee2bd", null ],
    [ "Nev", "class_sail_doc_1_1_data_1_1_models_1_1_kikoto.html#a269e9d9b7cae9f1ff07c2b5196661159", null ],
    [ "UtvonalErkezesiKikoto", "class_sail_doc_1_1_data_1_1_models_1_1_kikoto.html#a215134f5ac599967f9c6f63715dfe5ad", null ],
    [ "UtvonalIndulasiKikoto", "class_sail_doc_1_1_data_1_1_models_1_1_kikoto.html#aae16f1bb96f3b8e7a329fdba510fa90d", null ],
    [ "VendegHelyDarab", "class_sail_doc_1_1_data_1_1_models_1_1_kikoto.html#a26c53ddc08577abd0d618cf13bd04d42", null ]
];