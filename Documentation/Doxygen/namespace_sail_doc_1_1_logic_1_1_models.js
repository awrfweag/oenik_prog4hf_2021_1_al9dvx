var namespace_sail_doc_1_1_logic_1_1_models =
[
    [ "HarbourStat", "class_sail_doc_1_1_logic_1_1_models_1_1_harbour_stat.html", "class_sail_doc_1_1_logic_1_1_models_1_1_harbour_stat" ],
    [ "ITrip", "interface_sail_doc_1_1_logic_1_1_models_1_1_i_trip.html", "interface_sail_doc_1_1_logic_1_1_models_1_1_i_trip" ],
    [ "SailorStat", "class_sail_doc_1_1_logic_1_1_models_1_1_sailor_stat.html", "class_sail_doc_1_1_logic_1_1_models_1_1_sailor_stat" ],
    [ "Trip", "class_sail_doc_1_1_logic_1_1_models_1_1_trip.html", "class_sail_doc_1_1_logic_1_1_models_1_1_trip" ],
    [ "TripDetails", "class_sail_doc_1_1_logic_1_1_models_1_1_trip_details.html", "class_sail_doc_1_1_logic_1_1_models_1_1_trip_details" ]
];