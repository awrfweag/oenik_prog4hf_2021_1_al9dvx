var interface_sail_doc_1_1_logic_1_1_interfaces_1_1_i_sailor_logic =
[
    [ "DeleteSailor", "interface_sail_doc_1_1_logic_1_1_interfaces_1_1_i_sailor_logic.html#a1d28b01039beec29f29c0a5e978efa2d", null ],
    [ "GetAllSailor", "interface_sail_doc_1_1_logic_1_1_interfaces_1_1_i_sailor_logic.html#a0e4c44557e413ad0aa432c6088aeeee6", null ],
    [ "GetSailorById", "interface_sail_doc_1_1_logic_1_1_interfaces_1_1_i_sailor_logic.html#ae55996c76108d14858610e4f617178af", null ],
    [ "InsertSailor", "interface_sail_doc_1_1_logic_1_1_interfaces_1_1_i_sailor_logic.html#ad3196ea25e2a8065e0b8f4552389a38e", null ],
    [ "UpdateSailor", "interface_sail_doc_1_1_logic_1_1_interfaces_1_1_i_sailor_logic.html#a5add19d3a5f834430c3de0f24760374d", null ]
];