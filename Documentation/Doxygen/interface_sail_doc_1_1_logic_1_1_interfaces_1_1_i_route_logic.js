var interface_sail_doc_1_1_logic_1_1_interfaces_1_1_i_route_logic =
[
    [ "DeleteRoute", "interface_sail_doc_1_1_logic_1_1_interfaces_1_1_i_route_logic.html#aba9cb7b7c375be3040003462d6d32e2a", null ],
    [ "GetAllRoute", "interface_sail_doc_1_1_logic_1_1_interfaces_1_1_i_route_logic.html#af08fdad7f96c1242dfdf0849aea02a77", null ],
    [ "GetRouteById", "interface_sail_doc_1_1_logic_1_1_interfaces_1_1_i_route_logic.html#a316c1e51778133f30c49035714397635", null ],
    [ "InsertRoute", "interface_sail_doc_1_1_logic_1_1_interfaces_1_1_i_route_logic.html#a8f4f4707d570150c22c4ded7f8d2ae6a", null ],
    [ "UpdateRoute", "interface_sail_doc_1_1_logic_1_1_interfaces_1_1_i_route_logic.html#a4763993d33186da626dc59cc17510dac", null ]
];