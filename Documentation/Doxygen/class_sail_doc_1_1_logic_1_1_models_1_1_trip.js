var class_sail_doc_1_1_logic_1_1_models_1_1_trip =
[
    [ "Equals", "class_sail_doc_1_1_logic_1_1_models_1_1_trip.html#a053461773f54cf0ccdd2663e019f1a99", null ],
    [ "GetHashCode", "class_sail_doc_1_1_logic_1_1_models_1_1_trip.html#a5dd3033231a05a8c3a4a0fff464827a5", null ],
    [ "ToString", "class_sail_doc_1_1_logic_1_1_models_1_1_trip.html#a93e5d51089a2358d7947c9b45d01b426", null ],
    [ "ArrivalHarbour", "class_sail_doc_1_1_logic_1_1_models_1_1_trip.html#ab7f771b46bcb48a1d1fc1b01e10c7e3e", null ],
    [ "DeparatureHarbour", "class_sail_doc_1_1_logic_1_1_models_1_1_trip.html#a2647860e6177fc6131f6ebcf525cbfe4", null ],
    [ "SailBoatName", "class_sail_doc_1_1_logic_1_1_models_1_1_trip.html#a7cc8d3368193ecbcdac82ec7dee7c550", null ],
    [ "SailorName", "class_sail_doc_1_1_logic_1_1_models_1_1_trip.html#ae470b1f15d43aaf1fd229e4cd2062dae", null ]
];