var interface_sail_doc_1_1_repository_1_1_interfaces_1_1_i_boat_repository =
[
    [ "SetBoatName", "interface_sail_doc_1_1_repository_1_1_interfaces_1_1_i_boat_repository.html#a8b7808a8ccacb1ea147761afcbca1fc4", null ],
    [ "SetDraft", "interface_sail_doc_1_1_repository_1_1_interfaces_1_1_i_boat_repository.html#a39cc35eba1759b325280313b1c33bcb5", null ],
    [ "SetRegistrationNumber", "interface_sail_doc_1_1_repository_1_1_interfaces_1_1_i_boat_repository.html#a9c528cd7967337324432cc4eeff44375", null ],
    [ "SetSBoatLength", "interface_sail_doc_1_1_repository_1_1_interfaces_1_1_i_boat_repository.html#adb0475975ad30694735f306c6e57aa8c", null ],
    [ "SetSeats", "interface_sail_doc_1_1_repository_1_1_interfaces_1_1_i_boat_repository.html#afafdcd1b88822614e6757ad2224d6447", null ]
];