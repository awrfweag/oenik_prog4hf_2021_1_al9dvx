var class_sail_doc_1_1_repository_1_1_repositories_1_1_boat_repository =
[
    [ "BoatRepository", "class_sail_doc_1_1_repository_1_1_repositories_1_1_boat_repository.html#a53c6e6bece4a7c2213d4e534714fc327", null ],
    [ "GetById", "class_sail_doc_1_1_repository_1_1_repositories_1_1_boat_repository.html#ab34d051e43945dff4f271ecae4b8fe74", null ],
    [ "SetBoatName", "class_sail_doc_1_1_repository_1_1_repositories_1_1_boat_repository.html#a91af254433830a63945a9b120c1db662", null ],
    [ "SetDraft", "class_sail_doc_1_1_repository_1_1_repositories_1_1_boat_repository.html#a34eaee7511f0ebd9156971f74a8a78a9", null ],
    [ "SetRegistrationNumber", "class_sail_doc_1_1_repository_1_1_repositories_1_1_boat_repository.html#affb2baa46758314966dbd05e74b20b1c", null ],
    [ "SetSBoatLength", "class_sail_doc_1_1_repository_1_1_repositories_1_1_boat_repository.html#a58e5181810c6ff0354f0cba330ded403", null ],
    [ "SetSeats", "class_sail_doc_1_1_repository_1_1_repositories_1_1_boat_repository.html#a07e7ed0d76f88043d388c4067df9b51f", null ]
];