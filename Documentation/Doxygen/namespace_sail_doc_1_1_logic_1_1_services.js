var namespace_sail_doc_1_1_logic_1_1_services =
[
    [ "BoatLogic", "class_sail_doc_1_1_logic_1_1_services_1_1_boat_logic.html", "class_sail_doc_1_1_logic_1_1_services_1_1_boat_logic" ],
    [ "HarbourLogics", "class_sail_doc_1_1_logic_1_1_services_1_1_harbour_logics.html", "class_sail_doc_1_1_logic_1_1_services_1_1_harbour_logics" ],
    [ "HarbourManagement", "class_sail_doc_1_1_logic_1_1_services_1_1_harbour_management.html", "class_sail_doc_1_1_logic_1_1_services_1_1_harbour_management" ],
    [ "RouteLogics", "class_sail_doc_1_1_logic_1_1_services_1_1_route_logics.html", "class_sail_doc_1_1_logic_1_1_services_1_1_route_logics" ],
    [ "SailorLogic", "class_sail_doc_1_1_logic_1_1_services_1_1_sailor_logic.html", "class_sail_doc_1_1_logic_1_1_services_1_1_sailor_logic" ],
    [ "SailorManagement", "class_sail_doc_1_1_logic_1_1_services_1_1_sailor_management.html", "class_sail_doc_1_1_logic_1_1_services_1_1_sailor_management" ],
    [ "TourLogic", "class_sail_doc_1_1_logic_1_1_services_1_1_tour_logic.html", "class_sail_doc_1_1_logic_1_1_services_1_1_tour_logic" ],
    [ "TripHandling", "class_sail_doc_1_1_logic_1_1_services_1_1_trip_handling.html", "class_sail_doc_1_1_logic_1_1_services_1_1_trip_handling" ]
];