var class_sail_doc_1_1_logic_1_1_services_1_1_route_logics =
[
    [ "RouteLogics", "class_sail_doc_1_1_logic_1_1_services_1_1_route_logics.html#a22a1df1c722e1fa2d98a2427b4f42c37", null ],
    [ "DeleteRoute", "class_sail_doc_1_1_logic_1_1_services_1_1_route_logics.html#afd01065e8ff3f0204727254d3dbc0af9", null ],
    [ "GetAllRoute", "class_sail_doc_1_1_logic_1_1_services_1_1_route_logics.html#ab4f4d6f5da26851fe518c558c683d271", null ],
    [ "GetRouteById", "class_sail_doc_1_1_logic_1_1_services_1_1_route_logics.html#af7a6f3aaa84159cc9afaa8dca0400f89", null ],
    [ "InsertRoute", "class_sail_doc_1_1_logic_1_1_services_1_1_route_logics.html#ae7fe2ec63355eb4553b20156cd6d10a1", null ],
    [ "UpdateRoute", "class_sail_doc_1_1_logic_1_1_services_1_1_route_logics.html#a193c8d9548dd564660bae4a5c46ebd78", null ]
];