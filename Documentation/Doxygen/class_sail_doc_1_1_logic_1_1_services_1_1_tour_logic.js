var class_sail_doc_1_1_logic_1_1_services_1_1_tour_logic =
[
    [ "TourLogic", "class_sail_doc_1_1_logic_1_1_services_1_1_tour_logic.html#a0125f0e0eb1e032ca6c0365bfbe4f945", null ],
    [ "DeleteTour", "class_sail_doc_1_1_logic_1_1_services_1_1_tour_logic.html#a85123da84ecee9bf4e8b2e44fdc98a19", null ],
    [ "GetAllTour", "class_sail_doc_1_1_logic_1_1_services_1_1_tour_logic.html#a7043418c6b9870f95866018d806b37b0", null ],
    [ "GetTourById", "class_sail_doc_1_1_logic_1_1_services_1_1_tour_logic.html#a7df14662da477be175855d9a6c571370", null ],
    [ "InsertTour", "class_sail_doc_1_1_logic_1_1_services_1_1_tour_logic.html#a81a2b1e79e24408f63e57e91bcd976d0", null ],
    [ "UpdateTour", "class_sail_doc_1_1_logic_1_1_services_1_1_tour_logic.html#ad7d556923434d8b9e798416c3bb3a463", null ]
];