var class_sail_doc_1_1_logic_1_1_models_1_1_trip_details =
[
    [ "Equals", "class_sail_doc_1_1_logic_1_1_models_1_1_trip_details.html#ac7b1d5a577a7bcece3075a582fb25afc", null ],
    [ "GetHashCode", "class_sail_doc_1_1_logic_1_1_models_1_1_trip_details.html#a0c5eef94c9fd1d5007e0f7a71b7cb057", null ],
    [ "ArrivalHarbour", "class_sail_doc_1_1_logic_1_1_models_1_1_trip_details.html#a6e501647e79bd8b578ee115121ec441f", null ],
    [ "DeparatureHarbour", "class_sail_doc_1_1_logic_1_1_models_1_1_trip_details.html#a0da27b110d48c79f9acb2a0a5818a8e6", null ],
    [ "SailBoatName", "class_sail_doc_1_1_logic_1_1_models_1_1_trip_details.html#a49391a695a69f05bb865508143484042", null ],
    [ "SailorName", "class_sail_doc_1_1_logic_1_1_models_1_1_trip_details.html#a5b919bd242cfb553e138969d621a959d", null ]
];