var class_sail_doc_1_1_logic_1_1_services_1_1_harbour_logics =
[
    [ "HarbourLogics", "class_sail_doc_1_1_logic_1_1_services_1_1_harbour_logics.html#a63b62c80b8fdfb40bb3cdc09a2e5ba02", null ],
    [ "DeleteHarbour", "class_sail_doc_1_1_logic_1_1_services_1_1_harbour_logics.html#a37bd03c90d640e4764e8f7df4562356a", null ],
    [ "GetAllHarbour", "class_sail_doc_1_1_logic_1_1_services_1_1_harbour_logics.html#a357fb01acffc8fbba5307cfefc090770", null ],
    [ "GetHarbourById", "class_sail_doc_1_1_logic_1_1_services_1_1_harbour_logics.html#a9b2dce8da167a33a45cea6fd019667c2", null ],
    [ "InsertHarbour", "class_sail_doc_1_1_logic_1_1_services_1_1_harbour_logics.html#a48ed6fbf3c7b73f93f6e554fb51a0c57", null ],
    [ "UpdateHarbour", "class_sail_doc_1_1_logic_1_1_services_1_1_harbour_logics.html#a417a5bc78a397fd66986bd579c89e2f2", null ]
];