var indexSectionsWithContent =
{
  0: "abcdefghiklmnoprstuv",
  1: "bdhikmprstu",
  2: "s",
  3: "abcdeghikorstu",
  4: "d",
  5: "abdefhiklmnstuv"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "namespaces",
  3: "functions",
  4: "variables",
  5: "properties"
};

var indexSectionLabels =
{
  0: "All",
  1: "Classes",
  2: "Namespaces",
  3: "Functions",
  4: "Variables",
  5: "Properties"
};

