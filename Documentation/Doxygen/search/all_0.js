var searchData=
[
  ['addnewsailortest_0',['AddNewSailorTest',['../class_sail_doc_1_1_test_1_1_sailor_management_tests.html#a953e0cc3495264584f277cd52d5ae9a0',1,'SailDoc::Test::SailorManagementTests']]],
  ['alltimeonwater_1',['AllTimeOnWater',['../class_sail_doc_1_1_logic_1_1_models_1_1_sailor_stat.html#a874124a4f8f9fde3a7353a5c7149a16e',1,'SailDoc::Logic::Models::SailorStat']]],
  ['arrivalharbour_2',['ArrivalHarbour',['../interface_sail_doc_1_1_logic_1_1_models_1_1_i_trip.html#a09ad3fb88709e2bc5a7d8a19f2fb2fbf',1,'SailDoc.Logic.Models.ITrip.ArrivalHarbour()'],['../class_sail_doc_1_1_logic_1_1_models_1_1_trip.html#ab7f771b46bcb48a1d1fc1b01e10c7e3e',1,'SailDoc.Logic.Models.Trip.ArrivalHarbour()'],['../class_sail_doc_1_1_logic_1_1_models_1_1_trip_details.html#a6e501647e79bd8b578ee115121ec441f',1,'SailDoc.Logic.Models.TripDetails.ArrivalHarbour()']]]
];
