var searchData=
[
  ['tripcount_332',['TripCount',['../class_sail_doc_1_1_logic_1_1_models_1_1_sailor_stat.html#adc688b8acda005d73ec0f92a70a6f7d1',1,'SailDoc::Logic::Models::SailorStat']]],
  ['tura_333',['Tura',['../class_sail_doc_1_1_data_1_1_models_1_1_sail_doc_context.html#aa24cf9f1f9ede64d31ae45e3118c1e89',1,'SailDoc::Data::Models::SailDocContext']]],
  ['turadatum_334',['TuraDatum',['../class_sail_doc_1_1_data_1_1_models_1_1_sail_doc_context.html#a2b5d5943d73d3b90d074198facc0b8f9',1,'SailDoc::Data::Models::SailDocContext']]],
  ['turaid_335',['TuraId',['../class_sail_doc_1_1_data_1_1_models_1_1_tura.html#a6a8f603c9b7e452d5ec5ac85b4602eea',1,'SailDoc.Data.Models.Tura.TuraId()'],['../class_sail_doc_1_1_data_1_1_models_1_1_tura_datum.html#aec48373dd8b7bece740dcff14267191a',1,'SailDoc.Data.Models.TuraDatum.TuraId()']]],
  ['turanavigation_336',['TuraNavigation',['../class_sail_doc_1_1_data_1_1_models_1_1_tura.html#a6cd805a3393440d1b17b8e8d11fad469',1,'SailDoc::Data::Models::Tura']]]
];
