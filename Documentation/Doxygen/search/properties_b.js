var searchData=
[
  ['sailboatname_329',['SailBoatName',['../interface_sail_doc_1_1_logic_1_1_models_1_1_i_trip.html#a400faff3715e693665f633789e68e422',1,'SailDoc.Logic.Models.ITrip.SailBoatName()'],['../class_sail_doc_1_1_logic_1_1_models_1_1_trip.html#a7cc8d3368193ecbcdac82ec7dee7c550',1,'SailDoc.Logic.Models.Trip.SailBoatName()'],['../class_sail_doc_1_1_logic_1_1_models_1_1_trip_details.html#a49391a695a69f05bb865508143484042',1,'SailDoc.Logic.Models.TripDetails.SailBoatName()']]],
  ['sailorname_330',['SailorName',['../class_sail_doc_1_1_logic_1_1_models_1_1_sailor_stat.html#aeca941a01016027266ec091c050ba04e',1,'SailDoc.Logic.Models.SailorStat.SailorName()'],['../class_sail_doc_1_1_logic_1_1_models_1_1_trip.html#ae470b1f15d43aaf1fd229e4cd2062dae',1,'SailDoc.Logic.Models.Trip.SailorName()'],['../class_sail_doc_1_1_logic_1_1_models_1_1_trip_details.html#a5b919bd242cfb553e138969d621a959d',1,'SailDoc.Logic.Models.TripDetails.SailorName()']]],
  ['sailornames_331',['SailorNames',['../class_sail_doc_1_1_logic_1_1_models_1_1_harbour_stat.html#a58787f5bd0aefe4fb878ade69142f816',1,'SailDoc::Logic::Models::HarbourStat']]]
];
