
#   $$$$$$\            $$\ $$\ $$$$$$$\                      
#  $$  __$$\           \__|$$ |$$  __$$\                     
#  $$ /  \__| $$$$$$\  $$\ $$ |$$ |  $$ | $$$$$$\   $$$$$$$\ 
#  \$$$$$$\   \____$$\ $$ |$$ |$$ |  $$ |$$  __$$\ $$  _____|
#   \____$$\  $$$$$$$ |$$ |$$ |$$ |  $$ |$$ /  $$ |$$ /      
#  $$\   $$ |$$  __$$ |$$ |$$ |$$ |  $$ |$$ |  $$ |$$ |      
#  \$$$$$$  |\$$$$$$$ |$$ |$$ |$$$$$$$  |\$$$$$$  |\$$$$$$$\ 
#   \______/  \_______|\__|\__|\_______/  \______/  \_______|
#                                                            
#                                                            
#                                                            

SailDoc is a sailboat trip logger application.

SailDoc Funtions:

- Sailboat -> create/read/update/delete
- Harbour-> create/read/update/delete
- Crew -> create/read/update/delete

- Create trip
- Show trips
- Sailors stats
- Harbour stats 