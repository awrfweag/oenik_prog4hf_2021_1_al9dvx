﻿namespace SailDoc.Test
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using Moq;
    using NUnit.Framework;
    using SailDoc.Data.Models;
    using SailDoc.Logic.Interfaces;
    using SailDoc.Logic.Models;
    using SailDoc.Logic.Services;
    using SailDoc.Repository.Interfaces;
    using SailDoc.Repository.Repositories;

    /// <summary>
    /// Harbour management tests.
    /// </summary>
    [TestFixture]
    public class HarbourManagementTests
    {
        /// <summary>
        /// Test getting boat.
        /// </summary>
        [Test]
        public void GetHarbourByIdTest()
        {
            Mock<IHarbourRepository> mockHarbour = new Mock<IHarbourRepository>();
            Kikoto harbour = new Kikoto() { KikotoId = 2, Nev = "TesztKikoto2", KikotoDij = 2000, Mosdo = false, VendegHelyDarab = 123 };
            mockHarbour.Setup(repo => repo.GetById(It.IsAny<int>())).Returns(harbour);

            HarbourLogics harbourLogics = new HarbourLogics(mockHarbour.Object);
            Kikoto kikoto = harbourLogics.GetHarbourById(2);

            Assert.That(kikoto, Is.EqualTo(harbour));
            mockHarbour.Verify(repo => repo.GetById(2), Times.Once);
        }

        /// <summary>
        /// Test deleting harbour.
        /// </summary>
        [Test]
        public void DeleteHarbourTest()
        {
            Mock<IHarbourRepository> mockHarbour = new Mock<IHarbourRepository>();
            Kikoto harbour = new Kikoto() { KikotoId = 3, Nev = "TesztKikoto2", KikotoDij = 200, Mosdo = false, VendegHelyDarab = 12 };
            mockHarbour.Setup(repo => repo.Delete(It.IsAny<Kikoto>()));

            HarbourLogics harbourLogics = new HarbourLogics(mockHarbour.Object);
            harbourLogics.DeleteHarbour(harbour);

            mockHarbour.Verify(repo => repo.Delete(harbour), Times.Once);
        }

        /// <summary>
        /// Harbour management tests.
        /// </summary>
        [Test]
        public void TestHarbourStatsWithInvalidData()
        {
            Mock<IHarbourManagement> mockHarbour = new Mock<IHarbourManagement>();
            Mock<IHarbourRepository> harbourRepo = new Mock<IHarbourRepository>();
            Mock<IBoatRepository> boatRepo = new Mock<IBoatRepository>();
            Mock<ITourRepository> tourRepo = new Mock<ITourRepository>();
            Mock<ISailorRepository> sailorRepo = new Mock<ISailorRepository>();
            Mock<IRouteRepository> routeRepo = new Mock<IRouteRepository>();

            List<Kikoto> harbours = new List<Kikoto>()
            {
                new Kikoto() { KikotoId = 0, Nev = "TesztHarbour1" },
                new Kikoto() { KikotoId = 1, Nev = "TesztHarbour2" },
            };

            List<Matroz> sailors = new List<Matroz>()
            {
                new Matroz() { MatrozId = 0, Nev = "Sailor2" },
                new Matroz() { MatrozId = 1, Nev = "Sailor1" },
            };

            List<Hajo> boats = new List<Hajo>()
            {
                new Hajo() { HajoId = 0, Nev = "TesztBoat1", Lajstromszam = "HA-13123" },
                new Hajo() { HajoId = 1, Nev = "TesztBoat2", Lajstromszam = "HA-13167" },
            };

            List<Tura> tours = new List<Tura>
            {
                new Tura() { HajoId = 0, MatrozId = 0, TuraId = 0, UtvonalId = 0 },
                new Tura() { HajoId = 1, MatrozId = 1, TuraId = 1, UtvonalId = 1 },
            };

            List<Utvonal> routes = new List<Utvonal>()
            {
                new Utvonal() { UtvonalId = 0, ErkezesiKikotoId = 0, IndulasiKikotoId = 1 },
                new Utvonal() { UtvonalId = 1, ErkezesiKikotoId = 0, IndulasiKikotoId = 1 },
            };

            List<HarbourStat> harbourStatList = new List<HarbourStat>()
            {
                new HarbourStat()
                {
                    HarbourName = harbours[0].Nev,
                    BoatNames = boats.Select(x => x.Nev).ToArray(),
                    SailorNames = sailors.Select(x => x.Nev).ToArray(),
                },
            };
            List<HarbourStat> expectedHarbourStats = new List<HarbourStat> { harbourStatList[0] };

            mockHarbour.Setup(repo => repo.CalculateHarbourStats()).Returns(harbourStatList);

            harbourRepo.Setup(repo => repo.GetAll()).Returns(harbours.AsQueryable);
            boatRepo.Setup(repo => repo.GetAll()).Returns(boats.AsQueryable);
            sailorRepo.Setup(repo => repo.GetAll()).Returns(sailors.AsQueryable);
            routeRepo.Setup(repo => repo.GetAll()).Returns(routes.AsQueryable);
            tourRepo.Setup(repo => repo.GetAll()).Returns(tours.AsQueryable);
            HarbourManagement harbourManagement = new HarbourManagement(tourRepo.Object, sailorRepo.Object, routeRepo.Object, harbourRepo.Object, boatRepo.Object);

            Assert.That(
                () => harbourManagement.CalculateHarbourStats(),
                Throws.TypeOf(typeof(NullReferenceException)));

            mockHarbour.Verify(repo => repo.CalculateHarbourStats(), Times.Never);
        }
    }
}
