﻿namespace SailDoc.Test
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using Moq;
    using NUnit.Framework;
    using SailDoc.Data.Models;
    using SailDoc.Logic.Services;
    using SailDoc.Repository.Interfaces;

    /// <summary>
    /// Sailor handling tests.
    /// </summary>
    [TestFixture]
    public class SailorManagementTests
    {
        /// <summary>
        /// Testing sailor insert.
        /// </summary>
        [Test]
        public void AddNewSailorTest()
        {
            Mock<ISailorRepository> mockSailor = new Mock<ISailorRepository>(MockBehavior.Loose);
            Matroz sailor = new Matroz() { MatrozId = 1, Nev = "Teszt Matroz" };
            mockSailor.Setup(repo => repo.Insert(It.IsAny<Matroz>())).Returns(sailor);
            SailorLogic sailorLogic = new SailorLogic(mockSailor.Object);

            Matroz matroz = sailorLogic.InsertSailor(sailor);

            Assert.That(matroz, Is.EqualTo(sailor));
            mockSailor.Verify(repo => repo.Insert(sailor), Times.Once);
        }

        /// <summary>
        /// Harbour test updating test.
        /// </summary>
        [Test]
        public void UpdateSailorTest()
        {
            Mock<ISailorRepository> mockSailor = new Mock<ISailorRepository>(MockBehavior.Loose);
            Matroz sailor = new Matroz() { MatrozId = 1, Nev = "Teszt Matroz" };
            mockSailor.Setup(repo => repo.Update(It.IsAny<Matroz>())).Returns(sailor);
            SailorLogic sailorLogic = new SailorLogic(mockSailor.Object);

            Matroz matroz = sailorLogic.UpdateSailor(sailor);

            mockSailor.Verify(repo => repo.Update(sailor), Times.Once);
        }
    }
}
