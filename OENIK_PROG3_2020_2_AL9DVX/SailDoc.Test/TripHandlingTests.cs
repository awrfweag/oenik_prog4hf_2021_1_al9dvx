﻿namespace SailDoc.Test
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Moq;
    using NUnit.Framework;
    using SailDoc.Data.Models;
    using SailDoc.Logic.Interfaces;
    using SailDoc.Logic.Models;
    using SailDoc.Logic.Services;
    using SailDoc.Repository.Interfaces;

    /// <summary>
    /// Trip handling tests.
    /// </summary>
    [TestFixture]
    public class TripHandlingTests
    {
        /// <summary>
        /// Trip mockdata.
        /// </summary>
        [Test]
        public void TestTripCreating()
        {
            Mock<ITripHandling> tripMocked = new Mock<ITripHandling>(MockBehavior.Loose);
            Mock<IHarbourRepository> harbourRepo = new Mock<IHarbourRepository>();
            Mock<IBoatRepository> boatRepo = new Mock<IBoatRepository>();
            Mock<ISailorRepository> sailorRepo = new Mock<ISailorRepository>();
            Mock<IRouteRepository> routeRepo = new Mock<IRouteRepository>();
            Mock<ITourRepository> tourRepo = new Mock<ITourRepository>();
            Mock<ITourDateRepository> tourDateRepo = new Mock<ITourDateRepository>();

            List<Kikoto> harbours = new List<Kikoto>()
            {
                new Kikoto() { KikotoId = 0, Nev = "TesztHarbour1" },
                new Kikoto() { KikotoId = 1, Nev = "TesztHarbour2" },
            };

            List<Matroz> sailors = new List<Matroz>()
            {
                new Matroz() { MatrozId = 0, Nev = "Sailor2" },
            };

            List<Hajo> boats = new List<Hajo>()
            {
                new Hajo() { HajoId = 0, Nev = "TesztBoat1", Lajstromszam = "HA-13123" },
            };

            List<TuraDatum> tourDate = new List<TuraDatum>()
            {
                new TuraDatum() { TuraId = 0, KezdeteDatum = new DateTime(2020, 12, 12), VegeDatum = new DateTime(2020, 12, 13) },
            };

            List<Tura> tours = new List<Tura>
            {
                new Tura() { HajoId = 0, MatrozId = 0, TuraId = 0, UtvonalId = 0 },
            };

            List<Utvonal> routes = new List<Utvonal>()
            {
                new Utvonal() { UtvonalId = 0, ErkezesiKikotoId = 0, IndulasiKikotoId = 1 },
            };

            Trip trip = new Trip()
            {
                ArrivalHarbour = harbours[1].Nev,
                DeparatureHarbour = harbours[0].Nev,
                SailBoatName = boats[0].Nev,
                SailorName = sailors[0].Nev,
            };

            Trip expectedTrip = new Trip()
            {
                ArrivalHarbour = harbours[1].Nev,
                DeparatureHarbour = harbours[0].Nev,
                SailBoatName = boats[0].Nev,
                SailorName = sailors[0].Nev,
            };

            boatRepo.Setup(repo => repo.GetById(It.IsAny<int>())).Returns(boats[0]);
            sailorRepo.Setup(repo => repo.GetById(It.IsAny<int>())).Returns(sailors[0]);
            harbourRepo.Setup(repo => repo.GetById(It.IsAny<int>())).Returns(harbours[0]);
            tourDateRepo.Setup(repo => repo.Insert(It.IsAny<TuraDatum>())).Returns(tourDate[0]);
            tourRepo.Setup(repo => repo.Insert(It.IsAny<Tura>())).Returns(tours[0]);
            routeRepo.Setup(repo => repo.Insert(It.IsAny<Utvonal>())).Returns(routes[0]);

            tripMocked.Setup(repo => repo.CreateTrip(It.IsAny<int>(), It.IsAny<int>(), It.IsAny<int>(), It.IsAny<int>(), It.IsAny<string>(), It.IsAny<string>())).Returns(trip);
            TripHandling tripHandling = new TripHandling(boatRepo.Object, sailorRepo.Object, harbourRepo.Object, routeRepo.Object, tourRepo.Object, tourDateRepo.Object);

            Trip createdTrip = tripHandling.CreateTrip(0, 0, 0, 1, "2020.12.12", "2020.12.13");

            Assert.That(createdTrip.SailorName, Is.EqualTo(expectedTrip.SailorName));
            Assert.That(createdTrip.SailBoatName, Is.EqualTo(expectedTrip.SailBoatName));
            boatRepo.Verify(repo => repo.GetById(0), Times.Once);
        }

        /// <summary>
        /// Showing trip comment.
        /// </summary>
        [Test]
        public void TestShowTrips()
        {
            Mock<ITripHandling> tripMocked = new Mock<ITripHandling>(MockBehavior.Loose);
            Mock<IHarbourRepository> harbourRepo = new Mock<IHarbourRepository>();
            Mock<IBoatRepository> boatRepo = new Mock<IBoatRepository>();
            Mock<ISailorRepository> sailorRepo = new Mock<ISailorRepository>();
            Mock<IRouteRepository> routeRepo = new Mock<IRouteRepository>();
            Mock<ITourRepository> tourRepo = new Mock<ITourRepository>();
            Mock<ITourDateRepository> tourDateRepo = new Mock<ITourDateRepository>();

            List<Kikoto> harbours = new List<Kikoto>()
            {
                new Kikoto() { KikotoId = 0, Nev = "TesztHarbour1" },
                new Kikoto() { KikotoId = 1, Nev = "TesztHarbour2" },
            };

            List<Matroz> sailors = new List<Matroz>()
            {
                new Matroz() { MatrozId = 0, Nev = "Sailor2" },
            };

            List<Hajo> boats = new List<Hajo>()
            {
                new Hajo() { HajoId = 0, Nev = "TesztBoat1", Lajstromszam = "HA-13123" },
            };

            List<TuraDatum> tourDate = new List<TuraDatum>()
            {
                new TuraDatum() { TuraId = 0, KezdeteDatum = new DateTime(2020, 12, 12), VegeDatum = new DateTime(2020, 12, 13) },
            };

            List<Tura> tours = new List<Tura>
            {
                new Tura() { HajoId = 0, MatrozId = 0, TuraId = 0, UtvonalId = 0 },
            };

            List<Utvonal> routes = new List<Utvonal>()
            {
                new Utvonal() { UtvonalId = 0, ErkezesiKikotoId = 0, IndulasiKikotoId = 1 },
            };

            List<TripDetails> trip = new List<TripDetails>()
            {
                new TripDetails
                {
                    ArrivalHarbour = harbours[0].Nev,
                    DeparatureHarbour = harbours[1].Nev,
                    SailBoatName = boats[0].Nev,
                    SailorName = sailors[0].Nev,
                },
            };

            List<TripDetails> expectedTrip = new List<TripDetails>()
            {
                new TripDetails
                {
                    ArrivalHarbour = harbours[0].Nev,
                    DeparatureHarbour = harbours[1].Nev,
                    SailBoatName = boats[0].Nev,
                    SailorName = sailors[0].Nev,
                },
            };

            TripStats tripStats = new TripStats()
            {
                TripDetails = trip,
            };

            harbourRepo.Setup(repo => repo.GetAll()).Returns(harbours.AsQueryable);
            boatRepo.Setup(repo => repo.GetAll()).Returns(boats.AsQueryable);
            sailorRepo.Setup(repo => repo.GetAll()).Returns(sailors.AsQueryable);
            tourDateRepo.Setup(repo => repo.GetAll()).Returns(tourDate.AsQueryable);
            tourRepo.Setup(repo => repo.GetAll()).Returns(tours.AsQueryable);
            routeRepo.Setup(repo => repo.GetAll()).Returns(routes.AsQueryable);

            tripMocked.Setup(repo => repo.ShowTrips()).Returns(tripStats);
            TripHandling tripHandling = new TripHandling(boatRepo.Object, sailorRepo.Object, harbourRepo.Object, routeRepo.Object, tourRepo.Object, tourDateRepo.Object);

            var showTrips = tripHandling.ShowTrips().TripDetails;

            Assert.That(showTrips, Is.EquivalentTo(expectedTrip));
            boatRepo.Verify(repo => repo.GetAll(), Times.Once);
        }
    }
}
