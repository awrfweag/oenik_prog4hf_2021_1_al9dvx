﻿namespace SailDoc.Test
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using Moq;
    using NUnit.Framework;
    using SailDoc.Data.Models;
    using SailDoc.Logic.Services;
    using SailDoc.Repository.Interfaces;

    /// <summary>
    /// Sailor management test.
    /// </summary>
    [TestFixture]
    public class BoatManagementTests
    {
        /// <summary>
        /// Test BoatRepository methods.
        /// </summary>
        [Test]
        public void GetAllBoatTest()
        {
            Mock<IBoatRepository> boatMocked = new Mock<IBoatRepository>(MockBehavior.Loose);

            List<Hajo> boats = new List<Hajo>()
            {
                new Hajo() { Nev = "TestBoat1", Ferohely = 1, Hajohossz = 1, Lajstromszam = "H-12121212", Merules = 2 },
                new Hajo() { Nev = "TestBoat2", Ferohely = 2, Hajohossz = 3, Lajstromszam = "H-1111111", Merules = 3 },
            };
            List<Hajo> expectetedBoats = new List<Hajo> { boats[0], boats[1] };
            boatMocked.Setup(repo => repo.GetAll()).Returns(boats.AsQueryable());
            BoatLogic boatLogic = new BoatLogic(boatMocked.Object);

            var allBoat = boatLogic.GettAllBoat();
            Assert.That(allBoat.Count, Is.EqualTo(expectetedBoats.Count));
            Assert.That(allBoat, Is.EquivalentTo(expectetedBoats));
            boatMocked.Verify(repo => repo.GetAll(), Times.Once);
        }
    }
}
