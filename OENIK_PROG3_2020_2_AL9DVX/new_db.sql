﻿CREATE TABLE Hajo (
    [Hajo_ID]      INT            NOT NULL IDENTITY(1,1),
    [Ferohely]     INT            NULL,
    [Lajstromszam] VARCHAR (30)   NOT NULL,
    [Merules]      NUMERIC (2, 1) NULL,
    [Nev]          VARCHAR (30)   NOT NULL,
    [Hajohossz]    INT            NULL,
    CONSTRAINT [hajo_pk] PRIMARY KEY CLUSTERED ([Hajo_ID] ASC),
    UNIQUE NONCLUSTERED ([Nev] ASC),
    UNIQUE NONCLUSTERED ([Lajstromszam] ASC)
);

CREATE TABLE KikotoNev (
    [Nev] VARCHAR (50) NOT NULL,
    [GPS] VARCHAR (50) NULL,
    CONSTRAINT [kikoto_nev_pk] PRIMARY KEY CLUSTERED ([Nev] ASC)
);

CREATE TABLE Kikoto (
    [Kikoto_ID]         INT          NOT NULL IDENTITY(1,1),
    [Mosdo]             BIT          DEFAULT ((0)) NULL,
    [Nev]               VARCHAR (50) NOT NULL,
    [Kikoto_dij]        INT          NULL,
    [Vendeg_hely_darab] INT          NULL,
    CONSTRAINT [kikoto_ID] PRIMARY KEY CLUSTERED ([Kikoto_ID] ASC),
    CONSTRAINT [kikoto_nev_fk] FOREIGN KEY ([Nev]) REFERENCES [KikotoNev] ([Nev])
);

CREATE TABLE MatrozNev (
    [Nev]              VARCHAR (50) NOT NULL,
    [Telefonszam]      BIGINT       NULL,
    [Szul_hely]        VARCHAR (30) NOT NULL,
    [Szul_datum]       DATE         NOT NULL,
    [Hajo_vezengedely] VARCHAR (20) NULL,
    CONSTRAINT [nev_pk] PRIMARY KEY CLUSTERED ([Nev] ASC),
    CONSTRAINT [matroz_tel] CHECK (len([Telefonszam])>=(10))
);

CREATE TABLE Matroz (
    [Matroz_ID] INT          NOT NULL IDENTITY(1,1),
    [Nev]       VARCHAR (50) NOT NULL,
    CONSTRAINT [matroz_ID_pk] PRIMARY KEY CLUSTERED ([Matroz_ID] ASC),
    CONSTRAINT [nev_fk] FOREIGN KEY ([Nev]) REFERENCES [MatrozNev] ([Nev])
);

CREATE TABLE TuraDatum (
    [Tura_ID]       INT NOT NULL IDENTITY(1,1),
    [Kezdete_datum] DATE NULL,
    [Vege_datum]    DATE NULL,
    CONSTRAINT [tura_ID_pk] PRIMARY KEY CLUSTERED ([Tura_ID] ASC)
);

CREATE TABLE Utvonal (
    [Utvonal_ID]         INT NOT NULL IDENTITY(1,1),
    [Indulasi_kikoto_ID] INT NOT NULL,
    [Erkezesi_kikoto_ID] INT NOT NULL,
    CONSTRAINT [utvonal_id_pk] PRIMARY KEY CLUSTERED ([Utvonal_ID] ASC),
    CONSTRAINT [indulasi_kikoto_ID_fk] FOREIGN KEY ([Indulasi_kikoto_ID]) REFERENCES [Kikoto] ([Kikoto_ID]),
    CONSTRAINT [erkezesi_kikoto_ID_fk] FOREIGN KEY ([Erkezesi_kikoto_ID]) REFERENCES [Kikoto] ([Kikoto_ID])
);

CREATE TABLE Tura(
    [Tura_ID]    INT NOT NULL,
    [Hajo_ID]    INT NOT NULL,
    [Utvonal_ID] INT NOT NULL,
    [Matroz_ID]  INT NOT NULL,
    CONSTRAINT [tura_id_fk] FOREIGN KEY ([Tura_ID]) REFERENCES [TuraDatum] ([Tura_ID]),
    CONSTRAINT [hajo_id_fk] FOREIGN KEY ([Hajo_ID]) REFERENCES [Hajo] ([Hajo_ID]),
    CONSTRAINT [utvonal_id_fk] FOREIGN KEY ([Utvonal_ID]) REFERENCES [Utvonal] ([Utvonal_ID]),
    CONSTRAINT [matroz_id_fk] FOREIGN KEY ([Matroz_ID]) REFERENCES [Matroz] ([Matroz_ID])
);

CREATE TABLE UtvonalErintettKikoto (
    [Utvonal_ID] INT NOT NULL,
    [Kikoto_ID]  INT NOT NULL,
    CONSTRAINT [utovonal_id_fk] FOREIGN KEY ([Utvonal_ID]) REFERENCES [Utvonal] ([Utvonal_ID]),
    CONSTRAINT [kikoto_id_fk] FOREIGN KEY ([Kikoto_ID]) REFERENCES [Kikoto] ([Kikoto_ID])
);

