﻿namespace SailDoc.UI.UI
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using SailDoc.UI.BL;
    using SailDoc.UI.Data;

    /// <summary>
    /// Editing throught window.
    /// </summary>
    public class EditorServiceWindow : IEditorService
    {
        /// <summary>
        /// Boat edit window.
        /// </summary>
        /// <param name="boat">Edited boat instance.</param>
        /// <returns>Is editing succes or fail through window.</returns>
        public bool EditBoat(Boat boat)
        {
            EditorWindow editorWindow = new EditorWindow(boat);
            return editorWindow.ShowDialog() ?? false;
        }
    }
}
