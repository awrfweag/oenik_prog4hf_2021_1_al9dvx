﻿namespace SailDoc.UI.VM
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows.Input;
    using CommonServiceLocator;
    using GalaSoft.MvvmLight;
    using GalaSoft.MvvmLight.Command;
    using SailDoc.UI.BL;
    using SailDoc.UI.Data;

    /// <summary>
    /// Main window.
    /// </summary>
    public class MainVM : ViewModelBase
    {
        private Boat selectedBoat;

        /// <summary>
        /// boat ui logic.
        /// </summary>
        private IBoatLogicUI boatLogicUI;

        /// <summary>
        /// Initializes a new instance of the <see cref="MainVM"/> class.
        /// </summary>
        /// <param name="boatLogicUI">DI BoatLogic class.</param>
        public MainVM(IBoatLogicUI boatLogicUI)
        {
            this.boatLogicUI = boatLogicUI;
            this.Boats = new ObservableCollection<Boat>();

            if (this.IsInDesignMode)
            {
                Boat boat1 = new Boat() { Nev = "Teszt MAIN VM BOAT", HajoId = 1, Lajstromszam = "HA1234" };
                this.Boats.Add(boat1);
            }
            else
            {
                this.boatLogicUI.GettAllBoat().ToList().ForEach(boat => this.Boats.Add(boat));
            }

            this.AddBoat = new RelayCommand(() => this.boatLogicUI.InsertBoat(this.Boats));
            this.UpdateBoat = new RelayCommand(() => this.boatLogicUI.UpdateBoat(this.SelectedBoat));
            this.DeleteBoat = new RelayCommand(() => this.boatLogicUI.DeleteBoat(this.Boats, this.SelectedBoat));
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="MainVM"/> class.
        /// </summary>
        public MainVM()
            : this(IsInDesignModeStatic ? null : ServiceLocator.Current.GetInstance<IBoatLogicUI>())
        {
        }

        /// <summary>
        /// UI boat list.
        /// </summary>
        public ObservableCollection<Boat> Boats { get; private set; }

        /// <summary>
        /// Currently selected boat from boat list.
        /// </summary>
        public Boat SelectedBoat
        {
            get { return this.selectedBoat; }
            set { this.Set(ref this.selectedBoat, value); }
        }

        /// <summary>
        /// UI add boat btn action handler.
        /// </summary>
        public ICommand AddBoat { get; private set; }

        /// <summary>
        /// UI delete boat btn action handler.
        /// </summary>
        public ICommand DeleteBoat { get; private set; }

        /// <summary>
        /// UI update boat btn action handler.
        /// </summary>
        public ICommand UpdateBoat { get; private set; }
    }
}
