﻿namespace SailDoc.UI.VM
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using GalaSoft.MvvmLight;
    using SailDoc.UI.Data;

    /// <summary>
    /// Edit VM.
    /// </summary>
    public class EditVM : ViewModelBase
    {
        private Boat boat;

        /// <summary>
        /// Initializes a new instance of the <see cref="EditVM"/> class.
        /// </summary>
        public EditVM()
        {
            this.boat = new Boat();
            if (this.IsInDesignMode)
            {
                this.boat.Nev = "WPF teszt boat name";
                this.boat.Lajstromszam = "WPF teszt boat lajtstrom";
            }
        }

        /// <summary>
        /// Boat UI instance.
        /// </summary>
        public Boat Boat
        {
            get { return this.boat; }
            set { this.Set(ref this.boat, value); }
        }
    }
}
