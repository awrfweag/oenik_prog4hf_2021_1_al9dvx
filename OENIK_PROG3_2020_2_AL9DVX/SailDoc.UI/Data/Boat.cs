﻿// <copyright file="Boat.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace SailDoc.UI.Data
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using GalaSoft.MvvmLight;

    /// <summary>
    /// Boat UI class.
    /// </summary>
    public class Boat : ObservableObject
    {
        private int hajoId;
        private string lajtstrom;
        private string nev;

        /// <summary>
        /// Boat unique id.
        /// </summary>
        public int HajoId
        {
            get { return this.hajoId; }
            set { this.Set(ref this.hajoId, value); }
        }

        /// <summary>
        /// Boat registration number.
        /// </summary>
        public string Lajstromszam
        {
            get { return this.lajtstrom; }
            set { this.Set(ref this.lajtstrom, value); }
        }

        /// <summary>
        /// Boat name.
        /// </summary>
        public string Nev
        {
            get { return this.nev; }
            set { this.Set(ref this.nev, value); }
        }

        /// <summary>
        /// We need this copy fn because of binding is irrevirsible.
        /// </summary>
        /// <param name="otherBoat">Other instancve.</param>
        public void ShallowBoatInstance(Boat otherBoat)
        {
            this.GetType().GetProperties().ToList().ForEach(prop => prop.SetValue(this, prop.GetValue(otherBoat)));
        }
    }
}
