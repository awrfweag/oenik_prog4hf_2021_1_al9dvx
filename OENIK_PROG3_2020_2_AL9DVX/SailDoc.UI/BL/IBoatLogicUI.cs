﻿// <copyright file="IBoatLogicUI.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace SailDoc.UI.BL
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using SailDoc.UI.Data;

    /// <summary>
    /// UI BoatLogic methods signatures.
    /// </summary>
    public interface IBoatLogicUI
    {
        /// <summary>
        /// Method signature for get all boats.
        /// </summary>
        /// <returns>Hajo list.</returns>
        IList<Boat> GettAllBoat();

        /// <summary>
        /// Insert new boat.
        /// </summary>
        /// <param name="boats">Boat instances.</param>
        void InsertBoat(IList<Boat> boats);

        /// <summary>
        /// Method signature for padate boat.
        /// </summary>
        /// <param name="boat">Boat instance.</param>
        void UpdateBoat(Boat boat);

        /// <summary>
        /// Method signature for delete a boat.
        /// </summary>
        /// <param name="boats">Boat instances.</param>
        /// <param name="boat">Deleted boat.</param>
        void DeleteBoat(IList<Boat> boats, Boat boat);
    }
}
