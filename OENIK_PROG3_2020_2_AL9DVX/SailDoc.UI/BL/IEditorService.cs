﻿namespace SailDoc.UI.BL
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using SailDoc.UI.Data;

    /// <summary>
    /// Separate UI BL layer from UI UI.
    /// </summary>
    public interface IEditorService
    {
        /// <summary>
        /// Boat edit will use DI.
        /// </summary>
        /// <param name="boat">Boat instance.</param>
        /// <returns>Editable or not.</returns>
        public bool EditBoat(Boat boat);
    }
}
