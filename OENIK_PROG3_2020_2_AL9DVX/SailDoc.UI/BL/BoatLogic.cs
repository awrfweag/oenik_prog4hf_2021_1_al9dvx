﻿namespace SailDoc.UI.BL
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Media;
    using System.Text;
    using System.Threading.Tasks;
    using GalaSoft.MvvmLight.Messaging;
    using SailDoc.Data.Models;
    using SailDoc.UI.Data;

    /// <summary>
    /// UI Boat handling.
    /// </summary>
    public class BoatLogic : IBoatLogicUI
    {
        /// <summary>
        /// Boat can be editable or not.
        /// </summary>
        private IEditorService editorService;

        /// <summary>
        /// Layer independent messenger service.
        /// </summary>
        private IMessenger messengerService;

        /// <summary>
        /// Boat factory.
        /// </summary>
        private DependencyFactory factory1;

        /// <summary>
        /// Initializes a new instance of the <see cref="BoatLogic"/> class.
        /// </summary>
        /// <param name="editorService">Editor Serice.</param>
        /// <param name="messengerService">Messanger Serice.</param>
        public BoatLogic(IEditorService editorService, IMessenger messengerService)
        {
            this.editorService = editorService;
            this.messengerService = messengerService;

            this.factory1 = new DependencyFactory();
        }

        /// <summary>
        /// Delete boat from UI and DB.
        /// </summary>
        /// <param name="boats">UI Boat list.</param>
        /// <param name="boat">Delete Boat.</param>
        public void DeleteBoat(IList<Boat> boats, Boat boat)
        {
            if (boat != null && boats?.Count != 0 && boats.Remove(boat))
            {
                var deleteBoat = this.factory1.BoatLogic().GettAllBoat().Where(x => x.Nev == boat.Nev).FirstOrDefault();
                this.factory1.BoatLogic().DeleteBoat(deleteBoat);
                SystemSounds.Exclamation.Play();
                this.messengerService.Send($"{deleteBoat.Nev} removed!", "LogicRes");
            }
            else
            {
                this.messengerService.Send($"Delete {boat?.Nev} failed!", "LogicRes");
            }
        }

        /// <summary>
        /// Get all boat from DB.
        /// </summary>
        /// <returns>Boats.</returns>
        public IList<Boat> GettAllBoat()
        {
            var boats = this.factory1.BoatLogic().GettAllBoat().ToList();
            IList<Boat> boatList = new List<Boat>();
            boats.ForEach(boat => boatList.Add(new Boat() { HajoId = boat.HajoId, Lajstromszam = boat.Lajstromszam, Nev = boat.Nev }));

            return boatList;
        }

        /// <summary>
        /// Add new to UI and DB.
        /// </summary>
        /// <param name="boats">Boat instance.</param>
        public void InsertBoat(IList<Boat> boats)
        {
            Boat boat = new Boat();
            if (this.editorService.EditBoat(boat) == true)
            {
                boats?.Add(boat);
                this.factory1.BoatLogic().InsertBoat(new Hajo() { Nev = boat.Nev, Lajstromszam = boat.Lajstromszam });
                this.messengerService.Send($"{boat.Nev} added!", "LogicRes");
            }
            else
            {
                SystemSounds.Beep.Play();
                this.messengerService.Send("Cancelled", "LogicRes");
            }
        }

        /// <summary>
        /// Upadate boat in the UI and in the DB.
        /// </summary>
        /// <param name="boat">Boat UI instance.</param>
        public void UpdateBoat(Boat boat)
        {
            if (boat == null)
            {
                this.messengerService.Send($"Failed to update", "LogicRes");
                return;
            }

            Boat copiedBoat = new Boat();
            copiedBoat.ShallowBoatInstance(boat);
            if (this.editorService.EditBoat(copiedBoat) == true)
            {
                var boat1 = this.factory1.BoatLogic().GettAllBoat().Where(x => x.Nev == boat.Nev).FirstOrDefault() ?? new Hajo();
                boat.ShallowBoatInstance(copiedBoat);
                boat1.Nev = boat.Nev;
                boat1.Lajstromszam = boat.Lajstromszam;
                this.factory1.BoatLogic().UpdateBoat(boat1);
                this.messengerService.Send($"{boat.Nev} updated!", "LogicRes");
            }
            else
            {
                this.messengerService.Send($"{boat.Nev} cancelled!", "LogicRes");
            }
        }
    }
}
