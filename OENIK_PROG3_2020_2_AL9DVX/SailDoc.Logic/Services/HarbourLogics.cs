﻿namespace SailDoc.Logic.Services
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using SailDoc.Data.Models;
    using SailDoc.Logic.Interfaces;
    using SailDoc.Repository.Interfaces;

    /// <summary>
    /// Harbour CRUD methods.
    /// </summary>
    public class HarbourLogics : IHarbourLogic
    {
        /// <summary>
        /// IHarbourRepository instace.
        /// </summary>
        private readonly IHarbourRepository harbourRepository;

        /// <summary>
        /// Initializes a new instance of the <see cref="HarbourLogics"/> class.
        /// The method who initializes a new instance will inject a IHarbourRepository dependency.
        /// </summary>
        /// <param name="harbourRepository">Harbourepository injection instace.</param>
        public HarbourLogics(IHarbourRepository harbourRepository)
        {
            this.harbourRepository = harbourRepository;
        }

        /// <summary>
        /// Delete harbour.
        /// </summary>
        /// <param name="harbour">Kikoto instance.</param>
        public void DeleteHarbour(Kikoto harbour)
        {
            this.harbourRepository.Delete(harbour);
        }

        /// <summary>
        /// Get all harbour.
        /// </summary>
        /// <returns>Kikoto list.</returns>
        public IList<Kikoto> GetAllHarbour()
        {
            return this.harbourRepository.GetAll().ToList();
        }

        /// <summary>
        /// Get harbour by id.
        /// </summary>
        /// <param name="id">Harbour id.</param>
        /// <returns>Kikoto instace.</returns>
        public Kikoto GetHarbourById(int id)
        {
            return this.harbourRepository.GetById(id);
        }

        /// <summary>
        /// Add new harbour.
        /// </summary>
        /// <param name="harbour">Kikoto instance.</param>
        /// <returns>Kikoto instace.</returns>
        public Kikoto InsertHarbour(Kikoto harbour)
        {
            return this.harbourRepository.Insert(harbour);
        }

        /// <summary>
        /// Update harbour.
        /// </summary>
        /// <param name="harbour">Kikoto instace.</param>
        /// <returns>Updated Kikoto instace.</returns>
        public Kikoto UpdateHarbour(Kikoto harbour)
        {
            return this.harbourRepository.Update(harbour);
        }
    }
}
