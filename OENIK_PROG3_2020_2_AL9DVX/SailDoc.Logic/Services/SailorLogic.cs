﻿namespace SailDoc.Logic.Services
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using SailDoc.Data.Models;
    using SailDoc.Logic.Interfaces;
    using SailDoc.Repository.Interfaces;

    /// <summary>
    /// Matroz CRUD methods.
    /// </summary>
    public class SailorLogic : ISailorLogic
    {
        /// <summary>
        /// Variable for ISailor logic.
        /// </summary>
        private readonly ISailorRepository sailorRepository;

        /// <summary>
        /// Initializes a new instance of the <see cref="SailorLogic"/> class.
        /// The method who initializes a new instance will inject a ISailorRepository dependency.
        /// </summary>
        /// <param name="sailorRepository">Repository instance.</param>
        public SailorLogic(ISailorRepository sailorRepository)
        {
            this.sailorRepository = sailorRepository;
        }

        /// <summary>
        /// Delete sailor.
        /// </summary>
        /// <param name="sailor">Matroz instace.</param>
        public void DeleteSailor(Matroz sailor)
        {
            this.sailorRepository.Delete(sailor);
        }

        /// <summary>
        /// Geta all sailor.
        /// </summary>
        /// <returns>Matoz list.</returns>
        public IList<Matroz> GetAllSailor()
        {
            return this.sailorRepository.GetAll().ToList();
        }

        /// <summary>
        /// Get sailor by id.
        /// </summary>
        /// <param name="id">Matroz id.</param>
        /// <returns>Matroz instance.</returns>
        public Matroz GetSailorById(int id)
        {
            return this.sailorRepository.GetById(id);
        }

        /// <summary>
        /// Add new sailor.
        /// </summary>
        /// <param name="sailor">Matroz instance.</param>
        /// <returns>New Matroz instance.</returns>
        public Matroz InsertSailor(Matroz sailor)
        {
            return this.sailorRepository.Insert(sailor);
        }

        /// <summary>
        /// Update sailor.
        /// </summary>
        /// <param name="sailor">Matroz instance.</param>
        /// <returns>Updated Sailor instance.</returns>
        public Matroz UpdateSailor(Matroz sailor)
        {
            return this.sailorRepository.Update(sailor);
        }
    }
}
