﻿// namespace SailDoc.Logic.Services
// {
//    using System;
//    using System.Collections.Generic;
//    using System.Linq;
//    using System.Text;
//    using SailDoc.Data.Models;
//    using SailDoc.Logic.Interfaces;
//    using SailDoc.Repository.Interfaces;

// /// <summary>
//    /// MatrozNev base CRUD methods.
//    /// </summary>
//    public class SailorNameLogics : ISailorNameLogic
//    {
//        /// <summary>
//        /// Variable SailorNameRepository.
//        /// </summary>
//        private readonly ISailorNameRepository iSailorNameLogic;

// /// <summary>
//        /// Initializes a new instance of the <see cref="SailorNameLogics"/> class.
//        /// The method who initializes a new instance will inject a ISailorNameRepository dependency.
//        /// </summary>
//        /// <param name="iSailorNameLogic">Repository instance.</param>
//        public SailorNameLogics(ISailorNameRepository iSailorNameLogic)
//        {
//            this.iSailorNameLogic = iSailorNameLogic;
//        }

// /// <summary>
//        /// Delete MatrozNev.
//        /// </summary>
//        /// <param name="sailorName">MatrozNev instance.</param>
//        public void DeleteMatrozNev(MatrozNev sailorName)
//        {
//            this.iSailorNameLogic.Delete(sailorName);
//        }

// /// <summary>
//        /// Get all MatrozNev.
//        /// </summary>
//        /// <returns>MatrozNev list.</returns>
//        public IList<MatrozNev> GetAllSailorName()
//        {
//            return this.iSailorNameLogic.GetAll().ToList();
//        }

// /// <summary>
//        /// Get MatrozNev by id.
//        /// </summary>
//        /// <param name="name">Name as id.</param>
//        /// <returns>MatrozNev instace.</returns>
//        public MatrozNev GetSailorNameByName(string name)
//        {
//            return this.iSailorNameLogic.GetById(name);
//        }

// /// <summary>
//        /// Add new MatrozNev instance.
//        /// </summary>
//        /// <param name="sailorName">MatrozNev instance.</param>
//        /// <returns>New MatrozNev instance.</returns>
//        public MatrozNev InsertSailorName(MatrozNev sailorName)
//        {
//            return this.iSailorNameLogic.Insert(sailorName);
//        }

// /// <summary>
//        /// Update MatrozNev.
//        /// </summary>
//        /// <param name="sailorName">MatrozNev instance.</param>
//        /// <returns>Updated MatrozNev instance.</returns>
//        public MatrozNev UpdateMatrozNev(MatrozNev sailorName)
//        {
//            return this.iSailorNameLogic.Update(sailorName);
//        }
//    }
// }
