﻿namespace SailDoc.Logic.Services
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using SailDoc.Data.Models;
    using SailDoc.Logic.Interfaces;
    using SailDoc.Repository.Interfaces;

    /// <summary>
    /// CRUD methods for route.
    /// </summary>
    public class RouteLogics : IRouteLogic
    {
        private readonly IRouteRepository routeRepository;

        /// <summary>
        /// Initializes a new instance of the <see cref="RouteLogics"/> class.
        /// The method who initializes a new instance will inject a IBoatRepository dependency.
        /// </summary>
        /// <param name="routeRepository">RouteRepository instance.</param>
        public RouteLogics(IRouteRepository routeRepository)
        {
            this.routeRepository = routeRepository;
        }

        /// <summary>
        /// Delete route.
        /// </summary>
        /// <param name="route">Utvonal instance.</param>
        public void DeleteRoute(Utvonal route)
        {
            this.routeRepository.Delete(route);
        }

        /// <summary>
        /// Get all route.
        /// </summary>
        /// <returns>Utvonal list.</returns>
        public IList<Utvonal> GetAllRoute()
        {
            return this.routeRepository.GetAll().ToList();
        }

        /// <summary>
        /// Get route by id.
        /// </summary>
        /// <param name="id">Route id.</param>
        /// <returns>Utvonal instance.</returns>
        public Utvonal GetRouteById(int id)
        {
            return this.routeRepository.GetById(id);
        }

        /// <summary>
        /// Add new route.
        /// </summary>
        /// <param name="route">Route instance.</param>
        /// <returns>New route instance.</returns>
        public Utvonal InsertRoute(Utvonal route)
        {
            return this.routeRepository.Insert(route);
        }

        /// <summary>
        /// Update route.
        /// </summary>
        /// <param name="route">Utvonal instance.</param>
        /// <returns>Updated utvonal instance.</returns>
        public Utvonal UpdateRoute(Utvonal route)
        {
            return this.routeRepository.Update(route);
        }
    }
}
