﻿namespace SailDoc.Logic.Services
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using SailDoc.Logic.Interfaces;
    using SailDoc.Logic.Models;
    using SailDoc.Repository.Interfaces;

    /// <summary>
    /// Methods for handling harbours.
    /// </summary>
    public class HarbourManagement : IHarbourManagement
    {
        private readonly ITourRepository tourRepository;
        private readonly ISailorRepository sailorRepository;
        private readonly IRouteRepository routeRepository;
        private readonly IHarbourRepository harbourRepository;
        private readonly IBoatRepository boatRepository;

        /// <summary>
        /// Initializes a new instance of the <see cref="HarbourManagement"/> class.
        /// </summary>
        /// <param name="tourRepository">TourRepository instance.</param>
        /// <param name="sailorRepository">SailorRepository instance.</param>
        /// <param name="routeRepository">RouteRepository instance.</param>
        /// <param name="harbourRepository">HarbourRepository instance.</param>
        /// <param name="boatRepository">BoatRepository instance.</param>
        public HarbourManagement(ITourRepository tourRepository, ISailorRepository sailorRepository, IRouteRepository routeRepository, IHarbourRepository harbourRepository, IBoatRepository boatRepository)
        {
            this.tourRepository = tourRepository;
            this.sailorRepository = sailorRepository;
            this.routeRepository = routeRepository;
            this.harbourRepository = harbourRepository;
            this.boatRepository = boatRepository;
        }

        /// <inheritdoc/>
        public IList<HarbourStat> CalculateHarbourStats()
        {
            var harbourDetails = from tour in this.tourRepository.GetAll()
                                  join route in this.routeRepository.GetAll() on tour.UtvonalId equals route.UtvonalId
                                  join boat in this.boatRepository.GetAll() on tour.HajoId equals boat.HajoId
                                  join harbourArr in this.harbourRepository.GetAll() on route.ErkezesiKikotoId equals harbourArr.KikotoId
                                  join harbourDep in this.harbourRepository.GetAll() on route.IndulasiKikotoId equals harbourDep.KikotoId
                                  join sailor in this.sailorRepository.GetAll() on tour.MatrozId equals sailor.MatrozId
                                  select new
                                  {
                                      HarbourIdArr = harbourArr.KikotoId,
                                      HarbourIdDep = harbourDep.KikotoId,
                                      SailorName = sailor.Nev,
                                      BoatName = boat.Nev,
                                  };

            var grouppedByHarbourArr = from harbourDetail in harbourDetails.ToList()
                                       group harbourDetail by harbourDetail.HarbourIdArr into grouppedArr
                                       select new HarbourStat()
                                       {
                                           HarbourName = this.harbourRepository.GetById(grouppedArr.Key).Nev,
                                           SailorNames = grouppedArr.Select(x => x.SailorName).ToArray(),
                                           BoatNames = grouppedArr.Select(x => x.BoatName).ToArray(),
                                       };

            var grouppedByHarbourDep = from harbourDetail in harbourDetails.ToList()
                                       group harbourDetail by harbourDetail.HarbourIdDep into grouppedDep
                                       select new HarbourStat()
                                       {
                                           HarbourName = this.harbourRepository.GetById(grouppedDep.Key).Nev,
                                           SailorNames = grouppedDep.Select(x => x.SailorName).ToArray(),
                                           BoatNames = grouppedDep.Select(x => x.BoatName).ToArray(),
                                       };

            IList<HarbourStat> allHarbour = new List<HarbourStat>();

            foreach (var item in grouppedByHarbourArr)
            {
                allHarbour.Add(item);
            }

            foreach (var item in grouppedByHarbourDep)
            {
                allHarbour.Add(item);
            }

            return allHarbour.GroupBy(x => x.HarbourName).Select(x => new HarbourStat()
            {
                HarbourName = x.Key,
                BoatNames = x.SelectMany(x => x.BoatNames).ToArray(),
                SailorNames = x.SelectMany(x => x.SailorNames).ToArray(),
            }).ToList();
        }

        /// <summary>
        /// <inheritdoc/>
        /// </summary>
        /// <returns>A <see cref="Task"/> representing the asynchronous operation.</returns>
        public Task<IList<HarbourStat>> CalculateHarbourStatsAsync()
        {
            return Task.Run(() => this.CalculateHarbourStats());
        }
    }
}
