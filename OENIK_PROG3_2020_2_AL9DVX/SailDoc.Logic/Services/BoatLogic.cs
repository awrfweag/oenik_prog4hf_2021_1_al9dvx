﻿namespace SailDoc.Logic.Services
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using SailDoc.Data.Models;
    using SailDoc.Logic.Interfaces;
    using SailDoc.Repository.Interfaces;

    /// <summary>
    /// Boat logic instace.
    /// </summary>
    public class BoatLogic : IBoatLogic
    {
        /// <summary>
        /// Variable for IBoatRepository.
        /// </summary>
        private readonly IBoatRepository boatRepo;

        /// <summary>
        /// Initializes a new instance of the <see cref="BoatLogic"/> class.
        /// The method who initializes a new instance will inject a IBoatRepository dependency.
        /// </summary>
        /// <param name="boatRepository">BoatRepository instace.</param>
        public BoatLogic(IBoatRepository boatRepository)
        {
            this.boatRepo = boatRepository;
        }

        /// <summary>
        /// Add new boat.
        /// </summary>
        /// <param name="boat">Boat instance.</param>
        /// <returns>Boat.</returns>
        public Hajo InsertBoat(Hajo boat)
        {
            return this.boatRepo.Insert(boat);
        }

        /// <summary>
        /// Get all boat from database.
        /// </summary>
        /// <returns>Boat list.</returns>
        public IList<Hajo> GettAllBoat()
        {
            return this.boatRepo.GetAll().ToList();
        }

        /// <summary>
        /// Get a boat instance by id.
        /// </summary>
        /// <param name="id">Boat id.</param>
        /// <returns>Boat instance.</returns>
        public Hajo GetHajo(int id)
        {
            return this.boatRepo.GetById(id);
        }

        /// <summary>
        /// Update a boat instance.
        /// </summary>
        /// <param name="boat">Boat instance.</param>
        /// <returns>Hajo instance.</returns>
        public Hajo UpdateBoat(Hajo boat)
        {
            return this.boatRepo.Update(boat);
        }

        /// <summary>
        /// Delete a boat.
        /// </summary>
        /// <param name="boat">Hajo instance.</param>
        public void DeleteBoat(Hajo boat)
        {
            this.boatRepo.Delete(boat);
        }

        // TODO: implement equals and gethashcode and tostring
    }
}
