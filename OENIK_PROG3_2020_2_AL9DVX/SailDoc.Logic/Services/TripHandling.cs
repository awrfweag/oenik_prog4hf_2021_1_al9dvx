﻿namespace SailDoc.Logic.Services
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using Microsoft.EntityFrameworkCore;
    using SailDoc.Data.Models;
    using SailDoc.Logic.Interfaces;
    using SailDoc.Logic.Models;
    using SailDoc.Repository.Interfaces;

    /// <summary>
    /// Trip creating.
    /// </summary>
    public class TripHandling : ITripHandling
    {
        private readonly IBoatRepository boatRepository;
        private readonly ISailorRepository sailorRepository;
        private readonly IHarbourRepository harbourRepository;
        private readonly IRouteRepository routeRepository;
        private readonly ITourRepository tourRepository;
        private readonly ITourDateRepository tourDateRepository;

        /// <summary>
        /// Initializes a new instance of the <see cref="TripHandling"/> class.
        /// </summary>
        /// <param name="boatRepository">Boat repository instance.</param>
        /// <param name="sailorRepository">Sailor repository instance.</param>
        /// <param name="harbourRepository">Harbour repository instance.</param>
        /// <param name="routeRepository">Route repository instance.</param>
        /// <param name="tourRepository">Tour repository instance.</param>
        /// <param name="tourDateRepository">TourDate repository instance.</param>
        public TripHandling(IBoatRepository boatRepository, ISailorRepository sailorRepository, IHarbourRepository harbourRepository, IRouteRepository routeRepository, ITourRepository tourRepository, ITourDateRepository tourDateRepository)
        {
            this.boatRepository = boatRepository;
            this.sailorRepository = sailorRepository;
            this.harbourRepository = harbourRepository;
            this.routeRepository = routeRepository;
            this.tourRepository = tourRepository;
            this.tourDateRepository = tourDateRepository;
        }

        /// <summary>
        /// Create trip method.
        /// </summary>
        /// <param name="boatId">Boat used under the trip.</param>
        /// <param name="sailorId">Sailor were take part in that trip.</param>
        /// <param name="depHarbourId">Deparature harbour.</param>
        /// <param name="arrHarbourId">Arrival harbour.</param>
        /// <param name="depDate">Trip deparature date.</param>
        /// <param name="arrDate">Trip arrival date.</param>
        /// <returns>Trip insatance.</returns>
        public Trip CreateTrip(int boatId, int sailorId, int depHarbourId, int arrHarbourId, string depDate, string arrDate)
        {
            Hajo boat = this.boatRepository.GetById(boatId);
            Matroz sailor = this.sailorRepository.GetById(sailorId);
            Kikoto depHarbour = this.harbourRepository.GetById(depHarbourId);
            Kikoto arrHarbour = this.harbourRepository.GetById(arrHarbourId);

            DateTime deparatureDate = new DateTime(int.Parse(depDate.Split('.')[0]), int.Parse(depDate.Split('.')[1]), int.Parse(depDate.Split('.')[2]));
            DateTime arrivalDate = new DateTime(int.Parse(arrDate.Split('.')[0]), int.Parse(arrDate.Split('.')[1]), int.Parse(arrDate.Split('.')[2]));

            Utvonal route = this.routeRepository.Insert(new Utvonal()
            {
                IndulasiKikotoId = depHarbour.KikotoId,
                ErkezesiKikotoId = arrHarbour.KikotoId,
            });

            TuraDatum tourDate = this.tourDateRepository.Insert(new TuraDatum()
            {
                KezdeteDatum = deparatureDate,
                VegeDatum = arrivalDate,
            });

            Tura trip = this.tourRepository.Insert(new Tura()
            {
                TuraId = tourDate.TuraId,
                HajoId = boat.HajoId,
                MatrozId = sailor.MatrozId,
                UtvonalId = route.UtvonalId,
            });

            return new Trip() { SailBoatName = boat.Nev, SailorName = sailor.Nev, DeparatureHarbour = depHarbour.Nev, ArrivalHarbour = arrHarbour.Nev };
        }

        /// <summary>
        /// Give a detailed view about trips.
        /// </summary>
        /// <returns>TripDetails list.</returns>
        public TripStats ShowTrips()
        {
            IList<TripDetails> tripDetails = new List<TripDetails>();

            var trips = from trip in this.tourRepository.GetAll()
                        join tourDate in this.tourDateRepository.GetAll() on trip.TuraId equals tourDate.TuraId
                        join boat in this.boatRepository.GetAll() on trip.HajoId equals boat.HajoId
                        join sailor in this.sailorRepository.GetAll() on trip.MatrozId equals sailor.MatrozId
                        join route in this.routeRepository.GetAll() on trip.UtvonalId equals route.UtvonalId
                        join arrH in this.harbourRepository.GetAll() on route.ErkezesiKikotoId equals arrH.KikotoId
                        join depH in this.harbourRepository.GetAll() on route.IndulasiKikotoId equals depH.KikotoId
                        select new TripDetails()
                        {
                            SailBoatName = boat.Nev,
                            SailorName = sailor.Nev,
                            ArrivalHarbour = arrH.Nev,
                            DeparatureHarbour = depH.Nev,
                            SailTime = EF.Functions.DateDiffDay(tourDate.KezdeteDatum.Value, tourDate.VegeDatum.Value),
                        };

            // group sailor by sailor.MatrozId into groupped
            var groupBySailor = from trip in trips
                                group trip by trip.SailorName into groupped
                                select new SailorStat()
                                {
                                    SailorName = groupped.Key,
                                    AllTimeOnWater = groupped.Average(x => x.SailTime),
                                };

            var avarageTripTime = trips.Average(x => x.SailTime);

            return new TripStats() { TripDetails = trips.ToList(),  AvgTripTime = avarageTripTime, SailorStats = groupBySailor.ToList() };
        }

        /// <summary>
        /// <inheritdoc/>
        /// </summary>
        /// <returns>A <see cref="Task"/> representing the asynchronous operation.</returns>
        public Task<TripStats> ShowTripsAsync()
        {
            return Task.Run(() => this.ShowTrips());
        }
    }
}
