﻿namespace SailDoc.Logic.Services
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using SailDoc.Logic.Interfaces;
    using SailDoc.Logic.Models;
    using SailDoc.Repository.Interfaces;
    using SailDoc.Repository.Repositories;

    /// <summary>
    /// Sailor methods.
    /// </summary>
    public class SailorManagement : ISailorManagement
    {
        private readonly ITourRepository tourRepository;
        private readonly ISailorRepository sailorRepository;
        private readonly ITourDateRepository tourDateRepository;

        /// <summary>
        /// Initializes a new instance of the <see cref="SailorManagement"/> class.
        /// </summary>
        /// <param name="tourRepository">TourRepository instance.</param>
        /// <param name="sailorRepository">SailorRepository instance.</param>
        /// <param name="tourDateRepository">TourDateRepository instance.</param>
        public SailorManagement(ITourRepository tourRepository, ISailorRepository sailorRepository, ITourDateRepository tourDateRepository)
        {
            this.tourRepository = tourRepository;
            this.sailorRepository = sailorRepository;
            this.tourDateRepository = tourDateRepository;
        }

        /// <summary>
        /// Calculating sailor's stats.
        /// </summary>
        /// <returns>List of Sailor Stat.</returns>
        public IList<SailorStat> CalculateSailorStats()
        {
            IList<SailorStat> sailorStats = new List<SailorStat>();

            var sailorsToursDetails = from sailor in this.sailorRepository.GetAll()
                                      join tours in this.tourRepository.GetAll() on sailor.MatrozId equals tours.MatrozId
                                      join routes in this.tourDateRepository.GetAll() on tours.TuraId equals routes.TuraId
                                      select new
                                      {
                                          SailorId = sailor.MatrozId,
                                          SailorName = sailor.Nev,
                                          DaysOnTour = (routes.VegeDatum.Value - routes.KezdeteDatum.Value).Days,
                                      };

            var stats = from stat in sailorsToursDetails.ToList()
                        group stat by stat.SailorId into groupped
                        select new SailorStat()
                        {
                            AllTimeOnWater = groupped.Sum(x => x.DaysOnTour),
                            TripCount = groupped.Count(),
                            SailorName = groupped.Select(x => x.SailorName).FirstOrDefault(),
                        };

             // https://stackoverflow.com/questions/58138556/client-side-groupby-is-not-supported
             // var f = sailorsToursDetails.ToList().GroupBy(x => x.SailorId).Select(y => new { y.Key, Days = y.Count() }).ToDictionary( d => d.Key, d => d.Days);
            return sailorStats = stats.ToList();
        }

        /// <summary>
        /// Calculating sailor's stats async.
        /// </summary>
        /// <returns>List of sailors.</returns>
        public Task<IList<SailorStat>> CalculateSailorStatsAsync()
        {
            return Task.Run(() => this.CalculateSailorStats());
        }
    }
}
