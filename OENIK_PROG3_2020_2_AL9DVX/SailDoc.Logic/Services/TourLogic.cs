﻿namespace SailDoc.Logic.Services
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using SailDoc.Data.Models;
    using SailDoc.Logic.Interfaces;
    using SailDoc.Repository.Interfaces;

    /// <summary>
    /// CRUD methods for tour.
    /// </summary>
    public class TourLogic : ITourLogics
    {
        /// <summary>
        /// tour reposiotry.
        /// </summary>
        private readonly ITourRepository tourRepository;

        /// <summary>
        /// Initializes a new instance of the <see cref="TourLogic"/> class.
        /// The method who initializes a new instance will inject a ITourRepository dependency.
        /// </summary>
        /// <param name="tourRepository">Tour repository instance.</param>
        public TourLogic(ITourRepository tourRepository)
        {
            this.tourRepository = tourRepository;
        }

        /// <summary>
        /// Delete tour.
        /// </summary>
        /// <param name="tour">Tura instance.</param>
        public void DeleteTour(Tura tour)
        {
            this.tourRepository.Delete(tour);
        }

        /// <summary>
        /// Get all tour.
        /// </summary>
        /// <returns>Tura list.</returns>
        public IList<Tura> GetAllTour()
        {
            return this.tourRepository.GetAll().ToList();
        }

        /// <summary>
        /// Get tour by id.
        /// </summary>
        /// <param name="id">Tura id.</param>
        /// <returns>Tura instance.</returns>
        public Tura GetTourById(int id)
        {
            return this.tourRepository.GetById(id);
        }

        /// <summary>
        /// Add tour.
        /// </summary>
        /// <param name="tour">Tura instance.</param>
        /// <returns>New Tura instance.</returns>
        public Tura InsertTour(Tura tour)
        {
            return this.tourRepository.Insert(tour);
        }

        /// <summary>
        /// Update tour.
        /// </summary>
        /// <param name="tour">Tura instance.</param>
        /// <returns>Updated Tura instnace.</returns>
        public Tura UpdateTour(Tura tour)
        {
            return this.tourRepository.Update(tour);
        }
    }
}
