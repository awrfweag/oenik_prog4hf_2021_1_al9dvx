﻿namespace SailDoc.Logic.Interfaces
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Text;
    using SailDoc.Data.Models;

    /// <summary>
    /// Method signatures for Utvonal CRUD methods.
    /// </summary>
    public interface IRouteLogic
    {
        /// <summary>
        /// Get all route.
        /// </summary>
        /// <returns>Utvonal list.</returns>
        IList<Utvonal> GetAllRoute();

        /// <summary>
        /// Get route by id.
        /// </summary>
        /// <param name="id">Utvonal id.</param>
        /// <returns>Utvonal instance.</returns>
        Utvonal GetRouteById(int id);

        /// <summary>
        /// Add route.
        /// </summary>
        /// <param name="route">New Utvonal instance.</param>
        /// <returns>Utvonal instance.</returns>
        Utvonal InsertRoute(Utvonal route);

        /// <summary>
        /// Update route.
        /// </summary>
        /// <param name="route">Utvonal instance.</param>
        /// <returns>Updated Utvonal instance.</returns>
        Utvonal UpdateRoute(Utvonal route);

        /// <summary>
        /// Delete route.
        /// </summary>
        /// <param name="route">Utvonal instance.</param>
        void DeleteRoute(Utvonal route);
    }
}
