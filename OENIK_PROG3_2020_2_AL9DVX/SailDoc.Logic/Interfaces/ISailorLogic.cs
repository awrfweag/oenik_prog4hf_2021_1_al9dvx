﻿namespace SailDoc.Logic.Interfaces
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using SailDoc.Data.Models;

    /// <summary>
    /// CRUD methods implementations.
    /// </summary>
    public interface ISailorLogic
    {
        /// <summary>
        /// Get all Matroz.
        /// </summary>
        /// <returns>Matroz list.</returns>
        IList<Matroz> GetAllSailor();

        /// <summary>
        /// Get Matroz by id.
        /// </summary>
        /// <param name="id">Matroz id.</param>
        /// <returns>Matroz instance.</returns>
        Matroz GetSailorById(int id);

        /// <summary>
        /// Update Matroz.
        /// </summary>
        /// <param name="sailor">Matroz instace.</param>
        /// <returns>Updated Matroz instance.</returns>
        Matroz UpdateSailor(Matroz sailor);

        /// <summary>
        /// Add Matroz.
        /// </summary>
        /// <param name="sailor">Matroz instance.</param>
        /// <returns>New Matroz instance.</returns>
        Matroz InsertSailor(Matroz sailor);

        /// <summary>
        /// Delete Matroz.
        /// </summary>
        /// <param name="sailor">Matroz instance.</param>
        void DeleteSailor(Matroz sailor);
    }
}
