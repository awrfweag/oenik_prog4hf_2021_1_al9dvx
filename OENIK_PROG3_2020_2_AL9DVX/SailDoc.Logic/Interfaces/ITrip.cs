﻿namespace SailDoc.Logic.Models
{
    using System;
    using System.Collections.Generic;
    using System.Text;

    /// <summary>
    /// Base trip properties.
    /// </summary>
    public interface ITrip
    {
        /// <summary>
        /// Boat name.
        /// </summary>
        public string SailBoatName { get; set; }

        /// <summary>
        /// Arrival harbour name.
        /// </summary>
        public string ArrivalHarbour { get; set; }

        /// <summary>
        /// Deparature harbour name.
        /// </summary>
        public string DeparatureHarbour { get; set; }

        /// <summary>
        /// Avarage sail time.
        /// </summary>
        public int SailTime { get; set; }
    }
}
