﻿namespace SailDoc.Logic.Interfaces
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using System.Threading.Tasks;
    using SailDoc.Logic.Models;
    using SailDoc.Repository.Interfaces;

    /// <summary>
    /// Method signatures for harbour management.
    /// </summary>
    public interface IHarbourManagement
    {
        /// <summary>
        /// Harbour stat elements.
        /// </summary>
        /// <returns>Harbour stat list.</returns>
        IList<HarbourStat> CalculateHarbourStats();

        /// <summary>
        /// Harbour stat async version.
        /// </summary>
        /// <returns>Harbour stats.</returns>
        Task<IList<HarbourStat>> CalculateHarbourStatsAsync();
    }
}
