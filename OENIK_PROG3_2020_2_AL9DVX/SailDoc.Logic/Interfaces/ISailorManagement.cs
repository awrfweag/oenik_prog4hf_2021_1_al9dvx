﻿namespace SailDoc.Logic.Interfaces
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using System.Threading.Tasks;
    using SailDoc.Logic.Models;

    /// <summary>
    /// Base method for sailor handling.
    /// </summary>
    public interface ISailorManagement
    {
        /// <summary>
        /// Method signature for caluclating sailor's stat.
        /// </summary>
        /// <returns>List of Sailor stat.</returns>
        IList<SailorStat> CalculateSailorStats();

        /// <summary>
        /// Method signature for caluclating sailor's stat asnyc.
        /// </summary>
        /// <returns>List of Sailor stat.</returns>
        Task<IList<SailorStat>> CalculateSailorStatsAsync();
    }
}
