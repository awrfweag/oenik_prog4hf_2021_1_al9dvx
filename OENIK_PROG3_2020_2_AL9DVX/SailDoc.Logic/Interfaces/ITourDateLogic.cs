﻿namespace SailDoc.Logic.Interfaces
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using SailDoc.Data.Models;

    /// <summary>
    /// TourDate CRUD method signatures.
    /// </summary>
    public interface ITourDateLogic
    {
        /// <summary>
        /// Get all tour date.
        /// </summary>
        /// <returns>Tura list.</returns>
        IList<TuraDatum> GetAllTour();

        /// <summary>
        /// Get tour date by id.
        /// </summary>
        /// <param name="id">TuraDatum id.</param>
        /// <returns>TuraDatum instnace.</returns>
        TuraDatum GetTourDateById(int id);

        /// <summary>
        /// Add tour date.
        /// </summary>
        /// <param name="tourDate">TuraDatum instance.</param>
        /// <returns>New TuraDatum instance.</returns>
        TuraDatum InsertTour(TuraDatum tourDate);

        /// <summary>
        /// Update tour date.
        /// </summary>
        /// <param name="tourDate">Tura instance.</param>
        /// <returns>Updated Tura instance.</returns>
        TuraDatum UpdateTour(TuraDatum tourDate);

        /// <summary>
        /// Delete tour date.
        /// </summary>
        /// <param name="tourDate">Tura date instance.</param>
        void DeleteTour(TuraDatum tourDate);
    }
}
