﻿namespace SailDoc.Logic.Interfaces
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using SailDoc.Data.Models;

    /// <summary>
    /// IBoatLogic basic method signatures.
    /// </summary>
    public interface IBoatLogic
    {
        /// <summary>
        /// Method signature for get all boats.
        /// </summary>
        /// <returns>Hajo list.</returns>
        IList<Hajo> GettAllBoat();

        /// <summary>
        /// Insert new boat.
        /// </summary>
        /// <param name="boat">Boat instance.</param>
        /// <returns>New Boat instance.</returns>
        Hajo InsertBoat(Hajo boat);

        /// <summary>
        /// Method signature for get a boat.
        /// </summary>
        /// <param name="id">Boat id.</param>
        /// <returns>Boat entity.</returns>
        Hajo GetHajo(int id);

        /// <summary>
        /// Method signature for padate boat.
        /// </summary>
        /// <param name="boat">Boat instance.</param>
        /// <returns>Boat entity.</returns>
        Hajo UpdateBoat(Hajo boat);

        /// <summary>
        /// Method signature for delete a boat.
        /// </summary>
        /// <param name="boat">Boat instace.</param>
        void DeleteBoat(Hajo boat);
    }
}
