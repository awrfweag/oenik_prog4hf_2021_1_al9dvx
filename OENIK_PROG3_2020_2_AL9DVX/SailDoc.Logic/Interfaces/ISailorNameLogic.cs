﻿// namespace SailDoc.Logic.Interfaces
// {
//    using System;
//    using System.Collections.Generic;
//    using System.Text;
//    using SailDoc.Data.Models;

// /// <summary>
//    /// Base method signatures for MatrozNev.
//    /// </summary>
//    public interface ISailorNameLogic
//    {
//        /// <summary>
//        /// Get all MatrozNev instance.
//        /// </summary>
//        /// <returns>MatrozNev list.</returns>
//        IList<MatrozNev> GetAllSailorName();

// /// <summary>
//        /// Get sailor name by id.
//        /// </summary>
//        /// <param name="name">Sailor name.</param>
//        /// <returns>MatrozNev instance.</returns>
//        MatrozNev GetSailorNameByName(string name);

// /// <summary>
//        /// Add new MatrozNev.
//        /// </summary>
//        /// <param name="sailorName">MatrozNev instance.</param>
//        /// <returns>New MatrozNev instance.</returns>
//        MatrozNev InsertSailorName(MatrozNev sailorName);

// /// <summary>
//        /// Update Matroznev.
//        /// </summary>
//        /// <param name="sailorName">MatrozNev instance.</param>
//        /// <returns>Updated MatrozNev instance.</returns>
//        MatrozNev UpdateMatrozNev(MatrozNev sailorName);

// /// <summary>
//        /// Delete MatrozNev.
//        /// </summary>
//        /// <param name="sailorName">Matroznev instance.</param>
//        void DeleteMatrozNev(MatrozNev sailorName);
//    }
// }
