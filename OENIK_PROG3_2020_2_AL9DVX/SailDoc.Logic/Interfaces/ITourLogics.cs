﻿namespace SailDoc.Logic.Interfaces
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using SailDoc.Data.Models;

    /// <summary>
    /// Tura CRUD methods sigantaures.
    /// </summary>
    public interface ITourLogics
    {
        /// <summary>
        /// Get all tour.
        /// </summary>
        /// <returns>Tura list.</returns>
        IList<Tura> GetAllTour();

        /// <summary>
        /// Get tour by id.
        /// </summary>
        /// <param name="id">Tura id.</param>
        /// <returns>Tura instnace.</returns>
        Tura GetTourById(int id);

        /// <summary>
        /// Add tour.
        /// </summary>
        /// <param name="tour">Tura instance.</param>
        /// <returns>New Tura instance.</returns>
        Tura InsertTour(Tura tour);

        /// <summary>
        /// Update tour.
        /// </summary>
        /// <param name="tour">Tura instance.</param>
        /// <returns>Updated Tura instance.</returns>
        Tura UpdateTour(Tura tour);

        /// <summary>
        /// Delete tour.
        /// </summary>
        /// <param name="tour">Tura instance.</param>
        void DeleteTour(Tura tour);
    }
}
