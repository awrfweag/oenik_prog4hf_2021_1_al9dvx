﻿namespace SailDoc.Logic.Interfaces
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using System.Threading.Tasks;
    using SailDoc.Data.Models;
    using SailDoc.Logic.Models;

    /// <summary>
    /// Trip methods.
    /// </summary>
    public interface ITripHandling
    {
        /// <summary>
        /// Trip creating method signature.
        /// </summary>
        /// <param name="boatId">Boat id.</param>
        /// <param name="sailorId">Sailor id.</param>
        /// <param name="depHarbourId">Deparature harbour id.</param>
        /// <param name="arrHarbourId">Arrival harbour id.</param>
        /// <param name="depDate">Deparature trip date.</param>
        /// <param name="arrDate">Arrival trip date.</param>
        /// <returns>Trip instance.</returns>
        Trip CreateTrip(int boatId, int sailorId, int depHarbourId, int arrHarbourId, string depDate, string arrDate);

        /// <summary>
        /// Base method to show all trip details.
        /// </summary>
        /// <returns>Trips.</returns>
        TripStats ShowTrips();

        /// <summary>
        /// Show trip details async version.
        /// </summary>
        /// <returns>Trip details.</returns>
        Task<TripStats> ShowTripsAsync();
    }
}
