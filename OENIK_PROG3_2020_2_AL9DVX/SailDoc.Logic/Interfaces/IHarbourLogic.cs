﻿namespace SailDoc.Logic.Interfaces
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using SailDoc.Data.Models;

    /// <summary>
    /// Base CRUD method signatures for Kikoto.
    /// </summary>
    public interface IHarbourLogic
    {
        /// <summary>
        /// Get all harbour.
        /// </summary>
        /// <returns>Kikoto instace.</returns>
        IList<Kikoto> GetAllHarbour();

        /// <summary>
        /// Get harbour by id.
        /// </summary>
        /// <param name="id">Harbour id.</param>
        /// <returns>Kikoto instace.</returns>
        Kikoto GetHarbourById(int id);

        /// <summary>
        /// Insert new harbour.
        /// </summary>
        /// <param name="harbour">Kikoto instace.</param>
        /// <returns>New Kikoto instace.</returns>
        Kikoto InsertHarbour(Kikoto harbour);

        /// <summary>
        /// Update harbour.
        /// </summary>
        /// <param name="harbour">Kikoto instace.</param>
        /// <returns>Updated Kikoto instace.</returns>
        Kikoto UpdateHarbour(Kikoto harbour);

        /// <summary>
        /// Delete harbour.
        /// </summary>
        /// <param name="harbour">Kikoto instace.</param>
        void DeleteHarbour(Kikoto harbour);
    }
}
