﻿namespace SailDoc.Logic.Models
{
    using System;
    using System.Collections.Generic;
    using System.Text;

    /// <summary>
    /// Harbour stat object.
    /// </summary>
    public class HarbourStat
    {
        /// <summary>
        /// harbour name.
        /// </summary>
        public string HarbourName { get; set; }

        /// <summary>
        /// boats name ever appeare in this harbour.
        /// </summary>
        public string[] BoatNames { get; set; }

        /// <summary>
        /// sailors name ever appeare in this harbour.
        /// </summary>
        public string[] SailorNames { get; set; }

        /// <summary>
        /// Implement equals.
        /// </summary>
        /// <param name="obj">Harbour stat object.</param>
        /// <returns>Boolean.</returns>
        public override bool Equals(object obj)
        {
            return obj is HarbourStat stat &&
                   this.HarbourName == stat.HarbourName &&
                   EqualityComparer<string[]>.Default.Equals(this.BoatNames, stat.BoatNames) &&
                   EqualityComparer<string[]>.Default.Equals(this.SailorNames, stat.SailorNames);
        }

        /// <summary>
        /// Implenet gethashcode.
        /// </summary>
        /// <returns>Integer.</returns>
        public override int GetHashCode()
        {
            return HashCode.Combine(this.HarbourName, this.BoatNames, this.SailorNames);
        }

        /// <summary>
        /// Implement tostring.
        /// </summary>
        /// <returns>String wiht boatname.</returns>
        public override string ToString()
        {
            return $"Harbour name: {this.HarbourName}";
        }
    }
}
