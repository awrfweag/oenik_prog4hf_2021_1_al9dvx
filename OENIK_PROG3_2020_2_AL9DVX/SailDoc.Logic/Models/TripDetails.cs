﻿namespace SailDoc.Logic.Models
{
    using System;
    using System.Collections.Generic;
    using System.Text;

    /// <summary>
    /// Detailed trip model.
    /// </summary>
    public class TripDetails : ITrip
    {
        /// <summary>
        /// Boat name.
        /// </summary>
        public string SailBoatName { get; set; }

        /// <summary>
        /// Arrival harbour name.
        /// </summary>
        public string ArrivalHarbour { get; set; }

        /// <summary>
        /// Deparature harbour name.
        /// </summary>
        public string DeparatureHarbour { get; set; }

        /// <summary>
        /// Sailors name.
        /// </summary>
        public string SailorName { get; set; }

        /// <summary>
        /// <inheritdoc/>
        /// </summary>
        public int SailTime { get; set; }

        /// <summary>
        /// Instance equlas method implementation.
        /// </summary>
        /// <param name="obj">Instance object.</param>
        /// <returns>Boolena.</returns>
        public override bool Equals(object obj)
        {
            return obj is TripDetails details &&
                   this.SailBoatName == details.SailBoatName &&
                   this.ArrivalHarbour == details.ArrivalHarbour &&
                   this.DeparatureHarbour == details.DeparatureHarbour &&
                   this.SailorName == details.SailorName;
        }

        /// <summary>
        /// Instance GetHashCode implementation.
        /// </summary>
        /// <returns>Int.</returns>
        public override int GetHashCode()
        {
            return HashCode.Combine(this.SailBoatName, this.ArrivalHarbour, this.DeparatureHarbour, this.SailorName);
        }
    }
}
