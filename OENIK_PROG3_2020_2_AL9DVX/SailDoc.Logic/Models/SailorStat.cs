﻿namespace SailDoc.Logic.Models
{
    using System;
    using System.Collections.Generic;
    using System.Text;

    /// <summary>
    /// Sailor stat object.
    /// </summary>
    public class SailorStat
    {
        /// <summary>
        /// Sailor Name.
        /// </summary>
        public string SailorName { get; set; }

        /// <summary>
        /// Number or trips where sailor take part.
        /// </summary>
        public int TripCount { get; set; }

        /// <summary>
        /// Grand total of sailing time.
        /// </summary>
        public double AllTimeOnWater { get; set; }
    }
}
