﻿namespace SailDoc.Logic.Models
{
    using System;
    using System.Collections.Generic;
    using System.Text;

    /// <summary>
    /// Trip stat class.
    /// </summary>
    public class TripStats
    {
        /// <summary>
        /// Trip details.
        /// </summary>
        public IList<TripDetails> TripDetails { get; set; }

        /// <summary>
        /// Avarage trip time.
        /// </summary>
        public double AvgTripTime { get; set; }

        /// <summary>
        /// Sailor avg trip time.
        /// </summary>
        public IList<SailorStat> SailorStats { get; set; }
    }
}
