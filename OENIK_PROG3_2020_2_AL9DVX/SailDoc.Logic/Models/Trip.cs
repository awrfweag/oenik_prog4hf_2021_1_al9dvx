﻿namespace SailDoc.Logic.Models
{
    using System;
    using System.Collections.Generic;
    using System.Text;

    /// <summary>
    /// Base class for creating trip.
    /// </summary>
    public class Trip : ITrip
    {
        /// <summary>
        /// Boat name.
        /// </summary>
        public string SailBoatName { get; set; }

        /// <summary>
        /// Sailor name.
        /// </summary>
        public string SailorName { get; set; }

        /// <summary>
        /// Deparature harbour name.
        /// </summary>
        public string DeparatureHarbour { get; set; }

        /// <summary>
        /// Arrival harbour name.
        /// </summary>
        public string ArrivalHarbour { get; set; }

        /// <summary>
        /// <inheritdoc/>
        /// </summary>
        public int SailTime { get; set; }

        /// <summary>
        /// Trip object equals fn.
        /// </summary>
        /// <param name="obj">Trip object.</param>
        /// <returns>Boolean.</returns>
        public override bool Equals(object obj)
        {
            return obj is Trip trip &&
                   this.SailBoatName == trip.SailBoatName &&
                   this.SailorName == trip.SailorName &&
                   this.DeparatureHarbour == trip.DeparatureHarbour &&
                   this.ArrivalHarbour == trip.ArrivalHarbour;
        }

        /// <summary>
        /// Trip gethashcode method.
        /// </summary>
        /// <returns>Unique int.</returns>
        public override int GetHashCode()
        {
            return HashCode.Combine(this.SailBoatName, this.SailorName, this.DeparatureHarbour, this.ArrivalHarbour);
        }

        /// <summary>
        /// Trip toString implementation.
        /// </summary>
        /// <returns>String.</returns>
        public override string ToString()
        {
            return $"Boat: {this.SailBoatName}, SailorName: {this.SailorName}, DepHarbour: {this.DeparatureHarbour}, ArrHarbour: {this.ArrivalHarbour}";
        }
    }
}
