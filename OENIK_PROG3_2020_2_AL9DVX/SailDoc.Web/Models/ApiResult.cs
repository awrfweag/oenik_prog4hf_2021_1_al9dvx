﻿namespace SailDoc.Web.Models
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;

    /// <summary>
    /// ApiRes class.
    /// </summary>
    public class ApiResult
    {
        /// <summary>
        /// Opre res.
        /// </summary>
        public bool OperationResult { get; set; }
    }
}
