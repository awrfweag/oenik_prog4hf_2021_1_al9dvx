﻿namespace SailDoc.Web.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.Linq;
    using System.Threading.Tasks;

    /// <summary>
    /// Boat Form model and MVC ViewModel.
    /// </summary>
    public class Boat
    {
        /// <summary>
        /// Boat unique id.
        /// </summary>
        public int HajoId { get; set; }

        /// <summary>
        /// Boat registration number.
        /// </summary>
        [Display(Name = "Registration Number")]
        [Required]
        [StringLength(10, MinimumLength = 5)]
        public string Lajstromszam { get; set; }

        /// <summary>
        /// Boat name.
        /// </summary>
        [Display(Name = "Name")]
        [Required]
        [StringLength(100, MinimumLength = 2)]
        public string Nev { get; set; }
    }
}
