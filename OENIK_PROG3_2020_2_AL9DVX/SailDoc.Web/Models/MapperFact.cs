﻿namespace SailDoc.Web.Models
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using AutoMapper;

    /// <summary>
    /// Conversion between data and web boat model.
    /// </summary>
    public static class MapperFact
    {
        /// <summary>
        /// Mapper function.
        /// </summary>
        /// <returns>Mapper config.</returns>
        public static IMapper CreateMapper()
        {
            var config = new MapperConfiguration(config =>
            {
                config.CreateMap<Data.Models.Hajo, Web.Models.Boat>().ReverseMap();
            });

            return config.CreateMapper();
        }
    }
}
