﻿namespace SailDoc.Web.Models
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;

    /// <summary>
    /// Display Boat list.
    /// </summary>
    public class BoatListViewModel
    {
        /// <summary>
        /// Boat list.
        /// </summary>
        public List<Boat> Boats { get; set; }

        /// <summary>
        /// Represent willing to edit boat.
        /// </summary>
        public Boat Boat { get; set; }
    }
}
