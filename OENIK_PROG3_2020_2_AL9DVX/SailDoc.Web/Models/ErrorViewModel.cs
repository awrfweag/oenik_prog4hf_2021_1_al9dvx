namespace SailDoc.Web.Models
{
    using System;

    /// <summary>
    /// Error ViewModel.
    /// </summary>
    public class ErrorViewModel
    {
        /// <summary>
        /// Req id.
        /// </summary>
        public string RequestId { get; set; }

        /// <summary>
        /// Has req id.
        /// </summary>
        public bool ShowRequestId => !string.IsNullOrEmpty(this.RequestId);
    }
}
