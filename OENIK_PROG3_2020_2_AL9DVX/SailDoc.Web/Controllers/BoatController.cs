﻿namespace SailDoc.Web.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using AutoMapper;
    using Microsoft.AspNetCore.Mvc;
    using SailDoc.Data.Models;
    using SailDoc.Logic.Interfaces;
    using SailDoc.Web.Models;

    /// <summary>
    /// Boats controller.
    /// </summary>
    public class BoatController : Controller
    {
        private IBoatLogic boatLogic;
        private IMapper mapper;
        private BoatListViewModel listViewModel;

        /// <summary>
        /// Initializes a new instance of the <see cref="BoatController"/> class.
        /// </summary>
        /// <param name="boatLogic">boat handler instance.</param>
        /// <param name="mapper">mapper instance.</param>
        public BoatController(IBoatLogic boatLogic, IMapper mapper)
        {
            this.boatLogic = boatLogic;
            this.mapper = mapper;

            this.listViewModel = new BoatListViewModel();
            this.listViewModel.Boat = new Models.Boat();

            var boats = this.boatLogic.GettAllBoat();
            this.listViewModel.Boats = this.mapper.Map<IList<Data.Models.Hajo>, List<Web.Models.Boat>>(boats);
        }

        /// <summary>
        /// Land page for boats.
        /// </summary>
        /// <returns>View.</returns>
        public IActionResult Index()
        {
            this.ViewData["editAction"] = "AddNew";
            return this.View("BoatIndex", this.listViewModel);
        }

        /// <summary>
        /// Get boat details.
        /// </summary>
        /// <param name="id">Boat id.</param>
        /// <returns>View instance.</returns>
        public IActionResult Details(int id)
        {
            return this.View("BoatDetails", this.GetBoat(id));
        }

        /// <summary>
        /// Remove boat.
        /// </summary>
        /// <param name="id">Boat id.</param>
        /// <returns>Message.</returns>
        public IActionResult Remove(int id)
        {
            this.TempData["editResult"] = "Delete successed.";

            this.boatLogic.DeleteBoat(this.boatLogic.GetHajo(id));
            if (this.listViewModel.Boats.Find(b => b.HajoId == id) == null)
            {
                this.TempData["editResult"] = "Delete successed.";
            }

            return this.RedirectToAction(nameof(this.Index));
        }

        /// <summary>
        /// Edit boat.
        /// </summary>
        /// <param name="id">Boat id.</param>
        /// <returns>Edited boat.</returns>
        public IActionResult Edit(int id)
        {
            this.ViewData["editAction"] = "Edit";
            this.listViewModel.Boat = this.GetBoat(id);
            return this.View("BoatIndex", this.listViewModel);
        }

        /// <summary>
        /// Edit or update boat.
        /// </summary>
        /// <param name="boat">Boat instance.</param>
        /// <param name="editAction">Action type.</param>
        /// <returns>Redirect to Index.</returns>
        [HttpPost]
        public IActionResult Edit(Web.Models.Boat boat, string editAction)
        {
            if (this.ModelState.IsValid && boat != null)
            {
                this.TempData["editResult"] = "Insert successed.";
                if (editAction == "AddNew")
                {
                    var tmpB = this.mapper.Map<Web.Models.Boat, Data.Models.Hajo>(boat);
                    this.boatLogic.InsertBoat(tmpB);
                }
                else
                {
                    var boat1 = this.boatLogic.GetHajo(boat.HajoId) ?? new Hajo();
                    boat1.Nev = boat.Nev;
                    boat1.Lajstromszam = boat.Lajstromszam;
                    if (this.boatLogic.UpdateBoat(boat1) == null)
                    {
                        this.TempData["editResult"] = "Update failed.";
                    }
                }

                return this.RedirectToAction(nameof(this.Index));
            }
            else
            {
                this.ViewData["editAction"] = "Edit";
                this.listViewModel.Boat = boat;
                return this.View("BoatIndex", this.listViewModel);
            }
        }

        private Models.Boat GetBoat(int id)
        {
            Data.Models.Hajo boat = this.boatLogic.GetHajo(id);
            return this.mapper.Map<Data.Models.Hajo, Models.Boat>(boat);
        }
    }
}
