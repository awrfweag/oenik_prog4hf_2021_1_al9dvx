﻿namespace SailDoc.Web.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using AutoMapper;
    using Microsoft.AspNetCore.Mvc;
    using SailDoc.Data.Models;
    using SailDoc.Logic.Interfaces;
    using SailDoc.Web.Models;

    /// <summary>
    /// Boat API controller.
    /// </summary>
    public class BoatsApiController : Controller
    {
        private IBoatLogic boatLogic;
        private IMapper mapper;

        /// <summary>
        /// Initializes a new instance of the <see cref="BoatsApiController"/> class.
        /// </summary>
        /// <param name="boatLogic">Boat logic instance.</param>
        /// <param name="mapper">Mapper instance.</param>
        public BoatsApiController(IBoatLogic boatLogic, IMapper mapper)
        {
            this.boatLogic = boatLogic;
            this.mapper = mapper;
        }

        // GET BoatsApi/all

        /// <summary>
        /// Get all boats api.
        /// </summary>
        /// <returns>Boat list.</returns>
        [HttpGet]
        [ActionName("all")]
        public IEnumerable<Models.Boat> GetAllBoats()
        {
            var boats = this.boatLogic.GettAllBoat();
            return this.mapper.Map<IList<Data.Models.Hajo>, List<Models.Boat>>(boats);
        }

        /// <summary>
        /// Delete boat.
        /// </summary>
        /// <param name="id">Boat id.</param>
        /// <returns>ApiResult.</returns>
        [HttpGet]
        [ActionName("del")]
        public ApiResult DeleteBoat(int id)
        {
            this.boatLogic.DeleteBoat(this.boatLogic.GetHajo(id));
            return new ApiResult() { OperationResult = true };
        }

        /// <summary>
        /// Add boat.
        /// </summary>
        /// <param name="boat">Boat instance.</param>
        /// <returns>Api res.</returns>
        [HttpPost]
        [ActionName("add")]
        public ApiResult AddBoat(Models.Boat boat)
        {
            var tmpB = this.mapper.Map<Web.Models.Boat, Data.Models.Hajo>(boat);
            this.boatLogic.InsertBoat(tmpB);
            return new ApiResult() { OperationResult = true };
        }

        /// <summary>
        /// Update boat.
        /// </summary>
        /// <param name="boat">Boat.</param>
        /// <returns>Updated boat.</returns>
        [HttpPost]
        [ActionName("mod")]
        public ApiResult ModBoat(Models.Boat boat)
        {
            var boat1 = this.boatLogic.GetHajo(boat.HajoId) ?? new Hajo();
            boat1.Nev = boat.Nev;
            boat1.Lajstromszam = boat.Lajstromszam;
            this.boatLogic.UpdateBoat(boat1);
            return new ApiResult() { OperationResult = true };
        }
    }
}
