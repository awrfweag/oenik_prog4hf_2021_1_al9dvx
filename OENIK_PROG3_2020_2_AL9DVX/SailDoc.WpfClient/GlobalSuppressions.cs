﻿// This file is used by Code Analysis to maintain SuppressMessage
// attributes that are applied to this project.
// Project-level suppressions either have no target or are given
// a specific target and scoped to a namespace, type, member, etc.

using System.Diagnostics.CodeAnalysis;

[assembly: SuppressMessage("StyleCop.CSharp.DocumentationRules", "SA1623:Property summary documentation should match accessors", Justification = "<Pending>", Scope = "member", Target = "~P:SailDoc.WpfClient.VM.BoatVM.HajoId")]
[assembly: SuppressMessage("StyleCop.CSharp.DocumentationRules", "SA1623:Property summary documentation should match accessors", Justification = "<Pending>", Scope = "member", Target = "~P:SailDoc.WpfClient.VM.BoatVM.Lajstromszam")]
[assembly: SuppressMessage("StyleCop.CSharp.DocumentationRules", "SA1623:Property summary documentation should match accessors", Justification = "<Pending>", Scope = "member", Target = "~P:SailDoc.WpfClient.VM.BoatVM.Nev")]
[assembly: SuppressMessage("StyleCop.CSharp.DocumentationRules", "SA1633:File should have header", Justification = "<Pending>")]
[assembly: SuppressMessage("StyleCop.CSharp.MaintainabilityRules", "SA1404:Code analysis suppression should have justification", Justification = "<Pending>")]
[assembly: SuppressMessage("StyleCop.CSharp.DocumentationRules", "SA1623:Property summary documentation should match accessors", Justification = "<Pending>", Scope = "member", Target = "~P:SailDoc.WpfClient.VM.MainVM.Boats")]
[assembly: SuppressMessage("StyleCop.CSharp.DocumentationRules", "SA1623:Property summary documentation should match accessors", Justification = "<Pending>", Scope = "member", Target = "~P:SailDoc.WpfClient.VM.MainVM.SelectedBoat")]
[assembly: SuppressMessage("StyleCop.CSharp.DocumentationRules", "SA1623:Property summary documentation should match accessors", Justification = "<Pending>", Scope = "member", Target = "~P:SailDoc.WpfClient.VM.MainVM.EditorFunc")]
[assembly: SuppressMessage("StyleCop.CSharp.DocumentationRules", "SA1623:Property summary documentation should match accessors", Justification = "<Pending>", Scope = "member", Target = "~P:SailDoc.WpfClient.VM.MainVM.AddBoat")]
[assembly: SuppressMessage("StyleCop.CSharp.DocumentationRules", "SA1623:Property summary documentation should match accessors", Justification = "<Pending>", Scope = "member", Target = "~P:SailDoc.WpfClient.VM.MainVM.DeleteBoat")]
[assembly: SuppressMessage("StyleCop.CSharp.DocumentationRules", "SA1623:Property summary documentation should match accessors", Justification = "<Pending>", Scope = "member", Target = "~P:SailDoc.WpfClient.VM.MainVM.UpdateBoat")]
[assembly: SuppressMessage("StyleCop.CSharp.DocumentationRules", "SA1623:Property summary documentation should match accessors", Justification = "<Pending>", Scope = "member", Target = "~P:SailDoc.WpfClient.MyIoc.Instance")]
[assembly: SuppressMessage("StyleCop.CSharp.DocumentationRules", "SA1623:Property summary documentation should match accessors", Justification = "<Pending>", Scope = "member", Target = "~P:SailDoc.WpfClient.VM.MainVM.LoadBoats")]
[assembly: SuppressMessage("Usage", "CA2234:Pass system uri objects instead of strings", Justification = "<Pending>", Scope = "member", Target = "~M:SailDoc.WpfClient.BL.BoatLogic.ApiEditBoat(SailDoc.WpfClient.VM.BoatVM,System.Boolean)~System.Boolean")]
[assembly: SuppressMessage("Reliability", "CA2000:Dispose objects before losing scope", Justification = "<Pending>", Scope = "member", Target = "~M:SailDoc.WpfClient.BL.BoatLogic.ApiEditBoat(SailDoc.WpfClient.VM.BoatVM,System.Boolean)~System.Boolean")]
[assembly: SuppressMessage("Globalization", "CA1305:Specify IFormatProvider", Justification = "<Pending>", Scope = "member", Target = "~M:SailDoc.WpfClient.BL.BoatLogic.ApiEditBoat(SailDoc.WpfClient.VM.BoatVM,System.Boolean)~System.Boolean")]
[assembly: SuppressMessage("Performance", "CA1822:Mark members as static", Justification = "<Pending>", Scope = "member", Target = "~M:SailDoc.WpfClient.BL.BoatLogic.SendMessage(System.Boolean)")]
[assembly: SuppressMessage("Usage", "CA2234:Pass system uri objects instead of strings", Justification = "<Pending>", Scope = "member", Target = "~M:SailDoc.WpfClient.BL.BoatLogic.ApiDelBoat(SailDoc.WpfClient.VM.BoatVM)")]
[assembly: SuppressMessage("Globalization", "CA1305:Specify IFormatProvider", Justification = "<Pending>", Scope = "member", Target = "~M:SailDoc.WpfClient.BL.BoatLogic.ApiDelBoat(SailDoc.WpfClient.VM.BoatVM)")]
[assembly: SuppressMessage("Usage", "CA2234:Pass system uri objects instead of strings", Justification = "<Pending>", Scope = "member", Target = "~M:SailDoc.WpfClient.BL.BoatLogic.ApiGetBoats~System.Collections.Generic.List{SailDoc.WpfClient.VM.BoatVM}")]
[assembly: SuppressMessage("Design", "CA1001:Types that own disposable fields should be disposable", Justification = "<Pending>", Scope = "type", Target = "~T:SailDoc.WpfClient.BL.BoatLogic")]
[assembly: SuppressMessage("Design", "CA1002:Do not expose generic lists", Justification = "<Pending>", Scope = "member", Target = "~M:SailDoc.WpfClient.BL.IBoatLogicWpf.ApiGetBoats~System.Collections.Generic.List{SailDoc.WpfClient.VM.BoatVM}")]
[assembly: SuppressMessage("Usage", "CA2227:Collection properties should be read only", Justification = "<Pending>", Scope = "member", Target = "~P:SailDoc.WpfClient.VM.MainVM.Boats")]
[assembly: SuppressMessage("Style", "IDE0044:Add readonly modifier", Justification = "<Pending>", Scope = "member", Target = "~F:SailDoc.WpfClient.VM.MainVM.boatLogic")]
