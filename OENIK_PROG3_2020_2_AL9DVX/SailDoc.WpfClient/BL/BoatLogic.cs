﻿// This file is used by Code Analysis to maintain SuppressMessage
// attributes that are applied to this project.
// Project-level suppressions either have no target or are given
// a specific target and scoped to a namespace, type, member, etc.

namespace SailDoc.WpfClient.BL
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Net.Http;
    using System.Text;
    using System.Text.Json;
    using System.Threading.Tasks;
    using GalaSoft.MvvmLight.Messaging;
    using SailDoc.WpfClient.VM;

    /// <summary>
    /// API calls.
    /// </summary>
    public class BoatLogic : IBoatLogicWpf
    {
        /// <summary>
        /// Url.
        /// </summary>
        private readonly string url = "http://localhost:2448/BoatsApi/";
        private HttpClient client = new HttpClient();
        private JsonSerializerOptions jsonSerializerOptions = new JsonSerializerOptions(JsonSerializerDefaults.Web);

        /// <summary>
        /// GetBoats.
        /// </summary>
        /// <returns>Boat list.</returns>
        public List<BoatVM> ApiGetBoats()
        {
            string json = this.client.GetStringAsync(this.url + "all").Result;
            var list = JsonSerializer.Deserialize<List<BoatVM>>(json, this.jsonSerializerOptions);
            return list;
        }

        /// <summary>
        /// Delete boat.
        /// </summary>
        /// <param name="boat">Boat.</param>
        public void ApiDelBoat(BoatVM boat)
        {
            bool success = false;
            if (boat != null)
            {
                string json = this.client.GetStringAsync(this.url + "del/" + boat.HajoId).Result;
                JsonDocument jsonDocument = JsonDocument.Parse(json);
                success = jsonDocument.RootElement.EnumerateObject().First().Value.GetRawText() == "true";
            }

            this.SendMessage(success);
        }

        /// <summary>
        /// Edit boat or add.
        /// </summary>
        /// <param name="boat">Boat vm.</param>
        /// <param name="editorFunc">edit func.</param>
        public void ApiEditBoat(BoatVM boat, Func<BoatVM, bool> editorFunc)
        {
            BoatVM cloenBoat = new BoatVM();

            if (boat != null)
            {
                cloenBoat.ShallowBoatInstance(boat);
            }

            bool? sucess = editorFunc?.Invoke(cloenBoat);

            if (sucess == true)
            {
                if (boat != null)
                {
                    this.ApiEditBoat(cloenBoat, true);
                }
                else
                {
                    this.ApiEditBoat(cloenBoat, false);
                }
            }

            this.SendMessage(sucess == true);
        }

        private bool ApiEditBoat(BoatVM boatVM, bool isEditing)
        {
            if (boatVM == null)
            {
                return false;
            }

            string myUrl = this.url + (isEditing ? "mod" : "add");

            Dictionary<string, string> postData = new Dictionary<string, string>();
            if (isEditing)
            {
                postData.Add("hajoId", boatVM.HajoId.ToString());
            }

            postData.Add("lajstromszam", boatVM.Lajstromszam);
            postData.Add("nev", boatVM.Nev);
            string json = this.client.PostAsync(myUrl, new FormUrlEncodedContent(postData)).Result.Content.ReadAsStringAsync().Result;
            JsonDocument jsonDocument = JsonDocument.Parse(json);
            return jsonDocument.RootElement.EnumerateObject().First().Value.GetRawText() == "true";
        }

        private void SendMessage(bool sucess)
        {
            string msg = sucess ? "Success" : "Faild";
            Messenger.Default.Send(msg, "BoatResult");
        }
    }
}
