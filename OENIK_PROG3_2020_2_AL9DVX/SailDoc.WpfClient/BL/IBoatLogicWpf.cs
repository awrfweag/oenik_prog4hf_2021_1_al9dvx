﻿// <copyright file="IBoatLogicWpf.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace SailDoc.WpfClient.BL
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using SailDoc.WpfClient.VM;

    /// <summary>
    /// UI BoatLogic methods signatures.
    /// </summary>
    public interface IBoatLogicWpf
    {
        /// <summary>
        /// Method signature for get all boats.
        /// </summary>
        /// <returns>Hajo list.</returns>
        List<BoatVM> ApiGetBoats();

        /// <summary>
        /// Api edit.
        /// </summary>
        /// <param name="boat">boat.</param>
        /// <param name="editorFunc">func.</param>
        void ApiEditBoat(BoatVM boat, Func<BoatVM, bool> editorFunc);

        /// <summary>
        /// Method signature for delete a boat.
        /// </summary>
        /// <param name="boat">Deleted boat.</param>
        void ApiDelBoat(BoatVM boat);
    }
}
