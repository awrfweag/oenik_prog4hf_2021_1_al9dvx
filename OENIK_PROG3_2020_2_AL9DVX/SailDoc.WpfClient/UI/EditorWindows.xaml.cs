﻿// This file is used by Code Analysis to maintain SuppressMessage
// attributes that are applied to this project.
// Project-level suppressions either have no target or are given
// a specific target and scoped to a namespace, type, member, etc.

namespace SailDoc.WpfClient.UI
{
    using System.Windows;
    using SailDoc.WpfClient.VM;

    /// <summary>
    /// Interaction logic for EditorWindows.xaml.
    /// </summary>
    public partial class EditorWindows : Window
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="EditorWindows"/> class.
        /// </summary>
        public EditorWindows()
        {
            this.InitializeComponent();
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="EditorWindows"/> class.
        /// </summary>
        /// <param name="oldBoat">BoatVm.</param>
        public EditorWindows(BoatVM oldBoat)
          : this()
        {
            this.DataContext = oldBoat;
        }

        private void OnSave(object sender, RoutedEventArgs e)
        {
            this.DialogResult = true;
        }

        private void OnCancel(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }
    }
}
