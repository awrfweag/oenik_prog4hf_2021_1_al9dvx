﻿// This file is used by Code Analysis to maintain SuppressMessage
// attributes that are applied to this project.
// Project-level suppressions either have no target or are given
// a specific target and scoped to a namespace, type, member, etc.

namespace SailDoc.WpfClient.VM
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using GalaSoft.MvvmLight;

    /// <summary>
    /// Boat UI class.
    /// </summary>
    public class BoatVM : ObservableObject
    {
        private int hajoId;
        private string lajtstrom;
        private string nev;

        /// <summary>
        /// Boat unique id.
        /// </summary>
        public int HajoId
        {
            get { return this.hajoId; }
            set { this.Set(ref this.hajoId, value); }
        }

        /// <summary>
        /// Boat registration number.
        /// </summary>
        public string Lajstromszam
        {
            get { return this.lajtstrom; }
            set { this.Set(ref this.lajtstrom, value); }
        }

        /// <summary>
        /// Boat name.
        /// </summary>
        public string Nev
        {
            get { return this.nev; }
            set { this.Set(ref this.nev, value); }
        }

        /// <summary>
        /// We need this copy fn because of binding is irrevirsible.
        /// </summary>
        /// <param name="otherBoat">Other instancve.</param>
        public void ShallowBoatInstance(BoatVM otherBoat)
        {
            this.GetType().GetProperties().ToList().ForEach(prop => prop.SetValue(this, prop.GetValue(otherBoat)));
        }
    }
}
