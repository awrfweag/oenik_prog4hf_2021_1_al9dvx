﻿namespace SailDoc.WpfClient.VM
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows.Input;
    using CommonServiceLocator;
    using GalaSoft.MvvmLight;
    using GalaSoft.MvvmLight.Command;
    using SailDoc.WpfClient.BL;

    /// <summary>
    /// Main vm.
    /// </summary>
    public class MainVM : ViewModelBase
    {
        private IBoatLogicWpf boatLogic;
        private BoatVM selectedBoat;
        private ObservableCollection<BoatVM> boats;

        /// <summary>
        /// Initializes a new instance of the <see cref="MainVM"/> class.
        /// </summary>
        /// <param name="boatLogicWpf">DI.</param>
        public MainVM(IBoatLogicWpf boatLogicWpf)
        {
            this.boatLogic = boatLogicWpf;

            this.LoadBoats = new RelayCommand(() => this.Boats = new ObservableCollection<BoatVM>(this.boatLogic.ApiGetBoats()));
            this.AddBoat = new RelayCommand(() => this.boatLogic.ApiEditBoat(null, this.EditorFunc));
            this.UpdateBoat = new RelayCommand(() => this.boatLogic.ApiEditBoat(this.SelectedBoat, this.EditorFunc));
            this.DeleteBoat = new RelayCommand(() => this.boatLogic.ApiDelBoat(this.SelectedBoat));
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="MainVM"/> class.
        /// </summary>
        public MainVM()
            : this(IsInDesignModeStatic ? null : ServiceLocator.Current.GetInstance<IBoatLogicWpf>())
        {
        }

        /// <summary>
        /// Boats.
        /// </summary>
        public ObservableCollection<BoatVM> Boats
        {
            get { return this.boats; }
            set { this.Set(ref this.boats, value); }
        }

        /// <summary>
        /// Boat.
        /// </summary>
        public BoatVM SelectedBoat
        {
            get { return this.selectedBoat; }
            set { this.Set(ref this.selectedBoat, value); }
        }

        /// <summary>
        /// Edit window.
        /// </summary>
        public Func<BoatVM, bool> EditorFunc { get; set; }

        /// <summary>
        /// Load boat.
        /// </summary>
        public ICommand LoadBoats { get; private set; }

        /// <summary>
        /// UI add boat btn action handler.
        /// </summary>
        public ICommand AddBoat { get; private set; }

        /// <summary>
        /// UI delete boat btn action handler.
        /// </summary>
        public ICommand DeleteBoat { get; private set; }

        /// <summary>
        /// UI update boat btn action handler.
        /// </summary>
        public ICommand UpdateBoat { get; private set; }
    }
}
