﻿namespace SailDoc.Data.Models
{
    using System;
    using Microsoft.EntityFrameworkCore;
    using Microsoft.EntityFrameworkCore.Metadata;

    /// <summary>
    /// Context class.
    /// </summary>
    public partial class SailDocContext : DbContext
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="SailDocContext"/> class.
        /// </summary>
        public SailDocContext()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="SailDocContext"/> class.
        /// </summary>
        /// <param name="options">DbContext.</param>
        public SailDocContext(DbContextOptions<SailDocContext> options)
            : base(options)
        {
        }

        /// <summary>
        /// Boat table.
        /// </summary>
        public virtual DbSet<Hajo> Hajo { get; set; }

        /// <summary>
        /// Harbour table.
        /// </summary>
        public virtual DbSet<Kikoto> Kikoto { get; set; }

        /// <summary>
        /// Sailor table.
        /// </summary>
        public virtual DbSet<Matroz> Matroz { get; set; }

        ///// <summary>
        ///// SailorName table.
        ///// </summary>
        // public virtual DbSet<MatrozNev> MatrozNev { get; set; }

        /// <summary>
        /// Tour table.
        /// </summary>
        public virtual DbSet<Tura> Tura { get; set; }

        /// <summary>
        /// Tour date table.
        /// </summary>
        public virtual DbSet<TuraDatum> TuraDatum { get; set; }

        /// <summary>
        /// Route table.
        /// </summary>
        public virtual DbSet<Utvonal> Utvonal { get; set; }

        // public virtual DbSet<UtvonalErintettKikoto> UtvonalErintettKikoto { get; set; }

        /// <summary>
        /// Database configuring.
        /// </summary>
        /// <param name="optionsBuilder">Database options parameter.</param>
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.EnableSensitiveDataLogging();
            if (!optionsBuilder.IsConfigured)
            {
                optionsBuilder.UseLazyLoadingProxies();
                optionsBuilder.UseSqlServer("Data Source = (LocalDB)\\MSSQLLocalDB; AttachDbFilename = |DataDirectory|\\SailDoc.mdf; Integrated Security = True");
            }
        }

        /// <summary>
        /// Database tables logic.
        /// </summary>
        /// <param name="modelBuilder">Modelbuilder parameter.</param>
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Hajo>(entity =>
            {
                entity.HasIndex(e => e.Lajstromszam)
                    .HasDatabaseName("UQ__Hajo__73B9D1B76C9D0D9B")
                    .IsUnique();

                entity.HasIndex(e => e.Nev)
                    .HasDatabaseName("UQ__Hajo__C7D0355D5E611029")
                    .IsUnique();

                entity.Property(e => e.HajoId).HasColumnName("Hajo_ID");

                entity.Property(e => e.Lajstromszam)
                    .IsRequired()
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.Merules).HasColumnType("numeric(2, 1)");

                entity.Property(e => e.Nev)
                    .IsRequired()
                    .HasMaxLength(30)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<Kikoto>(entity =>
            {
                entity.Property(e => e.KikotoId).HasColumnName("Kikoto_ID");

                entity.Property(e => e.KikotoDij).HasColumnName("Kikoto_dij");

                entity.Property(e => e.Mosdo).HasDefaultValueSql("((0))");

                entity.Property(e => e.Nev)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.VendegHelyDarab).HasColumnName("Vendeg_hely_darab");

                entity.Property(e => e.Nev).HasColumnName("Nev");
            });

            modelBuilder.Entity<Matroz>(entity =>
            {
                entity.Property(e => e.MatrozId).HasColumnName("Matroz_ID");

                entity.Property(e => e.Nev)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                // entity.HasOne(d => d.NevNavigation)
                //    .WithMany(p => p.Matroz)
                //    .HasForeignKey(d => d.Nev)
                //    .OnDelete(DeleteBehavior.ClientSetNull)
                //    .HasConstraintName("nev_fk");
            });

            // modelBuilder.Entity<MatrozNev>(entity =>
            // {
            //    entity.HasKey(e => e.Nev)
            //        .HasName("nev_pk");

            // entity.Property(e => e.Nev)
            //        .HasMaxLength(50)
            //        .IsUnicode(false);

            // entity.Property(e => e.HajoVezengedely)
            //        .HasColumnName("Hajo_vezengedely")
            //        .HasMaxLength(20)
            //        .IsUnicode(false);

            // entity.Property(e => e.SzulDatum)
            //        .HasColumnName("Szul_datum")
            //        .HasColumnType("date");

            // entity.Property(e => e.SzulHely)
            //        .IsRequired()
            //        .HasColumnName("Szul_hely")
            //        .HasMaxLength(30)
            //        .IsUnicode(false);
            // });
            modelBuilder.Entity<Tura>(entity =>
            {
                entity.Property(e => e.HajoId).HasColumnName("Hajo_ID");

                entity.Property(e => e.MatrozId).HasColumnName("Matroz_ID");

                entity.Property(e => e.TuraId).HasColumnName("Tura_ID");

                entity.Property(e => e.UtvonalId).HasColumnName("Utvonal_ID");

                entity.HasOne(d => d.Hajo)
                    .WithMany()
                    .HasForeignKey(d => d.HajoId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("hajo_id_fk");

                entity.HasOne(d => d.Matroz)
                    .WithMany()
                    .HasForeignKey(d => d.MatrozId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("matroz_id_fk");

                entity.HasOne(d => d.TuraNavigation)
                    .WithMany()
                    .HasForeignKey(d => d.TuraId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("tura_id_fk");

                entity.HasOne(d => d.Utvonal)
                    .WithMany()
                    .HasForeignKey(d => d.UtvonalId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("utvonal_id_fk");
            });

            modelBuilder.Entity<TuraDatum>(entity =>
            {
                entity.HasKey(e => e.TuraId)
                    .HasName("tura_ID_pk");

                entity.Property(e => e.TuraId).HasColumnName("Tura_ID");

                entity.Property(e => e.KezdeteDatum)
                    .HasColumnName("Kezdete_datum")
                    .HasColumnType("date");

                entity.Property(e => e.VegeDatum)
                    .HasColumnName("Vege_datum")
                    .HasColumnType("date");
            });

            modelBuilder.Entity<Utvonal>(entity =>
            {
                entity.Property(e => e.UtvonalId).HasColumnName("Utvonal_ID");

                entity.Property(e => e.ErkezesiKikotoId).HasColumnName("Erkezesi_kikoto_ID");

                entity.Property(e => e.IndulasiKikotoId).HasColumnName("Indulasi_kikoto_ID");

                entity.HasOne(d => d.ErkezesiKikoto)
                    .WithMany(p => p.UtvonalErkezesiKikoto)
                    .HasForeignKey(d => d.ErkezesiKikotoId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("erkezesi_kikoto_ID_fk");

                entity.HasOne(d => d.IndulasiKikoto)
                    .WithMany(p => p.UtvonalIndulasiKikoto)
                    .HasForeignKey(d => d.IndulasiKikotoId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("indulasi_kikoto_ID_fk");
            });

            // modelBuilder.Entity<UtvonalErintettKikoto>(entity =>
            // {
            //    entity.HasNoKey();

            // entity.Property(e => e.KikotoId).HasColumnName("Kikoto_ID");

            // entity.Property(e => e.UtvonalId).HasColumnName("Utvonal_ID");

            // entity.HasOne(d => d.Kikoto)
            //        .WithMany()
            //        .HasForeignKey(d => d.KikotoId)
            //        .OnDelete(DeleteBehavior.ClientSetNull)
            //        .HasConstraintName("kikoto_id_fk");

            // entity.HasOne(d => d.Utvonal)
            //        .WithMany()
            //        .HasForeignKey(d => d.UtvonalId)
            //        .OnDelete(DeleteBehavior.ClientSetNull)
            //        .HasConstraintName("utovonal_id_fk");
            // });
            this.OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
