﻿namespace SailDoc.Data.Models
{
    using System;
    using System.Collections.Generic;

    /// <summary>
    /// Router entity class.
    /// </summary>
    public partial class Utvonal
    {
        /// <summary>
        /// Route id.
        /// </summary>
        public int UtvonalId { get; set; }

        /// <summary>
        /// Departure harbour id.
        /// </summary>
        public int IndulasiKikotoId { get; set; }

        /// <summary>
        /// Arrival harbour id.
        /// </summary>
        public int ErkezesiKikotoId { get; set; }

        /// <summary>
        /// FK for arrival harbour.
        /// </summary>
        public virtual Kikoto ErkezesiKikoto { get; set; }

        /// <summary>
        /// FK for depareture harbour.
        /// </summary>
        public virtual Kikoto IndulasiKikoto { get; set; }
    }
}
