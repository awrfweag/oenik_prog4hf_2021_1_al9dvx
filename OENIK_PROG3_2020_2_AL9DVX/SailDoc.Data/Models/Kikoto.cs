﻿namespace SailDoc.Data.Models
{
    using System;
    using System.Collections.Generic;

    /// <summary>
    /// Kikoto entity.
    /// </summary>
    public partial class Kikoto
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Kikoto"/> class.
        /// </summary>
        public Kikoto()
        {
            this.UtvonalErkezesiKikoto = new HashSet<Utvonal>();
            this.UtvonalIndulasiKikoto = new HashSet<Utvonal>();
        }

        /// <summary>
        /// PK value for a Kikoto instance.
        /// </summary>
        public int KikotoId { get; set; }

        /// <summary>
        /// Toalitte flag in the harbour.
        /// True if it is exits.
        /// </summary>
        public bool? Mosdo { get; set; }

        /// <summary>
        /// Harbour name.
        /// </summary>
        public string Nev { get; set; }

        /// <summary>
        /// Price for harbour useage.
        /// </summary>
        public int? KikotoDij { get; set; }

        /// <summary>
        /// Number of free place for sailors who has not got any harbour pass.
        /// </summary>
        public int? VendegHelyDarab { get; set; }

        /// <summary>
        /// FK for destination harbour.
        /// </summary>
        public virtual ICollection<Utvonal> UtvonalErkezesiKikoto { get; set; }

        /// <summary>
        /// FK for depareture harbour.
        /// </summary>
        public virtual ICollection<Utvonal> UtvonalIndulasiKikoto { get; set; }

        /// <summary>
        /// Making Kikoto instances comparable.
        /// </summary>
        /// <param name="obj">Kikoto object.</param>
        /// <returns>Is it equal or not.</returns>
        public override bool Equals(object obj)
        {
            return obj is Kikoto kikoto &&
                   this.KikotoId == kikoto.KikotoId &&
                   this.Mosdo == kikoto.Mosdo &&
                   this.Nev == kikoto.Nev &&
                   this.KikotoDij == kikoto.KikotoDij &&
                   this.VendegHelyDarab == kikoto.VendegHelyDarab;
        }

        /// <summary>
        /// Generating unieq hashcode.
        /// </summary>
        /// <returns>Hash as a int.</returns>
        public override int GetHashCode()
        {
            return HashCode.Combine(this.KikotoId, this.Mosdo, this.Nev, this.KikotoDij, this.VendegHelyDarab, this.UtvonalErkezesiKikoto, this.UtvonalIndulasiKikoto);
        }

        /// <summary>
        /// Create string for this class.
        /// </summary>
        /// <returns>String with the name of this harbour.</returns>
        public override string ToString()
        {
            return $"Name: {this.Nev}";
        }
    }
}
