﻿namespace SailDoc.Data.Models
{
    using System;
    using System.Collections.Generic;

    /// <summary>
    /// Boat entity.
    /// </summary>
    public partial class Hajo
    {
        /// <summary>
        /// Boat unique id.
        /// </summary>
        public int HajoId { get; set; }

        /// <summary>
        /// Boat crew capacity.
        /// </summary>
        public int? Ferohely { get; set; }

        /// <summary>
        /// Boat registration number.
        /// </summary>
        public string Lajstromszam { get; set; }

        /// <summary>
        /// Boat draft in meter.
        /// </summary>
        public decimal? Merules { get; set; }

        /// <summary>
        /// Boat name.
        /// </summary>
        public string Nev { get; set; }

        /// <summary>
        /// Boat length in meter.
        /// </summary>
        public int? Hajohossz { get; set; }

        /// <summary>
        /// Overrinding ToString.
        /// </summary>
        /// <returns>Formatted string.</returns>
        public override string ToString()
        {
            return $"BoatId: {this.HajoId}, Name: {this.Nev}";
        }

        /// <summary>
        /// Creating hashcode.
        /// </summary>
        /// <returns>Unique integer.</returns>
        public override int GetHashCode()
        {
            return this.HajoId * this.Nev.Length;
        }

        /// <summary>
        /// Making Hajo instances comparable.
        /// </summary>
        /// <param name="obj">Hajo-ish object.</param>
        /// <returns>Is is it equal or not.</returns>
        public override bool Equals(object obj)
        {
            if (obj is Hajo)
            {
                Hajo other = obj as Hajo;
                return this.HajoId == other.HajoId &&
                    this.Ferohely == other.Ferohely &&
                    this.Hajohossz == other.Hajohossz &&
                    this.Lajstromszam == other.Lajstromszam &&
                    this.Merules == other.Merules;
            }
            else
            {
                return false;
            }
        }
    }
}
