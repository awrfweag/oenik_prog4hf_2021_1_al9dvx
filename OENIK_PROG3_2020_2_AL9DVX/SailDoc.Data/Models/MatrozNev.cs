﻿// namespace SailDoc.Data.Models
// {
//    using System;
//    using System.Collections.Generic;

// /// <summary>
//    /// MatrozNev entity class.
//    /// </summary>
//    public partial class MatrozNev
//    {
//        /// <summary>
//        /// Initializes a new instance of the <see cref="MatrozNev"/> class.
//        /// </summary>
//        public MatrozNev()
//        {
//            this.Matroz = new HashSet<Matroz>();
//        }

// /// <summary>
//        /// Sailor name.
//        /// </summary>
//        public string Nev { get; set; }

// /// <summary>
//        /// Sailor telephone number.
//        /// </summary>
//        public long? Telefonszam { get; set; }

// /// <summary>
//        /// Sailor birth place.
//        /// </summary>
//        public string SzulHely { get; set; }

// /// <summary>
//        /// Sailor birth date.
//        /// </summary>
//        public DateTime SzulDatum { get; set; }

// /// <summary>
//        /// Sailor boat driving licence.
//        /// </summary>
//        public string HajoVezengedely { get; set; }

// /// <summary>
//        /// FK for MatrozNev in Matroz.
//        /// </summary>
//        public virtual ICollection<Matroz> Matroz { get; set; }
//    }
// }
