﻿namespace SailDoc.Data.Models
{
    using System;
    using System.Collections.Generic;

    /// <summary>
    /// Sailor entity class.
    /// </summary>
    public partial class Matroz
    {
        /// <summary>
        /// Sailor id.
        /// </summary>
        public int MatrozId { get; set; }

        /// <summary>
        /// Sailor name.
        /// </summary>
        public string Nev { get; set; }

        ///// <summary>
        ///// FK for MatrozNev.
        ///// </summary>
        // public virtual MatrozNev NevNavigation { get; set; }
    }
}
