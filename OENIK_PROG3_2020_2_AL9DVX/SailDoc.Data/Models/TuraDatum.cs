﻿namespace SailDoc.Data.Models
{
    using System;
    using System.Collections.Generic;

    /// <summary>
    /// Tour date entity class.
    /// </summary>
    public partial class TuraDatum
    {
        /// <summary>
        /// PK tour id.
        /// </summary>
        public int TuraId { get; set; }

        /// <summary>
        /// Tour start date.
        /// </summary>
        public DateTime? KezdeteDatum { get; set; }

        /// <summary>
        /// Tour end date.
        /// </summary>
        public DateTime? VegeDatum { get; set; }
    }
}
