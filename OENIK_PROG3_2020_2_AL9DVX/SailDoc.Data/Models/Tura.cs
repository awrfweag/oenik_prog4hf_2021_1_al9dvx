﻿namespace SailDoc.Data.Models
{
    using System;
    using System.Collections.Generic;

    /// <summary>
    /// Tura entity class.
    /// </summary>
    public partial class Tura
    {
        /// <summary>
        /// tour id.
        /// </summary>
        public int TuraId { get; set; }

        /// <summary>
        /// hajo id used under tour.
        /// </summary>
        public int HajoId { get; set; }

        /// <summary>
        /// route belongs to this tour.
        /// </summary>
        public int UtvonalId { get; set; }

        /// <summary>
        /// sailor id's whos taking part the tour.
        /// </summary>
        public int MatrozId { get; set; }

        /// <summary>
        /// FK for boat.
        /// </summary>
        public virtual Hajo Hajo { get; set; }

        /// <summary>
        /// FK for sailor.
        /// </summary>
        public virtual Matroz Matroz { get; set; }

        /// <summary>
        /// FK for tour date.
        /// </summary>
        public virtual TuraDatum TuraNavigation { get; set; }

        /// <summary>
        /// FK for route.
        /// </summary>
        public virtual Utvonal Utvonal { get; set; }
    }
}
