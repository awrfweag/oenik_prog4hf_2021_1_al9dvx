﻿INSERT INTO MatrozNev VALUES('Béla', 06320654123, 'Cegléd', DATEFROMPARTS(2020,02,11), 'ABC12234');
INSERT INTO MatrozNev VALUES('Béla2', 06303234123, 'Szolnok', DATEFROMPARTS(1990,04,11), 'ERT12234');
INSERT INTO MatrozNev VALUES('Béla3', 06641235423, 'Kecskemét', DATEFROMPARTS(1992,05,11), 'LGK12234');
INSERT INTO MatrozNev VALUES('Béla4', 066541234123, 'Sopron', DATEFROMPARTS(1995,06,11), 'EW4T234');

INSERT INTO KIKOTO (Mosdo, Nev, Kikoto_dij, Vendeg_hely_darab) VALUES(1, 'Kikoto1', 1000, 12);
INSERT INTO KIKOTO (Mosdo, Nev, Kikoto_dij, Vendeg_hely_darab) VALUES(1, 'Kikoto2', 2000, 32);
INSERT INTO KIKOTO (Mosdo, Nev, Kikoto_dij, Vendeg_hely_darab) VALUES(1, 'Kikoto3', 4300, 43);
INSERT INTO KIKOTO (Mosdo, Nev, Kikoto_dij, Vendeg_hely_darab) VALUES(0, 'Kikoto4', 4100, null);

ALTER TABLE KIKOTO DROP CONSTRAINT kikoto_nev_fk;

ALTER TABLE KikotoNev DROP CONSTRAINT kikoto_nev_pk;

INSERT INTO Matroz VALUES('Béla');
INSERT INTO Matroz VALUES('Béla2');
INSERT INTO Matroz VALUES('Béla3');
INSERT INTO Matroz VALUES('Béla4');
