﻿namespace SailDoc.Repository.Repositories
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using Microsoft.EntityFrameworkCore;
    using SailDoc.Data.Models;
    using SailDoc.Repository.Interfaces;

    /// <summary>
    /// Tour date repository class.
    /// </summary>
    public class TourDateRepository : RepositoryBase<TuraDatum, int>, ITourDateRepository
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="TourDateRepository"/> class.
        /// </summary>
        /// <param name="dbContext">Reach databse.</param>
        public TourDateRepository(DbContext dbContext)
            : base(dbContext)
        {
        }

        /// <summary>
        /// Get tour date by id.
        /// </summary>
        /// <param name="id">Tour date id.</param>
        /// <returns>TourDate instace.</returns>
        public override TuraDatum GetById(int id)
        {
            return this.GetAll().SingleOrDefault(x => x.TuraId == id);
        }
    }
}
