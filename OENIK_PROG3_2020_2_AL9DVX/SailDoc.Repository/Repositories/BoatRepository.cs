﻿namespace SailDoc.Repository.Repositories
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using Microsoft.EntityFrameworkCore;
    using SailDoc.Data.Models;
    using SailDoc.Repository.Interfaces;

    /// <summary>
    /// Class for Hajo CRUD methods.
    /// </summary>
    public class BoatRepository : RepositoryBase<Hajo, int>, IBoatRepository
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="BoatRepository"/> class.
        /// </summary>
        /// <param name="dbContext">Context to reach database.</param>
        public BoatRepository(DbContext dbContext)
            : base(dbContext)
        {
        }

        /// <summary>
        /// Get Hajo entity by id.
        /// </summary>
        /// <param name="id">Boat id.</param>
        /// <returns>Hajo instace.</returns>
        public override Hajo GetById(int id)
        {
            return this.GetAll().SingleOrDefault(x => x.HajoId == id);
        }

        /// <summary>
        /// Set hajo name by id.
        /// </summary>
        /// <param name="id">Boat id.</param>
        /// <param name="boatName">Hajo name.</param>
        public void SetBoatName(int id, string boatName)
        {
            var entity = this.GetById(id);
            entity.Nev = boatName;
            this.dbContext.Add<Hajo>(entity);
            this.dbContext.SaveChanges();
        }

        /// <summary>
        /// Set hajo draft by id.
        /// </summary>
        /// <param name="id">Boat id.</param>
        /// <param name="draft">Draft in meter.</param>
        public void SetDraft(int id, decimal draft)
        {
            var entity = this.GetById(id);
            entity.Merules = draft;
            this.dbContext.Add<Hajo>(entity);
            this.dbContext.SaveChanges();
        }

        /// <summary>
        /// Set hajo registration number by id.
        /// </summary>
        /// <param name="id">Boat id.</param>
        /// <param name="regNumber">Boat registration number.</param>
        public void SetRegistrationNumber(int id, string regNumber)
        {
            var entity = this.GetById(id);
            entity.Lajstromszam = regNumber;
            this.dbContext.Add<Hajo>(entity);
            this.dbContext.SaveChanges();
        }

        /// <summary>
        /// Set hajo legth by id.
        /// </summary>
        /// <param name="id">Boat id.</param>
        /// <param name="boatLength">Boat length.</param>
        public void SetSBoatLength(int id, int boatLength)
        {
            var entity = this.GetById(id);
            entity.Hajohossz = boatLength;
            this.dbContext.Add<Hajo>(entity);
            this.dbContext.SaveChanges();
        }

        /// <summary>
        /// Add seat capacity to this entity.
        /// </summary>
        /// <param name="id">Id of entity.</param>
        /// <param name="seatCapacity">Seat capacity.</param>
        public void SetSeats(int id, int seatCapacity)
        {
            var entity = this.GetById(id);
            entity.Ferohely = seatCapacity;
            this.dbContext.Add<Hajo>(entity);
            this.dbContext.SaveChanges();
        }

        ///// <summary>
        ///// Get a boat by id.
        ///// </summary>
        ///// <param name="id">Boat id.</param>
        ///// <returns>Boat entity.</returns>
        // public Hajo ReadBoat(int id)
        // {
        //    return this.GetById(id);
        // }

        ///// <summary>
        ///// Update a boat entity by id.
        ///// </summary>
        ///// <param name="boat">Boat id.</param>
        ///// <returns>Boat entity.</returns>
        // public Hajo UpdateBoat(Hajo boat)
        // {
        //    return this.Update(boat);
        // }

        ///// <summary>
        ///// Delete boat by id.
        ///// </summary>
        ///// <param name="boat">Boat id.</param>
        // public void DeleteBoat(Hajo boat)
        // {
        //    this.Delete(boat);
        // }
    }
}
