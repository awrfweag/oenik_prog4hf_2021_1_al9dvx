﻿namespace SailDoc.Repository.Repositories
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using Microsoft.EntityFrameworkCore;
    using SailDoc.Repository.Interfaces;

    /// <summary>
    /// Declare base methods for interact with database models.
    /// </summary>
    /// <typeparam name="TEntity">Generic instance type.</typeparam>
    /// <typeparam name="TKey">Specific instance key.</typeparam>
    public abstract class RepositoryBase<TEntity, TKey> : IRepositoryBase<TEntity, TKey>
        where TEntity : class
    {
        /// <summary>
        /// Descendants reach the database models.
        /// </summary>
        internal DbContext dbContext;

        /// <summary>
        /// Initializes a new instance of the <see cref="RepositoryBase{TEntity, TKey}"/> class.
        /// </summary>
        /// <param name="dbContext">Base context to reach database.</param>
        internal RepositoryBase(DbContext dbContext)
        {
            this.dbContext = dbContext;
        }

        /// <summary>
        /// Base method to get all entity instance from database.
        /// </summary>
        /// <returns>IQuerybale type which can be also queryable later.</returns>
        public IQueryable<TEntity> GetAll()
        {
            return this.dbContext.Set<TEntity>();
        }

        /// <summary>
        /// Entity specific method. Itt will be implemented by the specific instance.
        /// </summary>
        /// <param name="id">Id of entity.</param>
        /// <returns>Entity.</returns>
        public abstract TEntity GetById(TKey id);

        /// <summary>
        /// Base method to add new entity specific value to databse.
        /// </summary>
        /// <param name="entity">Entity willing to add.</param>
        /// <returns>Entity.</returns>
        public TEntity Insert(TEntity entity)
        {
            var result = this.dbContext.Add(entity);
            this.dbContext.SaveChanges();
            return result.Entity;
        }

        /// <summary>
        /// Base method to update entity specific value.
        /// </summary>
        /// <param name="entity">Entity willing to update.</param>
        /// <returns>Entity.</returns>
        public TEntity Update(TEntity entity)
        {
            var result = this.dbContext.Update(entity);
            this.dbContext.SaveChanges();
            return result.Entity;
        }

        /// <summary>
        /// Base method to delete from database.
        /// </summary>
        /// <param name="entity">Entity to delete.</param>
        public void Delete(TEntity entity)
        {
            this.dbContext.Remove(entity);
            this.dbContext.SaveChanges();
        }
    }
}
