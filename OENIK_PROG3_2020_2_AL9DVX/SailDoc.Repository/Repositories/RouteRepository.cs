﻿namespace SailDoc.Repository.Repositories
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using Microsoft.EntityFrameworkCore;
    using SailDoc.Data.Models;
    using SailDoc.Repository.Interfaces;

    /// <summary>
    /// Repository class for Utvonal CRUD methods.
    /// </summary>
    public class RouteRepository : RepositoryBase<Utvonal, int>, IRouteRepository
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="RouteRepository"/> class.
        /// </summary>
        /// <param name="dbContext">DbContext.</param>
        public RouteRepository(DbContext dbContext)
            : base(dbContext)
        {
        }

        /// <summary>
        /// Get route instance by id.
        /// </summary>
        /// <param name="id">Utvonal id.</param>
        /// <returns>Utvonal instance.</returns>
        public override Utvonal GetById(int id)
        {
            return this.GetAll().FirstOrDefault(x => x.UtvonalId == id);
        }
    }
}
