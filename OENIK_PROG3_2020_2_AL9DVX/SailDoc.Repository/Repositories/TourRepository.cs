﻿namespace SailDoc.Repository.Repositories
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using Microsoft.EntityFrameworkCore;
    using SailDoc.Data.Models;
    using SailDoc.Repository.Interfaces;

    /// <summary>
    /// Class for Tura CRUD methods.
    /// </summary>
    public class TourRepository : RepositoryBase<Tura, int>, ITourRepository
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="TourRepository"/> class.
        /// </summary>
        /// <param name="dbContext">DbContext.</param>
        public TourRepository(DbContext dbContext)
            : base(dbContext)
        {
        }

        /// <summary>
        /// Get tour by id.
        /// </summary>
        /// <param name="id">Tour id.</param>
        /// <returns>Tura instance.</returns>
        public override Tura GetById(int id)
        {
            return this.GetAll().SingleOrDefault(x => x.TuraId == id);
        }
    }
}
