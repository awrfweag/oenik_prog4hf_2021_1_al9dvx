﻿// namespace SailDoc.Repository.Repositories
// {
//    using System;
//    using System.Collections.Generic;
//    using System.Linq;
//    using System.Security.Cryptography.X509Certificates;
//    using System.Text;
//    using Microsoft.EntityFrameworkCore;
//    using SailDoc.Data.Models;
//    using SailDoc.Repository.Interfaces;

// /// <summary>
//    /// Repository class for sailor name CRUD methods.
//    /// </summary>
//    public class SailorNameRepository : RepositoryBase<MatrozNev, string>, ISailorNameRepository
//    {
//        /// <summary>
//        /// Initializes a new instance of the <see cref="SailorNameRepository"/> class.
//        /// </summary>
//        /// <param name="dbContext">Context to reach database.</param>
//        public SailorNameRepository(DbContext dbContext)
//            : base(dbContext)
//        {
//        }

// /// <summary>
//        /// Get Sailor by name.
//        /// </summary>
//        /// <param name="name">Sailor name.</param>
//        /// <returns>MatrozNev instance.</returns>
//        public override MatrozNev GetById(string name)
//        {
//            return this.GetAll().SingleOrDefault(x => x.Nev == name);
//        }
//    }
// }
