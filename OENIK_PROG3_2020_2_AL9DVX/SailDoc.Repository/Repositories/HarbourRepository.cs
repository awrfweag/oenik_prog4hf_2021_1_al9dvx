﻿namespace SailDoc.Repository.Repositories
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using Microsoft.EntityFrameworkCore;
    using SailDoc.Data.Models;
    using SailDoc.Repository.Interfaces;

    /// <summary>
    /// Class for Kikoto CRUD methods.
    /// </summary>
    public class HarbourRepository : RepositoryBase<Kikoto, int>, IHarbourRepository
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="HarbourRepository"/> class.
        /// </summary>
        /// <param name="dbContext">Database context.</param>
        public HarbourRepository(DbContext dbContext)
            : base(dbContext)
        {
        }

        /// <summary>
        /// Get Kikoto instace.
        /// </summary>
        /// <param name="id">Kikot instace id.</param>
        /// <returns>Kikoto.</returns>
        public override Kikoto GetById(int id)
        {
            return this.GetAll().SingleOrDefault(x => x.KikotoId == id);
        }

        /// <summary>
        /// Set toailette for Kikoto instace.
        /// </summary>
        /// <param name="id">Kikoto id.</param>
        /// <param name="hasToailette">Toailette flag.</param>
        public void SetToailette(int id, bool hasToailette)
        {
            var entity = this.GetById(id);
            entity.Mosdo = hasToailette;
            this.dbContext.Update(entity);
            this.dbContext.SaveChanges();
        }
    }
}
