﻿namespace SailDoc.Repository.Repositories
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Security.Cryptography.X509Certificates;
    using System.Text;
    using Microsoft.EntityFrameworkCore;
    using SailDoc.Data.Models;
    using SailDoc.Repository.Interfaces;

    /// <summary>
    /// Class for Matroz CRUD methods.
    /// </summary>
    public class SailorRepository : RepositoryBase<Matroz, int>, ISailorRepository
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="SailorRepository"/> class.
        /// </summary>
        /// <param name="dbContext">DbContext.</param>
        public SailorRepository(DbContext dbContext)
            : base(dbContext)
        {
        }

        /// <summary>
        /// Get Matroz by id.
        /// </summary>
        /// <param name="id">Sailor id.</param>
        /// <returns>Matroz instance.</returns>
        public override Matroz GetById(int id)
        {
            return this.GetAll().SingleOrDefault(x => x.MatrozId == id);
        }
    }
}
