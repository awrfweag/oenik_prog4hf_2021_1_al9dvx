﻿namespace SailDoc.Repository.Interfaces
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using SailDoc.Data.Models;

    /// <summary>
    /// Represents an interface for Hajo base methods.
    /// </summary>
    public interface IBoatRepository : IRepositoryBase<Hajo, int>
    {
        /// <summary>
        /// Method signature to set the seat capacity available in the boat.
        /// </summary>
        /// <param name="id">Boat id.</param>
        /// <param name="seatCapacity">Seat capacity available.</param>
        void SetSeats(int id, int seatCapacity);

        /// <summary>
        /// Method signature to set registration number of the boat.
        /// </summary>
        /// <param name="id">Boat id.</param>
        /// <param name="regNumber">Registration number.</param>
        void SetRegistrationNumber(int id, string regNumber);

        /// <summary>
        /// Method signature to set boat draft's.
        /// </summary>
        /// <param name="id">Boat id.</param>
        /// <param name="draft">Draft in meter.</param>
        void SetDraft(int id, decimal draft);

        /// <summary>
        /// Method signature to set boat name's.
        /// </summary>
        /// <param name="id">Boat id.</param>
        /// <param name="boatName">Boat name.</param>
        void SetBoatName(int id, string boatName);

        /// <summary>
        /// Method signature to set boat length's.
        /// </summary>
        /// <param name="id">Boat id.</param>
        /// <param name="boatLength">Length in meter.</param>
        void SetSBoatLength(int id, int boatLength);

        ///// <summary>
        ///// Method signature to get boat.
        ///// </summary>
        ///// <param name="id">Boat id.</param>
        ///// <returns>Boat entity.</returns>
        // Hajo ReadBoat(int id);

        ///// <summary>
        ///// Method signature to update boat.
        ///// </summary>
        ///// <param name="boat">Boat id.</param>
        ///// <returns>Boat entity.</returns>
        // Hajo UpdateBoat(Hajo boat);

        ///// <summary>
        ///// Method siganture to delete boat.
        ///// </summary>
        ///// <param name="boat">Boat id.</param>
        // void DeleteBoat(Hajo boat);
    }
}
