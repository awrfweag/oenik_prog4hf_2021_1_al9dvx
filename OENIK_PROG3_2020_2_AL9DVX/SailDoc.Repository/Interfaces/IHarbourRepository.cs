﻿namespace SailDoc.Repository.Interfaces
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using SailDoc.Data.Models;

    /// <summary>
    /// Base methods for Harbour entity.
    /// </summary>
    public interface IHarbourRepository : IRepositoryBase<Kikoto, int>
    {
        /// <summary>
        /// Method signature to represents toailette existance.
        /// </summary>
        /// <param name="id">Harbour id.</param>
        /// <param name="hasToailette">Flag for toailette existance.</param>
        void SetToailette(int id, bool hasToailette);
    }
}
