﻿namespace SailDoc.Repository.Interfaces
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using SailDoc.Data.Models;

    /// <summary>
    /// Base CRUD method signatures.
    /// </summary>
    public interface ITourDateRepository : IRepositoryBase<TuraDatum, int>
    {
    }
}
