﻿namespace SailDoc.Repository.Interfaces
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using SailDoc.Data.Models;

    /// <summary>
    /// Base methods for Matroz entity.
    /// </summary>
    public interface ISailorRepository : IRepositoryBase<Matroz, int>
    {
    }
}
