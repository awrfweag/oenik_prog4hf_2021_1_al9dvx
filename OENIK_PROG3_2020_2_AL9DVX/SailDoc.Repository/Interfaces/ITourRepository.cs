﻿namespace SailDoc.Repository.Interfaces
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using SailDoc.Data.Models;

    /// <summary>
    /// CRUD method sigantures for tour.
    /// </summary>
    public interface ITourRepository : IRepositoryBase<Tura, int>
    {
    }
}
