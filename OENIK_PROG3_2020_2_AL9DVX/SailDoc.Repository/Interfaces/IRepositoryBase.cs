﻿namespace SailDoc.Repository.Interfaces
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    /// <summary>
    /// Base methods for model specific interfaces.
    /// </summary>
    /// <typeparam name="TEntity">Model name.</typeparam>
    /// <typeparam name="TKey">Key for get model instance.</typeparam>
    public interface IRepositoryBase<TEntity, TKey>
        where TEntity : class
    {
        /// <summary>
        /// Base method signature to get all generic data from databes.
        /// </summary>
        /// <returns>Genereic tpye of IQueryable for later querying.</returns>
        IQueryable<TEntity> GetAll();

        /// <summary>
        /// Base method signatureto get one instance of generic type.
        /// </summary>
        /// <param name="id">Id for instance.</param>
        /// <returns>Generic instace.</returns>
        TEntity GetById(TKey id);

        /// <summary>
        /// Base method signature for add new value to database.
        /// </summary>
        /// <param name="entity">New value.</param>
        /// <returns>Instace type.</returns>
        TEntity Insert(TEntity entity);

        /// <summary>
        /// Base method signature to update the specific entitiy.
        /// </summary>
        /// <param name="entity">Entity to update.</param>
        /// <returns>Instance type.</returns>
        TEntity Update(TEntity entity);

        /// <summary>
        /// Base method signature to remove the specific entity.
        /// </summary>
        /// <param name="entity">Entity to remove.</param>
        void Delete(TEntity entity);
    }
}
