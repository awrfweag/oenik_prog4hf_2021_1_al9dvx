﻿namespace SailDoc.Repository.Interfaces
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using SailDoc.Data.Models;

    /// <summary>
    /// CRUD methods signature.
    /// </summary>
    public interface IRouteRepository : IRepositoryBase<Utvonal, int>
    {
    }
}
