﻿namespace SailDoc
{
    using System;
    using System.Linq;
    using System.Runtime.CompilerServices;
    using System.Threading.Tasks;
    using ConsoleTools;
    using Microsoft.EntityFrameworkCore;
    using SailDoc.Data.Models;
    using SailDoc.Logic.Services;
    using SailDoc.Repository.Interfaces;
    using SailDoc.Repository.Repositories;

    /// <summary>
    /// Program entry point.
    /// </summary>
    internal class Program
    {
        private static void Main()
        {
            ShowMenu();
        }

        private static void ShowMenu()
        {
            var factory = new DependencyFactory();

            var menu = new ConsoleMenu().
                       Add("List Boats", () => ShowAllBoats(factory)).
                       Add("Add New Boat", () => NewBoat(factory)).
                       Add("Show Boat By Name", () => GetBoatByName(factory)).
                       Add("Update Boat ", () => UpdateBoat(factory)).
                       Add("Delete Boat ", () => DeleteBoat(factory)).

                       Add("List Harbours", () => ShowAllHarbour(factory)).
                       Add("Add New Harbour", () => NewHarbour(factory)).
                       Add("Show Harbour By Name", () => GetHarbourByName(factory)).
                       Add("Update Harbour", () => UpdateHarbour(factory)).
                       Add("Delete Harbours", () => DeleteHarbour(factory)).

                       Add("List Sailors", () => ShowAllSailor(factory)).
                       Add("Add New Sailor", () => NewSailor(factory)).
                       Add("Show Sailor By Name", () => GetSailorByName(factory)).
                       Add("Update Sailor", () => UpdateSailor(factory)).
                       Add("Delete Sailors", () => DeleteSailor(factory)).

                       Add("Create trip", () => CreateTrip(factory)).
                       Add("Trips details", () => ShowTripDetails(factory)).
                       Add("Trips details async", () => ShowTripDetailsAsync(factory)).
                       Add("Sailor stats", () => ShowSailorStats(factory)).
                       Add("Sailor stats async", () => ShowSailorStatsAsync(factory)).
                       Add("Harbour stats", () => ShowHabourStats(factory)).
                       Add("Harbour stats async", () => ShowHabourStatsAsync(factory)).

                       Add("Close", ConsoleMenu.Close);
            menu.Show();
        }

        private static void ShowAllBoats(DependencyFactory dependencyFactory)
        {
            var boats = dependencyFactory.BoatLogic().GettAllBoat();

            if (boats.Count == 0)
            {
                Console.WriteLine("There are no boats in the database.");
                Console.ReadLine();
                return;
            }

            foreach (var boat in boats)
            {
                Console.WriteLine($"{boat.HajoId}. {boat.Nev}");
            }

            Console.ReadLine();
        }

        private static void NewBoat(DependencyFactory dependencyFactory)
        {
            Console.WriteLine("Name:");
            string name = Console.ReadLine();

            Console.WriteLine("Capacity:");
            int capacity = int.Parse(Console.ReadLine());

            Console.WriteLine("Lenght:");
            int lenght = int.Parse(Console.ReadLine());

            Console.WriteLine("Registration number:");
            string reg = Console.ReadLine();

            Console.WriteLine("Draft:");
            decimal draft = decimal.Parse(Console.ReadLine());

            var newBoat = dependencyFactory.BoatLogic().InsertBoat(new Hajo()
            {
                Ferohely = capacity,
                Nev = name,
                Lajstromszam = reg,
                Hajohossz = lenght,
                Merules = draft,
            });

            Console.WriteLine("Boat successfully added!");

            Console.ReadLine();
        }

        private static void GetBoatByName(DependencyFactory factory)
        {
            string boatName = Console.ReadLine();
            var boat = factory.BoatLogic().GettAllBoat().Where(x => x.Nev == boatName).FirstOrDefault();
            if (boat == null)
            {
                Console.WriteLine("Not Boat found");
            }
            else
            {
                var boat1 = factory.BoatLogic().GetHajo(boat.HajoId);
                Console.WriteLine($"Name: {boat1.Nev}, Registration: {boat1.Lajstromszam}, Capacity: {boat1.Ferohely}, Draft: {boat1.Merules}, Length: {boat1.Hajohossz}");
            }

            Console.ReadLine();
        }

        private static void UpdateBoat(DependencyFactory factory)
        {
            Console.WriteLine("-------------");
            ShowAllBoats(factory);
            Console.WriteLine("-------------");

            Console.WriteLine("Add the name of the boat wanted to update.");
            string boatName = Console.ReadLine();
            var boat = factory.BoatLogic().GettAllBoat().Where(x => x.Nev == boatName).FirstOrDefault();
            if (boat == null)
            {
                Console.WriteLine($"There is no boat with the name of {boatName}");
            }
            else
            {
                Console.WriteLine("Name:");
                string name = Console.ReadLine();

                Console.WriteLine("Capacity:");
                int capacity = int.Parse(Console.ReadLine());

                Console.WriteLine("Lenght:");
                int lenght = int.Parse(Console.ReadLine());

                Console.WriteLine("Registration number:");
                string reg = Console.ReadLine();

                Console.WriteLine("Draft:");
                decimal draft = decimal.Parse(Console.ReadLine());

                boat.Nev = name;
                boat.Merules = draft;
                boat.Lajstromszam = reg;
                boat.Hajohossz = lenght;
                boat.Ferohely = capacity;

                var updatedBoat = factory.BoatLogic().UpdateBoat(boat);

                Console.WriteLine($"{updatedBoat.Nev} was updated.");
                Console.ReadLine();
            }
        }

        private static void DeleteBoat(DependencyFactory factory)
        {
            Console.WriteLine("Boat Name:");
            string boatName = Console.ReadLine();
            var boat = factory.BoatLogic().GettAllBoat().Where(x => x.Nev == boatName).FirstOrDefault();
            if (boat == null)
            {
                Console.WriteLine($"There is no boat with the name of {boatName}");
            }
            else
            {
                factory.BoatLogic().DeleteBoat(boat);
                Console.WriteLine($"{boatName} was removed.");
            }
        }

        private static void ShowAllHarbour(DependencyFactory dependencyFactory)
        {
            var harbours = dependencyFactory.HarbourLogic().GetAllHarbour();

            if (harbours.Count == 0)
            {
                Console.WriteLine("There are no boats in the database.");
                Console.ReadLine();
                return;
            }

            foreach (var harbour in harbours)
            {
                Console.WriteLine($"{harbour.KikotoId}. {harbour.Nev}");
            }

            Console.ReadLine();
        }

        private static void NewHarbour(DependencyFactory dependencyFactory)
        {
            Console.WriteLine("Name:");
            string name = Console.ReadLine();

            Console.WriteLine("Harbour Fee:");
            int fee = int.Parse(Console.ReadLine());

            Console.WriteLine("Has Toilet:");
            bool hasToilett = Console.ReadLine() == "yes";

            Console.WriteLine("Number of free slots:");
            int slots = int.Parse(Console.ReadLine());

            dependencyFactory.HarbourLogic().InsertHarbour(new Kikoto()
            {
                Nev = name,
                KikotoDij = fee,
                Mosdo = hasToilett,
                VendegHelyDarab = slots,
            });

            Console.WriteLine("Harbour successfully added!");

            Console.ReadLine();
        }

        private static void GetHarbourByName(DependencyFactory factory)
        {
            string harbourName = Console.ReadLine();
            var harbour = factory.HarbourLogic().GetAllHarbour().Where(x => x.Nev == harbourName).FirstOrDefault();
            if (harbour == null)
            {
                Console.WriteLine("Not Harbour found");
            }
            else
            {
                harbour = factory.HarbourLogic().GetHarbourById(harbour.KikotoId);
                Console.WriteLine($"Name: {harbour.Nev}, Toilett: {((bool)harbour.Mosdo ? "Yes, it has" : "Nope")}, Harbour Fee: {harbour.KikotoDij}, Guest slots: {harbour}");
            }

            Console.ReadLine();
        }

        private static void DeleteHarbour(DependencyFactory factory)
        {
            Console.WriteLine("Harbour Name: ");
            string harbourName = Console.ReadLine();
            var harbour = factory.HarbourLogic().GetAllHarbour().Where(x => x.Nev == harbourName).FirstOrDefault();
            if (harbour == null)
            {
                Console.WriteLine($"There is no harbour with the name of {harbour}");
            }
            else
            {
                factory.HarbourLogic().DeleteHarbour(harbour);
                Console.WriteLine($"{harbour} was removed.");
            }
        }

        private static void UpdateHarbour(DependencyFactory factory)
        {
            Console.WriteLine("-------------");
            ShowAllHarbour(factory);
            Console.WriteLine("-------------");

            Console.WriteLine("Add the name of the harbour wanted to update.");
            string harbourName = Console.ReadLine();
            var harbour = factory.HarbourLogic().GetAllHarbour().Where(x => x.Nev == harbourName).FirstOrDefault();
            if (harbour == null)
            {
                Console.WriteLine($"There is no harbour with the name of {harbourName}");
            }
            else
            {
                Console.WriteLine("Name:");
                string name = Console.ReadLine();

                Console.WriteLine("Harbour Fee:");
                int fee = int.Parse(Console.ReadLine());

                Console.WriteLine("Has Toilet:");
                bool hasToilett = Console.ReadLine() == "yes";

                Console.WriteLine("Number of free slots:");
                int slots = int.Parse(Console.ReadLine());

                harbour.Nev = name;
                harbour.KikotoDij = fee;
                harbour.Mosdo = hasToilett;
                harbour.VendegHelyDarab = slots;

                harbour = factory.HarbourLogic().UpdateHarbour(harbour);

                Console.WriteLine($"{harbour.Nev} was updated.");
                Console.ReadLine();
            }
        }

        private static void NewSailor(DependencyFactory factory)
        {
            Console.WriteLine("Name: ");
            string name = Console.ReadLine();

            factory.SailorLogic().InsertSailor(new Matroz()
            {
                Nev = name,
            });

            Console.WriteLine("Sailor successfully added!");

            Console.ReadLine();
        }

        private static void DeleteSailor(DependencyFactory factory)
        {
            Console.WriteLine("Sailor Name: ");
            string sailorName = Console.ReadLine();
            var sailor = factory.SailorLogic().GetAllSailor().Where(x => x.Nev == sailorName).FirstOrDefault();
            if (sailor == null)
            {
                Console.WriteLine($"There is no harbour with the name of {sailor}");
            }
            else
            {
                factory.SailorLogic().DeleteSailor(sailor);
                Console.WriteLine($"{sailor} was removed.");
            }
        }

        private static void ShowAllSailor(DependencyFactory dependencyFactory)
        {
            var sailors = dependencyFactory.SailorLogic().GetAllSailor();

            if (sailors.Count == 0)
            {
                Console.WriteLine("There are no sailor in the database.");
                Console.ReadLine();
                return;
            }

            foreach (var sailor in sailors)
            {
                Console.WriteLine($"{sailor.MatrozId}. {sailor.Nev}");
            }

            Console.ReadLine();
        }

        private static void UpdateSailor(DependencyFactory factory)
        {
            Console.WriteLine("-------------");
            ShowAllSailor(factory);
            Console.WriteLine("-------------");

            Console.WriteLine("Add the name of the sailor wanted to update.");
            string sailorName = Console.ReadLine();
            var sailor = factory.SailorLogic().GetAllSailor().Where(x => x.Nev == sailorName).FirstOrDefault();
            if (sailor == null)
            {
                Console.WriteLine($"There is no sailor with the name of {sailorName}");
            }
            else
            {
                Console.WriteLine("Name: ");
                string name = Console.ReadLine();

                sailor.Nev = name;

                sailor = factory.SailorLogic().UpdateSailor(sailor);

                Console.WriteLine($"{sailor.Nev} was updated.");
                Console.ReadLine();
            }
        }

        private static void GetSailorByName(DependencyFactory factory)
        {
            string name = Console.ReadLine();
            var sailor = factory.SailorLogic().GetAllSailor().Where(x => x.Nev == name).FirstOrDefault();
            if (sailor == null)
            {
                Console.WriteLine("Not Sailor found");
            }
            else
            {
                sailor = factory.SailorLogic().GetSailorById(sailor.MatrozId);
                Console.WriteLine($"Name: {sailor.Nev}");
            }

            Console.ReadLine();
        }

        private static void CreateTrip(DependencyFactory factory)
        {
            Console.WriteLine("---");
            ShowAllBoats(factory);
            Console.WriteLine("---");
            Console.WriteLine("Choosen Boat Index: ");
            int boatId = int.Parse(Console.ReadLine());

            Console.WriteLine("---");
            ShowAllSailor(factory);
            Console.WriteLine("---");
            Console.WriteLine("Choosen Sailor Index: ");
            int sailorId = int.Parse(Console.ReadLine());

            Console.WriteLine("---");
            ShowAllHarbour(factory);
            Console.WriteLine("---");
            Console.WriteLine("Choosen Deparature Harbour Index: ");
            int depHarbour = int.Parse(Console.ReadLine());
            Console.WriteLine("Choosen Arrival Harbour Index: ");
            int arrHarbour = int.Parse(Console.ReadLine());

            Console.WriteLine("Deparature Date (YYYY.MM.DD)");
            string depDate = Console.ReadLine();
            Console.WriteLine("Arrival Date (YYYY.MM.DD)");
            string arrDate = Console.ReadLine();

            var trip = factory.TripHandler().CreateTrip(boatId, sailorId, depHarbour, arrHarbour, depDate, arrDate);

            Console.WriteLine($"Trip Successfully Created: Boat: {trip.SailBoatName}, Sailor: {trip.SailorName}, Deparature Harbour: {trip.DeparatureHarbour}, Arrival Harbour: {trip.ArrivalHarbour}");
            Console.ReadLine();
        }

        private static void ShowTripDetails(DependencyFactory factory)
        {
            var trips = factory.TripHandler().ShowTrips();

            if (trips.TripDetails.Count == 0)
            {
                Console.WriteLine("There is no trip in the database.");
            }
            else
            {
                foreach (var item in trips.SailorStats)
                {
                    Console.WriteLine($"Sailor: {item.SailorName}");
                    Console.WriteLine($"Sailor's Avarage Sailing Time: {item.AllTimeOnWater}");
                    Console.WriteLine();
                }

                Console.WriteLine($"Avarage trip time: {trips.AvgTripTime} days.");
                Console.WriteLine();
                foreach (var trip in trips.TripDetails)
                {
                    Console.WriteLine($"Boat: {trip.SailBoatName}");
                    Console.WriteLine($"Sailor: {trip.SailorName}");
                    Console.WriteLine($"Deparature Harbour: {trip.DeparatureHarbour}");
                    Console.WriteLine($"Arrival Harbour: {trip.ArrivalHarbour}");
                    Console.WriteLine($"Trip long: {trip.SailTime} days");
                    Console.WriteLine();
                }
            }

            Console.ReadLine();
        }

        private static void ShowTripDetailsAsync(DependencyFactory factory)
        {
            var trips = factory.TripHandler().ShowTripsAsync();

            trips.Wait();

            if (trips.Result.TripDetails.Count == 0)
            {
                Console.WriteLine("There is no trip in the database.");
            }
            else
            {
                Console.WriteLine($"Avarage trip time: {trips.Result.AvgTripTime} days.");

                foreach (var trip in trips.Result.TripDetails)
                {
                    Console.WriteLine($"Boat: {trip.SailBoatName}");
                    Console.WriteLine($"Sailor: {trip.SailorName}");
                    Console.WriteLine($"Deparature Harbour: {trip.DeparatureHarbour}");
                    Console.WriteLine($"Arrival Harbour: {trip.ArrivalHarbour}");
                    Console.WriteLine($"Trip long: {trip.SailTime} days");
                    Console.WriteLine();
                }
            }

            Console.ReadLine();
        }

        private static void ShowSailorStats(DependencyFactory factory)
        {
            var stats = factory.SailorManager().CalculateSailorStats();

            if (stats.Count == 0)
            {
                Console.WriteLine("There no sailor statistics.");
            }
            else
            {
                foreach (var sailorStat in stats)
                {
                    Console.WriteLine($"Name: {sailorStat.SailorName}");
                    Console.WriteLine($"Total Trip Count: {sailorStat.TripCount}");
                    Console.WriteLine($"Number of days on water: {sailorStat.AllTimeOnWater}");
                    Console.WriteLine();
                }
            }

            Console.ReadLine();
        }

        private static void ShowSailorStatsAsync(DependencyFactory factory)
        {
            var stats = factory.SailorManager().CalculateSailorStatsAsync();

            stats.Wait();

            if (stats.Result.Count == 0)
            {
                Console.WriteLine("There no sailor statistics.");
            }
            else
            {
                foreach (var sailorStat in stats.Result)
                {
                    Console.WriteLine($"Name: {sailorStat.SailorName}");
                    Console.WriteLine($"Total Trip Count: {sailorStat.TripCount}");
                    Console.WriteLine($"Number of days on water: {sailorStat.AllTimeOnWater}");
                    Console.WriteLine();
                }
            }

            Console.ReadLine();
        }

        private static void ShowHabourStats(DependencyFactory factory)
        {
            var stats = factory.HarbourManagement().CalculateHarbourStats();

            if (stats.Count == 0)
            {
                Console.WriteLine("There is no harbour statistics.");
            }
            else
            {
                foreach (var harbourStat in stats)
                {
                    Console.WriteLine("----");
                    Console.Write($"Harbour: {harbourStat.HarbourName}");
                    Console.WriteLine();
                    Console.Write("Sailors: ");
                    foreach (var sailor in harbourStat.SailorNames)
                    {
                        Console.Write($"{sailor}, ");
                    }

                    Console.WriteLine();
                    Console.Write("Boats: ");
                    foreach (var harbour in harbourStat.BoatNames)
                    {
                        Console.Write($"{harbour}, ");
                    }

                    Console.WriteLine();
                }
            }

            Console.ReadLine();
        }

        private static void ShowHabourStatsAsync(DependencyFactory factory)
        {
            var stats = factory.HarbourManagement().CalculateHarbourStatsAsync();

            stats.Wait();

            if (stats.Result.Count == 0)
            {
                Console.WriteLine("There is no harbour statistics.");
            }
            else
            {
                foreach (var harbourStat in stats.Result)
                {
                    Console.WriteLine("----");
                    Console.Write($"Harbour: {harbourStat.HarbourName}");
                    Console.WriteLine();
                    Console.Write("Sailors: ");
                    foreach (var sailor in harbourStat.SailorNames)
                    {
                        Console.Write($"{sailor}, ");
                    }

                    Console.WriteLine();
                    Console.Write("Boats: ");
                    foreach (var harbour in harbourStat.BoatNames)
                    {
                        Console.Write($"{harbour}, ");
                    }

                    Console.WriteLine();
                }
            }

            Console.ReadLine();
        }
    }
}
