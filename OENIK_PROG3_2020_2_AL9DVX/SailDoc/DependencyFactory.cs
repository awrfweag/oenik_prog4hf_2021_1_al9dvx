﻿namespace SailDoc
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using Microsoft.EntityFrameworkCore;
    using SailDoc.Data.Models;
    using SailDoc.Logic.Interfaces;
    using SailDoc.Logic.Services;
    using SailDoc.Repository.Repositories;

    /// <summary>
    /// Factory for instances CRUD methods.
    /// </summary>
    public class DependencyFactory
    {
        private DbContext ctx;

        /// <summary>
        /// Return all Boat specifix CRUD methods.
        /// </summary>
        /// <returns>Boat logic instance.</returns>
        public IBoatLogic BoatLogic()
        {
            if (this.ctx == null)
            {
                this.ctx = new SailDocContext();
            }

            BoatRepository boatRepos = new BoatRepository(this.ctx);
            return new BoatLogic(boatRepos);
        }

        /// <summary>
        /// Return all Harbour specific CRUD methods.
        /// </summary>
        /// <returns>Harbour logic instance.</returns>
        public IHarbourLogic HarbourLogic()
        {
            if (this.ctx == null)
            {
                this.ctx = new SailDocContext();
            }

            HarbourRepository harbourRepository = new HarbourRepository(this.ctx);
            return new HarbourLogics(harbourRepository);
        }

        /// <summary>
        /// Return all Sailor specific CRUD methods.
        /// </summary>
        /// <returns>Sailor logic instance.</returns>
        public ISailorLogic SailorLogic()
        {
            if (this.ctx == null)
            {
                this.ctx = new SailDocContext();
            }

            SailorRepository sailorRepository = new SailorRepository(this.ctx);
            return new SailorLogic(sailorRepository);
        }

        /// <summary>
        /// Return methods for trip creating.
        /// </summary>
        /// <returns>ITriphandling interface type.</returns>
        public ITripHandling TripHandler()
        {
            if (this.ctx == null)
            {
                this.ctx = new SailDocContext();
            }

            SailorRepository sailorRepository = new SailorRepository(this.ctx);
            HarbourRepository harbourRepository = new HarbourRepository(this.ctx);
            BoatRepository boatRepository = new BoatRepository(this.ctx);
            RouteRepository routeRepository = new RouteRepository(this.ctx);
            TourRepository tourRepository = new TourRepository(this.ctx);
            TourDateRepository tourDateRepository = new TourDateRepository(this.ctx);

            return new TripHandling(boatRepository, sailorRepository, harbourRepository, routeRepository, tourRepository, tourDateRepository);
        }

        /// <summary>
        /// Return methods for managing sailor.
        /// </summary>
        /// <returns>ISailorManagement interface.</returns>
        public ISailorManagement SailorManager()
        {
            if (this.ctx == null)
            {
                this.ctx = new SailDocContext();
            }

            SailorRepository sailorRepository = new SailorRepository(this.ctx);
            TourRepository tourRepository = new TourRepository(this.ctx);
            TourDateRepository tourDateRepository = new TourDateRepository(this.ctx);

            return new SailorManagement(tourRepository, sailorRepository, tourDateRepository);
        }

        /// <summary>
        /// Retrun methods for harbour managements.
        /// </summary>
        /// <returns>IHabour interface.</returns>
        public IHarbourManagement HarbourManagement()
        {
            if (this.ctx == null)
            {
                this.ctx = new SailDocContext();
            }

            SailorRepository sailorRepository = new SailorRepository(this.ctx);
            HarbourRepository harbourRepository = new HarbourRepository(this.ctx);
            BoatRepository boatRepository = new BoatRepository(this.ctx);
            RouteRepository routeRepository = new RouteRepository(this.ctx);
            TourRepository tourRepository = new TourRepository(this.ctx);

            return new HarbourManagement(tourRepository, sailorRepository, routeRepository, harbourRepository, boatRepository);
        }
    }
}
